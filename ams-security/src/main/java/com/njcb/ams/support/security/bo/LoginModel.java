package com.njcb.ams.support.security.bo;

/**
 * 授权模式
 * @author LIUYANLONG
 *
 */
public enum LoginModel {
	/**
     * 用户密码
     */
	PASSWORD,
	
	/**
	 *授权码
	 */
	AUTHCODE,

	/**
	 * 不限制
	 */
	NONE;

}
