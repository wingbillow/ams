package com.njcb.ams.support.security.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.util.AmsJsonUtils;

/**
 * 用来解决认证过的用户访问无权限资源时的异常
 * @author liuyanlong
 */
public class CustomAccessDeineHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
            AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        ServletOutputStream out = response.getOutputStream();
		out.write(AmsJsonUtils.objectToJson(Response.buildFail("403", "此用户没有访问权限!")).getBytes("UTF-8"));
		out.flush();
    }

}
