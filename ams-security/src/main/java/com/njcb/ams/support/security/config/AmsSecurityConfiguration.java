package com.njcb.ams.support.security.config;

import java.util.List;
import java.util.Map;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import com.njcb.ams.support.security.bo.LoginModel;
import com.njcb.ams.support.security.bo.SecurityUser;
import com.njcb.ams.support.security.bo.SessionModel;
import com.njcb.ams.support.security.util.TokenUtil;

public interface AmsSecurityConfiguration {
	
	/**
	 * 配置登录方式，用户密码模式/授权码模式
	 * 默认用户密码模式
	 * @return
	 */
	default public LoginModel loginModel(){
		return LoginModel.PASSWORD;
	}
	

	/**
	 * 用户维持机制， session/token
	 * 默认用户密码模式
	 * @return
	 */
	default public SessionModel sessionModel(){
		return SessionModel.SESSION;
	}


	/**
	 * 加载用户，根据用户名称加载用于登录的用户
	 * @param userName 登录用户名,用户密码登录场景时使用
	 * @param authCode 授权码、授权码登录时使用
	 * @param param  登录时支持的扩展参数
	 * @param isLogin 是否初次登陆
	 * @return
	 */
	public SecurityUser loadUserByUsername(String userName, String authCode, Map<String, Object> param, Boolean isLogin);
	
	/**
	 * 用户登录动作通知更新
	 * @param securityUser
	 */
	default public void updateUser(SecurityUser securityUser){
		return;
	}
	
	
	/**
	 * 存储token
	 */
	default public void storage(String tokenId, Map<String,Object> claims){
		TokenUtil.cacheAddToken(tokenId, claims);
	}
	
	/**
	 * 获取token
	 */
	default public Map<String,Object> obtain(String tokenId){
		return TokenUtil.cacheGetToken(tokenId);
	}
	
	/**
	 * spring安全配置通过此处来配置
	 * @param http
	 */
	default public void configureHttpSecurity(HttpSecurity http){
		return;
	}
	
	/**
	 * 开发权限的URL列表
	 * @param antPatterns
	 */
	default public void permitMatchers(List<String> antPatterns){
	}
	
}
