package com.njcb.ams.support.security.config;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.njcb.ams.support.security.bo.SecurityUser;

@Component
public class DefaultAmsSecurityConfiguration implements AmsSecurityConfiguration {

	@Override
	public SecurityUser loadUserByUsername(String loginCode, String authCode, Map<String, Object> param, Boolean isLogin) {
		return new SecurityUser();
	}
	
}
