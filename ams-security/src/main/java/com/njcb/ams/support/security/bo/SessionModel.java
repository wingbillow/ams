package com.njcb.ams.support.security.bo;

/**
 * 授权模式
 * @author LIUYANLONG
 *
 */
public enum SessionModel {
	/**
     * session机制
     */
	SESSION,
	
	/**
	 *token机制
	 */
	TOKEN;

}
