//package com.njcb.ams.bootconfig;
//
//import org.apache.catalina.Context;
//import org.apache.catalina.Session;
//import org.apache.catalina.session.StandardManager;
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.session.SessionInformation;
//import org.springframework.security.core.session.SessionRegistryImpl;
//
//import java.util.List;
//
///**
// * @author LOONG
// */
//@Configuration
//public class SessionManagerConfig extends TomcatServletWebServerFactory {
//
//    @Override
//    protected void postProcessContext(Context context) {
//        context.setManager(new CustomSessionManager());
//    }
//
//    @Bean
//    public CustomSessionManager sessionManager() {
//        return new CustomSessionManager();
//    }
//
//    class CustomSessionManager extends StandardManager {
//        @Override
//        public void add(Session session) {
//            super.add(session);
//        }
//    }
//
//    static class CustomSessionRegistry extends SessionRegistryImpl{
//        CustomSessionManager customSessionManager;
//        public CustomSessionRegistry(CustomSessionManager customSessionManager) {
//            super();
//            this.customSessionManager = customSessionManager;
//        }
//
//        @Override
//        public List<SessionInformation> getAllSessions(Object principal, boolean includeExpiredSessions) {
//            return super.getAllSessions(principal,includeExpiredSessions);
//        }
//    }
//}