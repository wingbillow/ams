package com.njcb.ams.pojo.dto;

import com.njcb.ams.pojo.dto.AccessToken;

/**
 * @author LOONG
 */
public class AccessTokenResponse {
    private boolean success;
    private String code;
    private String message;
    private AccessToken entity;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AccessToken getEntity() {
        return entity;
    }

    public void setEntity(AccessToken entity) {
        this.entity = entity;
    }
}
