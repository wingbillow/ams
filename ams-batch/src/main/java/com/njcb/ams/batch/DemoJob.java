package com.njcb.ams.batch;

import com.njcb.ams.batch.step.DemoProcessor;
import com.njcb.ams.batch.step.DemoReader;
import com.njcb.ams.batch.step.DemoStepListener;
import com.njcb.ams.batch.step.DemoWriter;
import com.njcb.ams.repository.entity.SysTradeLog;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 日终对账任务
 *
 * @author LOONG
 */
@Configuration
public class DemoJob {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DemoReader demoReader;

    @Autowired
    public DemoProcessor demoProcessor;

    @Autowired
    public DemoWriter demoWriter;

    @Autowired
    public DemoStepListener demoStepListener;


    @Bean
    public Job processJob() {
        return jobBuilderFactory.get("dailyCheckJob")
                .incrementer(new RunIdIncrementer()).listener(listener())
                .start(demoStep())
                .build();
    }

    /**
     * 1、批量入库借据文件
     *
     * @return
     */
    @Bean
    public Step demoStep() {
        return stepBuilderFactory.get("orderStep")
                .listener(demoStepListener)
                .<SysTradeLog, String>chunk(100)
                .reader(demoReader).processor(demoProcessor)
                .writer(demoWriter).build();
    }


    @Bean
    public JobExecutionListener listener() {
        return new DemoJobCompletionListener();
    }

}