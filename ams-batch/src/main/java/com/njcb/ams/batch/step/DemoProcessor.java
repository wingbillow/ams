package com.njcb.ams.batch.step;

import com.njcb.ams.repository.entity.SysTradeLog;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class DemoProcessor implements ItemProcessor<SysTradeLog, String> {

    @Override
    public String process(SysTradeLog data) throws Exception {
        System.out.println("处理内容: " + data.getTradeCode()+ data.getTransSeq());
        return data.getTradeCode().toUpperCase();
    }

}