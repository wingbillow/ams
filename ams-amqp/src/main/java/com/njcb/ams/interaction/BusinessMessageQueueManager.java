package com.njcb.ams.application;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 交易管理类 系统所有对外及对内功能以交易未单位发布， 事务控制默认以交易为单位
 * 
 * @author liuyanlong
 *
 */
@Service
@Lazy(false)
//@Interaction
public class BusinessMessageQueueManager {
//	private static final Logger logger = LoggerFactory.getLogger(BusinessMessageQueueManager.class);
//
//	@Uncheck
//	public static BusinessMessageQueueManager getInstance() {
//		logger.debug("Instance BusinessManager");
//		return ContextUtil.getBean(BusinessMessageQueueManager.class);
//	}

}
