package com.njcb.ams.bootconfig;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.njcb.ams")
public class RabbitMqConfig {
	private String host = "trythis.cn";
	private String username = "guest";
	private String password = "guest";
	private String virtualHost = "/";

	public static final String EXCHANGE_WORKFLOW_CENTER = "WORKFLOW.CENTER";
	public static final String EXCHANGE_WORKFLOW_EXTERNAL = "WORKFLOW.EXTERNAL";

	public static final String B38_WORKFLOW_IN = "B38.WORKFLOW.IN";
	public static final String B39_WORKFLOW_IN = "B39.WORKFLOW.IN";
	public static final String B00_WORKFLOW_IN = "B00.WORKFLOW.IN";
	
	public static final String ALL_WORKFLOW_IN = "ALL.WORKFLOW.IN";

	public static final String ROUTINGKEY_B38_WORKFLOW_IN = "B38.WORKFLOW.IN";
	public static final String ROUTINGKEY_B39_WORKFLOW_IN = "B39.WORKFLOW.IN";
	public static final String ROUTINGKEY_B00_WORKFLOW_IN = "B00.WORKFLOW.IN";
	
	public static final String ROUTINGKEY_ALL_WORKFLOW_IN = "ALL.WORKFLOW.IN";
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setVirtualHost(virtualHost);
		connectionFactory.setPublisherConfirms(true);
		return connectionFactory;
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		return template;
	}

	@Bean
	public Jackson2JsonMessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}