package com.njcb.ams.bootconfig;

/**
 *	 定义rabbitMq需要的常量
 *
 **/
public class RabbitMqEnum {

	/**
	 * 	定义数据交换方式
	 * @author LOONG
	 *
	 */
	public enum Exchange {
		CONTRACT_FANOUT("CONTRACT_FANOUT", "消息分发"), 
		CONTRACT_TOPIC("CONTRACT_TOPIC", "消息订阅"),
		CONTRACT_DIRECT("CONTRACT_DIRECT", "点对点");

		private String code;
		private String name;

		Exchange(String code, String name) {
			this.code = code;
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}

}
