package com.njcb.ams.rabbitmq.producer;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.njcb.ams.bootconfig.ConsumerConfig;
import com.njcb.ams.bootconfig.RabbitMqConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RabbitMqConfig.class,ConsumerConfig.class})
@EnableRabbit
public class MessageSenderTest {

	@Autowired
	private MessageSender msgProducer;
	@Test
	public void testProducer() {
		for (int i = 0; i < 1; i++) {
			msgProducer.sendMsgDirect("你好22");
		}
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
