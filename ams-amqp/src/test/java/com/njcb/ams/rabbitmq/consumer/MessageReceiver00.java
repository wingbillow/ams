package com.njcb.ams.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;

//@Component
//@RabbitListener(queues = RabbitMqConfig.B00_WORKFLOW_IN)
public class MessageReceiver00 {

	@RabbitHandler
	public void process(String content) {
		System.out.println("B00_WORKFLOW_IN接收处理队列A当中的消息： " + content);
	}

}