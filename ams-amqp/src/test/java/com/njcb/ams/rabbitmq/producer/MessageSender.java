package com.njcb.ams.rabbitmq.producer;

import java.util.UUID;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.njcb.ams.bootconfig.RabbitMqConfig;

@Component
public class MessageSender implements RabbitTemplate.ConfirmCallback {

	// 由于rabbitTemplate的scope属性设置为ConfigurableBeanFactory.SCOPE_PROTOTYPE，所以不能自动注入
	private RabbitTemplate rabbitTemplate;

	/**
	 * 构造方法注入rabbitTemplate
	 */
	@Autowired
	public MessageSender(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
		rabbitTemplate.setConfirmCallback(this); // rabbitTemplate如果为单例的话，那回调就是最后设置的内容
	}

	public void sendMsgDirect(String content) {
		CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
		// 把消息放入ROUTINGKEY_A对应的队列当中去，对应的是队列A
		rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_WORKFLOW_EXTERNAL,
				RabbitMqConfig.ROUTINGKEY_B00_WORKFLOW_IN, content, correlationId);
	}

	public void sendMsgTopic(String content) {
		CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
		// 把消息放入ROUTINGKEY_A对应的队列当中去，对应的是队列A
		rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_WORKFLOW_CENTER,
				RabbitMqConfig.ROUTINGKEY_ALL_WORKFLOW_IN, content, correlationId);
	}

	/**
	 * 回调
	 */
	@Override
	public void confirm(CorrelationData correlationData, boolean ack, String cause) {
		System.out.println(" 回调id:" + correlationData);
		if (ack) {
			System.out.println("消息成功消费");
		} else {
			System.out.println("消息消费失败:" + cause);
		}
	}
}