package com.njcb.ams.assembler;

import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.scheduler.bean.ScheduleJob;
import com.njcb.ams.scheduler.utils.JobConstant;
import com.njcb.ams.util.AmsJsonUtils;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class SysTaskJobConvertor {

	public static ScheduleJob initScheduleJob(ScheduleJob job, SysTaskJob taskJob) {
		if (job == null) {
			job = new ScheduleJob();
		}
		job.setJobId(taskJob.getId());
		job.setJobGroup(JobConstant.GROUP_NAME);
		job.setJobName(taskJob.getJobName());
		job.setCronExpression(taskJob.getCronExpression());
		job.setClassName(taskJob.getProcessFunction());
		job.setJobParam(AmsJsonUtils.jsonToMap(taskJob.getProcessParam()));
		return job;
	}

	public static SysTaskJob initSysTaskJob(ScheduleJob scheduleJob) {
		SysTaskJob sysTaskJob = new SysTaskJob();
		sysTaskJob.setId(scheduleJob.getJobId());
		sysTaskJob.setJobName(scheduleJob.getJobName());
		sysTaskJob.setCronExpression(scheduleJob.getCronExpression());
		sysTaskJob.setProcessFunction(scheduleJob.getClassName());
		return sysTaskJob;
	}
}
