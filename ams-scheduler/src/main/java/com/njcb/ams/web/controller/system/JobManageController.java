package com.njcb.ams.web.controller.system;

import com.njcb.ams.application.BusinessJobManager;
import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.repository.entity.SysTaskJobLog;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.scheduler.bean.ScheduleJob;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author LOONG
 */
@RestController
@RequestMapping("/system/job")
@Api(value = "job",tags = "job")
public class JobManageController {

	@Autowired
	private BusinessJobManager businessManager;

	@ApiOperation(value = "获取任务列表",response = ScheduleJob.class)
	@RequestMapping(value = "query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<SysTaskJob> jobManageQuery(@RequestBody SysTaskJob sysTaskJob) {
		List<SysTaskJob> jobList = businessManager.taskJobQuery(sysTaskJob);
		return PageResponse.build(jobList);
	}

	@ApiOperation(value = "任务修改保存", produces = MediaType.APPLICATION_JSON_VALUE,response = Map.class)
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Response jobManageSave(@RequestBody SysTaskJob sysTaskJob) throws Exception {
		businessManager.taskJobSave(sysTaskJob);
		return Response.buildSucc();
	}

	@ApiOperation(value = "删除任务", notes = "单笔删除任务")
	@RequestMapping(value = "{jobId}/remove", method = RequestMethod.DELETE)
	@ResponseBody
	public Response orgInfoSimpleRemove(@ApiParam(required = true, value = "任务号") @PathVariable("jobId") Integer jobId) throws Exception {
		businessManager.taskJobDelete(jobId);
		return Response.buildSucc();
	}

	@ApiOperation(value = "启动任务")
	@RequestMapping(value = "start", method = RequestMethod.POST)
	@ResponseBody
	public Response jobStart(@RequestBody SysTaskJob sysTaskJob) throws Exception {
		businessManager.taskJobStart(sysTaskJob);
		return Response.buildSucc();
	}

	@ApiOperation(value = "停止任务")
	@RequestMapping(value = "stop", method = RequestMethod.POST)
	@ResponseBody
	public Response jobStop(@RequestBody SysTaskJob sysTaskJob) throws Exception {
		businessManager.taskJobStop(sysTaskJob);
		return Response.buildSucc();
	}

	@ApiOperation(value = "立即执行任务")
	@RequestMapping(value = "trigger", method = RequestMethod.POST)
	@ResponseBody
	public Response jobTrigger(@RequestBody SysTaskJob sysTaskJob) throws Exception {
		businessManager.taskJobTrigger(sysTaskJob);
		return Response.buildSucc();
	}
	
	
	@ApiOperation(value = "任务执行日志查询")
	@RequestMapping(value = "log/query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<SysTaskJobLog> jobLogQuery(@RequestBody SysTaskJobLog sysTaskJobLog) {
		PageHandle.startPage(sysTaskJobLog);
		List<SysTaskJobLog> jobList = businessManager.jobLogQuery(sysTaskJobLog);
		Page page = PageHandle.endPage();
		return PageResponse.build(jobList, page.getTotal());
	}

}
