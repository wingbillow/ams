package com.njcb.ams.bootconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.njcb.ams.scheduler.service.AmsJobSchedulerManager;

@Component
@Lazy(false)
public class AmsSchedulerInit {
	private static final Logger logger = LoggerFactory.getLogger(AmsSchedulerInit.class);
	@Autowired
	private AmsJobSchedulerManager amsJobSchedulerManager;

	@PostConstruct
	public void init() {
		try {
			logger.info("-------------------开始定时任务开始-------------------");
			amsJobSchedulerManager.initJobs();
			logger.info("-------------------开始定时任务成功-------------------");
		} catch (Throwable e) {
			logger.error("------------------开始定时任务失败-------------------", e);
			System.exit(0);
		}
	}

}
