package com.njcb.ams.scheduler.utils;

/**
 * 定时任务常量
 */

public class JobConstant {

	// 任务有效
	public static final String JOB_STATUS_ACTIVE = "01";
	// 任务无效
	public static final String JOB_STATUS_INVALID = "02";

	
	public static final String GROUP_NAME = "AmsGroup";
	
	public static final String JOBBEAN_KEY = "taskjobbeanid";
	
	/**
	 * RESULT_SUCCESS        任务运行结果_成功_轮询未结束则继续轮询
	 * RESULT_SUCCESS_TO_END 任务运行结果_成功_结束轮询
	 * RESULT_FAIL           任务运行结果_失败_轮询未结束则继续轮询
	 * RESULT_FAIL_TO_END    任务运行结果_失败_轮询未结束则继续轮询
	 * RESULT_IGNORE         任务运行结果_忽略_不计入轮询次数
	 */
	public enum JobResult {
		RESULT_SUCCESS("1"), RESULT_SUCCESS_TO_END("2"), RESULT_FAIL("3"), RESULT_FAIL_TO_END("4"), RESULT_IGNORE("5");
		/**
		 * 值
		 */
		private String value;

		private JobResult(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
}
