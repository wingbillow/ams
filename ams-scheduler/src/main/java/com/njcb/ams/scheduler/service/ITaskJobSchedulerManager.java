package com.njcb.ams.scheduler.service;

import java.util.List;

import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.repository.entity.SysTaskJobLog;

public interface ITaskJobSchedulerManager {
    List<SysTaskJob> findAllJob() throws Exception;

    void startJob(int jobno) throws Exception;

    void stopJob(int jobno) throws Exception;

    void restartJob(int jobno) throws Exception;

    void triggerJob(int jobno) throws Exception;

    void startAllJob() throws Exception;

    void stopAllJob() throws Exception;

    void stopScheduler() throws Exception;

    Boolean isRunning(int jobNo);

    void updateJob(SysTaskJob job);

    void writeLog(SysTaskJobLog log) throws Exception;
}
