package com.njcb.ams.scheduler.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.quartz.impl.calendar.DailyCalendar;

public class JobUtils {

	/**
	 * 字符串是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0));
	}

	/**
	 * 获取固定格式时间字符串
	 * 
	 * @return
	 */
	public static String getTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return format.format(Calendar.getInstance().getTime());
	}

	

	public static org.quartz.Calendar getDailyColendar(String startTime, String endTime) throws ParseException {
		// 获得每次的开始和结束时间
		SimpleDateFormat format = new SimpleDateFormat("HHmmss");
		Date startDate = format.parse(startTime);
		Date endDate = format.parse(endTime);

		Calendar calendarStart = Calendar.getInstance();
		Calendar calendarEnd = Calendar.getInstance();
		calendarStart.setTime(startDate);
		calendarEnd.setTime(endDate);

		format = new SimpleDateFormat("HH:mm:ss");
		// 时间段（配合cronexpression使用，可以排除掉设置的时间段外的触发）
		DailyCalendar timeRange;
		// 不跨0点的时间设置,排除掉设置的时间段之外的触发
		if (calendarStart.compareTo(calendarEnd) <= 0) {
			// 时间相同的情况下必须置endtime为大
			calendarEnd.add(Calendar.SECOND, 1);
			timeRange = new DailyCalendar(format.format(calendarStart.getTime()), format.format(calendarEnd.getTime()));
			timeRange.setInvertTimeRange(true);
		} else {
			// 跨0点的时间设置，排除掉开始结束时间换位后的时间段的触发(跨天的时间设置有问题，有待修复)
			timeRange = new DailyCalendar(format.format(calendarEnd.getTime()), format.format(calendarStart.getTime()));
			timeRange.setInvertTimeRange(false);
		}

		return timeRange;
	}

}
