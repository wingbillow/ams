package com.njcb.ams.application;

import java.util.List;

import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.service.SysTaskJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import com.njcb.ams.repository.entity.SysTaskJobLog;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.scheduler.service.AmsJobSchedulerManager;
import com.njcb.ams.support.annotation.Interaction;
import com.njcb.ams.support.annotation.Trader;
import com.njcb.ams.support.annotation.Uncheck;
import com.njcb.ams.support.annotation.enums.TradeType;

/**
 * 系统基础功能
 * 
 * @author liuyanlong
 *
 */
@Lazy(false)
@Interaction(groupName = "ZZ.AMS.SCHEDULER.JOB")
public class BusinessJobManager {
	private static final Logger logger = LoggerFactory.getLogger(BusinessJobManager.class);
	@Autowired
	private AmsJobSchedulerManager amsJobSchedulerManager;
	@Autowired
	private SysTaskJobService sysTaskJobService;

	@Uncheck
	public static BusinessJobManager getInstance() {
		logger.debug("Instance BusinessManager");
		return AppContext.getBean(BusinessJobManager.class);
	}

	@Trader(tradeCode = "TJ1001", tradeName = "定时任务查询", tradeType = TradeType.TYPE_QUERY) // 定时任务查询
	public List<SysTaskJob> taskJobQuery(SysTaskJob sysTaskJob) {
		return sysTaskJobService.taskJobQuery(sysTaskJob);
	}

	@Trader(tradeCode = "TJ1002", tradeName = "定时任务修改保存") // 定时任务修改保存
	public void taskJobSave(SysTaskJob sysTaskJob) throws Exception {
		sysTaskJobService.updateJob(sysTaskJob);
	}

	@Trader(tradeCode = "TJ1003", tradeName = "定时任务修删除") // 定时任务修改保存
	public void taskJobDelete(Integer jobId) throws Exception {
		sysTaskJobService.deleteJob(jobId);
	}

	@Trader(tradeCode = "TJ2001", tradeName = "定时任务启动") // 定时任务启动
	public void taskJobStart(SysTaskJob sysTaskJob) throws Exception {
		sysTaskJobService.taskJobStart(sysTaskJob);
	}

	@Trader(tradeCode = "TJ2002", tradeName = "定时任务停止") // 定时任务停止
	public void taskJobStop(SysTaskJob sysTaskJob) throws Exception {
		sysTaskJobService.taskJobStop(sysTaskJob);
	}

	@Trader(tradeCode = "TJ2003", tradeName = "定时任务立即执行") // 定时任务立即执行
	public void taskJobTrigger(SysTaskJob sysTaskJob) throws Exception {
		sysTaskJobService.taskJobTrigger(sysTaskJob);
	}
	
	@Trader(tradeCode = "TJ3001", tradeName = "任务执行日志查询", tradeType = TradeType.TYPE_QUERY) // 定时任务查询
	public List<SysTaskJobLog> jobLogQuery(SysTaskJobLog sysTaskJobLog) {
		return amsJobSchedulerManager.jobLogQuery(sysTaskJobLog);
	}

}
