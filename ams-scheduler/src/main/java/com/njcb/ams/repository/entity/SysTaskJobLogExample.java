/*
 * SysTaskJobLogExample.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class SysTaskJobLogExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysTaskJobLogExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andJobNameIsNull() {
            addCriterion("JOB_NAME is null");
            return (Criteria) this;
        }

        public Criteria andJobNameIsNotNull() {
            addCriterion("JOB_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andJobNameEqualTo(String value) {
            addCriterion("JOB_NAME =", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotEqualTo(String value) {
            addCriterion("JOB_NAME <>", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameGreaterThan(String value) {
            addCriterion("JOB_NAME >", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_NAME >=", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLessThan(String value) {
            addCriterion("JOB_NAME <", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLessThanOrEqualTo(String value) {
            addCriterion("JOB_NAME <=", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLike(String value) {
            addCriterion("JOB_NAME like", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotLike(String value) {
            addCriterion("JOB_NAME not like", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameIn(List<String> values) {
            addCriterion("JOB_NAME in", values, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotIn(List<String> values) {
            addCriterion("JOB_NAME not in", values, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameBetween(String value1, String value2) {
            addCriterion("JOB_NAME between", value1, value2, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotBetween(String value1, String value2) {
            addCriterion("JOB_NAME not between", value1, value2, "jobName");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIsNull() {
            addCriterion("PROCESS_FUNCTION is null");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIsNotNull() {
            addCriterion("PROCESS_FUNCTION is not null");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION =", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION <>", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionGreaterThan(String value) {
            addCriterion("PROCESS_FUNCTION >", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionGreaterThanOrEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION >=", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLessThan(String value) {
            addCriterion("PROCESS_FUNCTION <", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLessThanOrEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION <=", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLike(String value) {
            addCriterion("PROCESS_FUNCTION like", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotLike(String value) {
            addCriterion("PROCESS_FUNCTION not like", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIn(List<String> values) {
            addCriterion("PROCESS_FUNCTION in", values, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotIn(List<String> values) {
            addCriterion("PROCESS_FUNCTION not in", values, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionBetween(String value1, String value2) {
            addCriterion("PROCESS_FUNCTION between", value1, value2, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotBetween(String value1, String value2) {
            addCriterion("PROCESS_FUNCTION not between", value1, value2, "processFunction");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNull() {
            addCriterion("EXCUTE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNotNull() {
            addCriterion("EXCUTE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeEqualTo(String value) {
            addCriterion("EXCUTE_TIME =", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotEqualTo(String value) {
            addCriterion("EXCUTE_TIME <>", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThan(String value) {
            addCriterion("EXCUTE_TIME >", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThanOrEqualTo(String value) {
            addCriterion("EXCUTE_TIME >=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThan(String value) {
            addCriterion("EXCUTE_TIME <", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThanOrEqualTo(String value) {
            addCriterion("EXCUTE_TIME <=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLike(String value) {
            addCriterion("EXCUTE_TIME like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotLike(String value) {
            addCriterion("EXCUTE_TIME not like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIn(List<String> values) {
            addCriterion("EXCUTE_TIME in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotIn(List<String> values) {
            addCriterion("EXCUTE_TIME not in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeBetween(String value1, String value2) {
            addCriterion("EXCUTE_TIME between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotBetween(String value1, String value2) {
            addCriterion("EXCUTE_TIME not between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNull() {
            addCriterion("EXCUTE_RESULT is null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNotNull() {
            addCriterion("EXCUTE_RESULT is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultEqualTo(String value) {
            addCriterion("EXCUTE_RESULT =", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotEqualTo(String value) {
            addCriterion("EXCUTE_RESULT <>", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThan(String value) {
            addCriterion("EXCUTE_RESULT >", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThanOrEqualTo(String value) {
            addCriterion("EXCUTE_RESULT >=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThan(String value) {
            addCriterion("EXCUTE_RESULT <", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThanOrEqualTo(String value) {
            addCriterion("EXCUTE_RESULT <=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLike(String value) {
            addCriterion("EXCUTE_RESULT like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotLike(String value) {
            addCriterion("EXCUTE_RESULT not like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIn(List<String> values) {
            addCriterion("EXCUTE_RESULT in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotIn(List<String> values) {
            addCriterion("EXCUTE_RESULT not in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultBetween(String value1, String value2) {
            addCriterion("EXCUTE_RESULT between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotBetween(String value1, String value2) {
            addCriterion("EXCUTE_RESULT not between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeIsNull() {
            addCriterion("EXCUTE_NODE is null");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeIsNotNull() {
            addCriterion("EXCUTE_NODE is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeEqualTo(String value) {
            addCriterion("EXCUTE_NODE =", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeNotEqualTo(String value) {
            addCriterion("EXCUTE_NODE <>", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeGreaterThan(String value) {
            addCriterion("EXCUTE_NODE >", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeGreaterThanOrEqualTo(String value) {
            addCriterion("EXCUTE_NODE >=", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeLessThan(String value) {
            addCriterion("EXCUTE_NODE <", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeLessThanOrEqualTo(String value) {
            addCriterion("EXCUTE_NODE <=", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeLike(String value) {
            addCriterion("EXCUTE_NODE like", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeNotLike(String value) {
            addCriterion("EXCUTE_NODE not like", value, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeIn(List<String> values) {
            addCriterion("EXCUTE_NODE in", values, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeNotIn(List<String> values) {
            addCriterion("EXCUTE_NODE not in", values, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeBetween(String value1, String value2) {
            addCriterion("EXCUTE_NODE between", value1, value2, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andExcuteNodeNotBetween(String value1, String value2) {
            addCriterion("EXCUTE_NODE not between", value1, value2, "excuteNode");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIsNull() {
            addCriterion("TRIGGER_STATE is null");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIsNotNull() {
            addCriterion("TRIGGER_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerStateEqualTo(String value) {
            addCriterion("TRIGGER_STATE =", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotEqualTo(String value) {
            addCriterion("TRIGGER_STATE <>", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateGreaterThan(String value) {
            addCriterion("TRIGGER_STATE >", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateGreaterThanOrEqualTo(String value) {
            addCriterion("TRIGGER_STATE >=", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLessThan(String value) {
            addCriterion("TRIGGER_STATE <", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLessThanOrEqualTo(String value) {
            addCriterion("TRIGGER_STATE <=", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLike(String value) {
            addCriterion("TRIGGER_STATE like", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotLike(String value) {
            addCriterion("TRIGGER_STATE not like", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIn(List<String> values) {
            addCriterion("TRIGGER_STATE in", values, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotIn(List<String> values) {
            addCriterion("TRIGGER_STATE not in", values, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateBetween(String value1, String value2) {
            addCriterion("TRIGGER_STATE between", value1, value2, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotBetween(String value1, String value2) {
            addCriterion("TRIGGER_STATE not between", value1, value2, "triggerState");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgIsNull() {
            addCriterion("EXCEPTION_MSG is null");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgIsNotNull() {
            addCriterion("EXCEPTION_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgEqualTo(String value) {
            addCriterion("EXCEPTION_MSG =", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgNotEqualTo(String value) {
            addCriterion("EXCEPTION_MSG <>", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgGreaterThan(String value) {
            addCriterion("EXCEPTION_MSG >", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgGreaterThanOrEqualTo(String value) {
            addCriterion("EXCEPTION_MSG >=", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgLessThan(String value) {
            addCriterion("EXCEPTION_MSG <", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgLessThanOrEqualTo(String value) {
            addCriterion("EXCEPTION_MSG <=", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgLike(String value) {
            addCriterion("EXCEPTION_MSG like", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgNotLike(String value) {
            addCriterion("EXCEPTION_MSG not like", value, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgIn(List<String> values) {
            addCriterion("EXCEPTION_MSG in", values, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgNotIn(List<String> values) {
            addCriterion("EXCEPTION_MSG not in", values, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgBetween(String value1, String value2) {
            addCriterion("EXCEPTION_MSG between", value1, value2, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andExceptionMsgNotBetween(String value1, String value2) {
            addCriterion("EXCEPTION_MSG not between", value1, value2, "exceptionMsg");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}