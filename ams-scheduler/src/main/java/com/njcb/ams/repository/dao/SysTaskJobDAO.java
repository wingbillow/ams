/*
 * SysTaskJob.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTaskJobMapper;
import com.njcb.ams.repository.entity.SysTaskJob;
import com.njcb.ams.repository.entity.SysTaskJobExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SysTaskJobDAO extends BaseMyBatisDAO<SysTaskJob, SysTaskJobExample, Integer> {
    @Autowired
    private SysTaskJobMapper mapper;

    public SysTaskJobDAO() {
        this.entityClass = SysTaskJob.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SysTaskJobMapper getMapper() {
        return mapper;
    }

    public List<SysTaskJob> selectAll() {
        return mapper.selectByExample(new SysTaskJobExample());
    }


    public List<SysTaskJob> selectByJobDesc(String jobDesc) {
        SysTaskJobExample example = new SysTaskJobExample();
        example.createCriteria().andRemarkLike("%" + jobDesc + "%");
        return mapper.selectByExample(example);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public List<SysTaskJob> getAllJobForUpdate() {
        return selectAll();
    }

    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public void updateLastRunTime(Integer id , String lastRunTime) {
        SysTaskJob updateJobBean = new SysTaskJob();
        updateJobBean.setId(id);
        updateJobBean.setLastRunTime(lastRunTime);
        updateByPrimaryKeySelective(updateJobBean);
    }
}