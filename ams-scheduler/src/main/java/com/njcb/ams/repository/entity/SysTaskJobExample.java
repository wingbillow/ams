/*
 * SysTaskJobExample.java
 * Copyright(C) trythis.cn
 * 2020年06月02日 14时43分16秒Created
 */
package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class SysTaskJobExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysTaskJobExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andJobNameIsNull() {
            addCriterion("JOB_NAME is null");
            return (Criteria) this;
        }

        public Criteria andJobNameIsNotNull() {
            addCriterion("JOB_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andJobNameEqualTo(String value) {
            addCriterion("JOB_NAME =", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotEqualTo(String value) {
            addCriterion("JOB_NAME <>", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameGreaterThan(String value) {
            addCriterion("JOB_NAME >", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_NAME >=", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLessThan(String value) {
            addCriterion("JOB_NAME <", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLessThanOrEqualTo(String value) {
            addCriterion("JOB_NAME <=", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameLike(String value) {
            addCriterion("JOB_NAME like", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotLike(String value) {
            addCriterion("JOB_NAME not like", value, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameIn(List<String> values) {
            addCriterion("JOB_NAME in", values, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotIn(List<String> values) {
            addCriterion("JOB_NAME not in", values, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameBetween(String value1, String value2) {
            addCriterion("JOB_NAME between", value1, value2, "jobName");
            return (Criteria) this;
        }

        public Criteria andJobNameNotBetween(String value1, String value2) {
            addCriterion("JOB_NAME not between", value1, value2, "jobName");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIsNull() {
            addCriterion("PROCESS_FUNCTION is null");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIsNotNull() {
            addCriterion("PROCESS_FUNCTION is not null");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION =", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION <>", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionGreaterThan(String value) {
            addCriterion("PROCESS_FUNCTION >", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionGreaterThanOrEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION >=", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLessThan(String value) {
            addCriterion("PROCESS_FUNCTION <", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLessThanOrEqualTo(String value) {
            addCriterion("PROCESS_FUNCTION <=", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionLike(String value) {
            addCriterion("PROCESS_FUNCTION like", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotLike(String value) {
            addCriterion("PROCESS_FUNCTION not like", value, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionIn(List<String> values) {
            addCriterion("PROCESS_FUNCTION in", values, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotIn(List<String> values) {
            addCriterion("PROCESS_FUNCTION not in", values, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionBetween(String value1, String value2) {
            addCriterion("PROCESS_FUNCTION between", value1, value2, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessFunctionNotBetween(String value1, String value2) {
            addCriterion("PROCESS_FUNCTION not between", value1, value2, "processFunction");
            return (Criteria) this;
        }

        public Criteria andProcessParamIsNull() {
            addCriterion("PROCESS_PARAM is null");
            return (Criteria) this;
        }

        public Criteria andProcessParamIsNotNull() {
            addCriterion("PROCESS_PARAM is not null");
            return (Criteria) this;
        }

        public Criteria andProcessParamEqualTo(String value) {
            addCriterion("PROCESS_PARAM =", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamNotEqualTo(String value) {
            addCriterion("PROCESS_PARAM <>", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamGreaterThan(String value) {
            addCriterion("PROCESS_PARAM >", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamGreaterThanOrEqualTo(String value) {
            addCriterion("PROCESS_PARAM >=", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamLessThan(String value) {
            addCriterion("PROCESS_PARAM <", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamLessThanOrEqualTo(String value) {
            addCriterion("PROCESS_PARAM <=", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamLike(String value) {
            addCriterion("PROCESS_PARAM like", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamNotLike(String value) {
            addCriterion("PROCESS_PARAM not like", value, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamIn(List<String> values) {
            addCriterion("PROCESS_PARAM in", values, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamNotIn(List<String> values) {
            addCriterion("PROCESS_PARAM not in", values, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamBetween(String value1, String value2) {
            addCriterion("PROCESS_PARAM between", value1, value2, "processParam");
            return (Criteria) this;
        }

        public Criteria andProcessParamNotBetween(String value1, String value2) {
            addCriterion("PROCESS_PARAM not between", value1, value2, "processParam");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeIsNull() {
            addCriterion("TRIGGER_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeIsNotNull() {
            addCriterion("TRIGGER_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeEqualTo(String value) {
            addCriterion("TRIGGER_TYPE =", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeNotEqualTo(String value) {
            addCriterion("TRIGGER_TYPE <>", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeGreaterThan(String value) {
            addCriterion("TRIGGER_TYPE >", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TRIGGER_TYPE >=", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeLessThan(String value) {
            addCriterion("TRIGGER_TYPE <", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeLessThanOrEqualTo(String value) {
            addCriterion("TRIGGER_TYPE <=", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeLike(String value) {
            addCriterion("TRIGGER_TYPE like", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeNotLike(String value) {
            addCriterion("TRIGGER_TYPE not like", value, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeIn(List<String> values) {
            addCriterion("TRIGGER_TYPE in", values, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeNotIn(List<String> values) {
            addCriterion("TRIGGER_TYPE not in", values, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeBetween(String value1, String value2) {
            addCriterion("TRIGGER_TYPE between", value1, value2, "triggerType");
            return (Criteria) this;
        }

        public Criteria andTriggerTypeNotBetween(String value1, String value2) {
            addCriterion("TRIGGER_TYPE not between", value1, value2, "triggerType");
            return (Criteria) this;
        }

        public Criteria andCronExpressionIsNull() {
            addCriterion("CRON_EXPRESSION is null");
            return (Criteria) this;
        }

        public Criteria andCronExpressionIsNotNull() {
            addCriterion("CRON_EXPRESSION is not null");
            return (Criteria) this;
        }

        public Criteria andCronExpressionEqualTo(String value) {
            addCriterion("CRON_EXPRESSION =", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionNotEqualTo(String value) {
            addCriterion("CRON_EXPRESSION <>", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionGreaterThan(String value) {
            addCriterion("CRON_EXPRESSION >", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionGreaterThanOrEqualTo(String value) {
            addCriterion("CRON_EXPRESSION >=", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionLessThan(String value) {
            addCriterion("CRON_EXPRESSION <", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionLessThanOrEqualTo(String value) {
            addCriterion("CRON_EXPRESSION <=", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionLike(String value) {
            addCriterion("CRON_EXPRESSION like", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionNotLike(String value) {
            addCriterion("CRON_EXPRESSION not like", value, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionIn(List<String> values) {
            addCriterion("CRON_EXPRESSION in", values, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionNotIn(List<String> values) {
            addCriterion("CRON_EXPRESSION not in", values, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionBetween(String value1, String value2) {
            addCriterion("CRON_EXPRESSION between", value1, value2, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andCronExpressionNotBetween(String value1, String value2) {
            addCriterion("CRON_EXPRESSION not between", value1, value2, "cronExpression");
            return (Criteria) this;
        }

        public Criteria andJobStatusIsNull() {
            addCriterion("JOB_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andJobStatusIsNotNull() {
            addCriterion("JOB_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andJobStatusEqualTo(String value) {
            addCriterion("JOB_STATUS =", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotEqualTo(String value) {
            addCriterion("JOB_STATUS <>", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusGreaterThan(String value) {
            addCriterion("JOB_STATUS >", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_STATUS >=", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLessThan(String value) {
            addCriterion("JOB_STATUS <", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLessThanOrEqualTo(String value) {
            addCriterion("JOB_STATUS <=", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLike(String value) {
            addCriterion("JOB_STATUS like", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotLike(String value) {
            addCriterion("JOB_STATUS not like", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusIn(List<String> values) {
            addCriterion("JOB_STATUS in", values, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotIn(List<String> values) {
            addCriterion("JOB_STATUS not in", values, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusBetween(String value1, String value2) {
            addCriterion("JOB_STATUS between", value1, value2, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotBetween(String value1, String value2) {
            addCriterion("JOB_STATUS not between", value1, value2, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeIsNull() {
            addCriterion("REPEAT_TIME is null");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeIsNotNull() {
            addCriterion("REPEAT_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeEqualTo(Integer value) {
            addCriterion("REPEAT_TIME =", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeNotEqualTo(Integer value) {
            addCriterion("REPEAT_TIME <>", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeGreaterThan(Integer value) {
            addCriterion("REPEAT_TIME >", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("REPEAT_TIME >=", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeLessThan(Integer value) {
            addCriterion("REPEAT_TIME <", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeLessThanOrEqualTo(Integer value) {
            addCriterion("REPEAT_TIME <=", value, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeIn(List<Integer> values) {
            addCriterion("REPEAT_TIME in", values, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeNotIn(List<Integer> values) {
            addCriterion("REPEAT_TIME not in", values, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeBetween(Integer value1, Integer value2) {
            addCriterion("REPEAT_TIME between", value1, value2, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("REPEAT_TIME not between", value1, value2, "repeatTime");
            return (Criteria) this;
        }

        public Criteria andRepeatCountIsNull() {
            addCriterion("REPEAT_COUNT is null");
            return (Criteria) this;
        }

        public Criteria andRepeatCountIsNotNull() {
            addCriterion("REPEAT_COUNT is not null");
            return (Criteria) this;
        }

        public Criteria andRepeatCountEqualTo(Integer value) {
            addCriterion("REPEAT_COUNT =", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountNotEqualTo(Integer value) {
            addCriterion("REPEAT_COUNT <>", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountGreaterThan(Integer value) {
            addCriterion("REPEAT_COUNT >", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("REPEAT_COUNT >=", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountLessThan(Integer value) {
            addCriterion("REPEAT_COUNT <", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountLessThanOrEqualTo(Integer value) {
            addCriterion("REPEAT_COUNT <=", value, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountIn(List<Integer> values) {
            addCriterion("REPEAT_COUNT in", values, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountNotIn(List<Integer> values) {
            addCriterion("REPEAT_COUNT not in", values, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountBetween(Integer value1, Integer value2) {
            addCriterion("REPEAT_COUNT between", value1, value2, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andRepeatCountNotBetween(Integer value1, Integer value2) {
            addCriterion("REPEAT_COUNT not between", value1, value2, "repeatCount");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("START_TIME is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("START_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(String value) {
            addCriterion("START_TIME =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(String value) {
            addCriterion("START_TIME <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(String value) {
            addCriterion("START_TIME >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("START_TIME >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(String value) {
            addCriterion("START_TIME <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(String value) {
            addCriterion("START_TIME <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLike(String value) {
            addCriterion("START_TIME like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotLike(String value) {
            addCriterion("START_TIME not like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<String> values) {
            addCriterion("START_TIME in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<String> values) {
            addCriterion("START_TIME not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(String value1, String value2) {
            addCriterion("START_TIME between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(String value1, String value2) {
            addCriterion("START_TIME not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("END_TIME is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("END_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(String value) {
            addCriterion("END_TIME =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(String value) {
            addCriterion("END_TIME <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(String value) {
            addCriterion("END_TIME >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(String value) {
            addCriterion("END_TIME >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(String value) {
            addCriterion("END_TIME <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(String value) {
            addCriterion("END_TIME <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLike(String value) {
            addCriterion("END_TIME like", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotLike(String value) {
            addCriterion("END_TIME not like", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<String> values) {
            addCriterion("END_TIME in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<String> values) {
            addCriterion("END_TIME not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(String value1, String value2) {
            addCriterion("END_TIME between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(String value1, String value2) {
            addCriterion("END_TIME not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIsNull() {
            addCriterion("LAST_RUN_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIsNotNull() {
            addCriterion("LAST_RUN_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeEqualTo(String value) {
            addCriterion("LAST_RUN_TIME =", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotEqualTo(String value) {
            addCriterion("LAST_RUN_TIME <>", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeGreaterThan(String value) {
            addCriterion("LAST_RUN_TIME >", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_RUN_TIME >=", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeLessThan(String value) {
            addCriterion("LAST_RUN_TIME <", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeLessThanOrEqualTo(String value) {
            addCriterion("LAST_RUN_TIME <=", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeLike(String value) {
            addCriterion("LAST_RUN_TIME like", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotLike(String value) {
            addCriterion("LAST_RUN_TIME not like", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIn(List<String> values) {
            addCriterion("LAST_RUN_TIME in", values, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotIn(List<String> values) {
            addCriterion("LAST_RUN_TIME not in", values, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeBetween(String value1, String value2) {
            addCriterion("LAST_RUN_TIME between", value1, value2, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotBetween(String value1, String value2) {
            addCriterion("LAST_RUN_TIME not between", value1, value2, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeIsNull() {
            addCriterion("NEXT_FIRE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeIsNotNull() {
            addCriterion("NEXT_FIRE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeEqualTo(String value) {
            addCriterion("NEXT_FIRE_TIME =", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeNotEqualTo(String value) {
            addCriterion("NEXT_FIRE_TIME <>", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeGreaterThan(String value) {
            addCriterion("NEXT_FIRE_TIME >", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeGreaterThanOrEqualTo(String value) {
            addCriterion("NEXT_FIRE_TIME >=", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeLessThan(String value) {
            addCriterion("NEXT_FIRE_TIME <", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeLessThanOrEqualTo(String value) {
            addCriterion("NEXT_FIRE_TIME <=", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeLike(String value) {
            addCriterion("NEXT_FIRE_TIME like", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeNotLike(String value) {
            addCriterion("NEXT_FIRE_TIME not like", value, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeIn(List<String> values) {
            addCriterion("NEXT_FIRE_TIME in", values, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeNotIn(List<String> values) {
            addCriterion("NEXT_FIRE_TIME not in", values, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeBetween(String value1, String value2) {
            addCriterion("NEXT_FIRE_TIME between", value1, value2, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andNextFireTimeNotBetween(String value1, String value2) {
            addCriterion("NEXT_FIRE_TIME not between", value1, value2, "nextFireTime");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIsNull() {
            addCriterion("TRIGGER_STATE is null");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIsNotNull() {
            addCriterion("TRIGGER_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerStateEqualTo(String value) {
            addCriterion("TRIGGER_STATE =", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotEqualTo(String value) {
            addCriterion("TRIGGER_STATE <>", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateGreaterThan(String value) {
            addCriterion("TRIGGER_STATE >", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateGreaterThanOrEqualTo(String value) {
            addCriterion("TRIGGER_STATE >=", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLessThan(String value) {
            addCriterion("TRIGGER_STATE <", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLessThanOrEqualTo(String value) {
            addCriterion("TRIGGER_STATE <=", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateLike(String value) {
            addCriterion("TRIGGER_STATE like", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotLike(String value) {
            addCriterion("TRIGGER_STATE not like", value, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateIn(List<String> values) {
            addCriterion("TRIGGER_STATE in", values, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotIn(List<String> values) {
            addCriterion("TRIGGER_STATE not in", values, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateBetween(String value1, String value2) {
            addCriterion("TRIGGER_STATE between", value1, value2, "triggerState");
            return (Criteria) this;
        }

        public Criteria andTriggerStateNotBetween(String value1, String value2) {
            addCriterion("TRIGGER_STATE not between", value1, value2, "triggerState");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNull() {
            addCriterion("CONDUCT_USER is null");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNotNull() {
            addCriterion("CONDUCT_USER is not null");
            return (Criteria) this;
        }

        public Criteria andConductUserEqualTo(Integer value) {
            addCriterion("CONDUCT_USER =", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotEqualTo(Integer value) {
            addCriterion("CONDUCT_USER <>", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThan(Integer value) {
            addCriterion("CONDUCT_USER >", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThanOrEqualTo(Integer value) {
            addCriterion("CONDUCT_USER >=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThan(Integer value) {
            addCriterion("CONDUCT_USER <", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThanOrEqualTo(Integer value) {
            addCriterion("CONDUCT_USER <=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserIn(List<Integer> values) {
            addCriterion("CONDUCT_USER in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotIn(List<Integer> values) {
            addCriterion("CONDUCT_USER not in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserBetween(Integer value1, Integer value2) {
            addCriterion("CONDUCT_USER between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotBetween(Integer value1, Integer value2) {
            addCriterion("CONDUCT_USER not between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNull() {
            addCriterion("CONDUCT_TIME is null");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNotNull() {
            addCriterion("CONDUCT_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andConductTimeEqualTo(String value) {
            addCriterion("CONDUCT_TIME =", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotEqualTo(String value) {
            addCriterion("CONDUCT_TIME <>", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThan(String value) {
            addCriterion("CONDUCT_TIME >", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME >=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThan(String value) {
            addCriterion("CONDUCT_TIME <", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME <=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLike(String value) {
            addCriterion("CONDUCT_TIME like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotLike(String value) {
            addCriterion("CONDUCT_TIME not like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeIn(List<String> values) {
            addCriterion("CONDUCT_TIME in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotIn(List<String> values) {
            addCriterion("CONDUCT_TIME not in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME between", value1, value2, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME not between", value1, value2, "conductTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}