/**
 * @author liuyanlong
 *
 * 类功能描述：针对交易的挡板服务，根据交易码@Trader注解、实现此注解的挡板功能开发，在服务监控中将服务状态设为模拟测试即可路由到挡板功能
 *
 */
package com.njcb.ams.application.handle;
