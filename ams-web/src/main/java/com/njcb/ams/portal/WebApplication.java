package com.njcb.ams.portal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

import com.njcb.ams.support.autoconfigure.annotation.EnableAmsManagement;

/**
 * @author LOONG
 */
@SpringBootApplication(scanBasePackages = {"com.njcb"})
@MapperScan("com.njcb.*.mapper")
@EnableAmsManagement(scanBasePackages = { "com.njcb" })
public class WebApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

    public static void main(String[] args) {
    	SpringApplication.run(WebApplication.class, args);
    }
}
