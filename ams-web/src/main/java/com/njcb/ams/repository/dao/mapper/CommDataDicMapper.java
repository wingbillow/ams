package com.njcb.ams.repository.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.njcb.ams.repository.entity.CommDataDic;
import com.njcb.ams.repository.entity.CommDataDicExample;
import org.springframework.stereotype.Component;

@Component
public interface CommDataDicMapper extends BaseMapper<CommDataDic, CommDataDicExample, Integer> {
	List<CommDataDic> selectBySql(@Param("sql") String sql, @Param("record") CommDataDic record);
}