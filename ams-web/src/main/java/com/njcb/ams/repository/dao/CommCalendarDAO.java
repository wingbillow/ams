/*
 * CommCalendar.java
 * Copyright(C) trythis.cn
 * 2021年03月02日 15时43分19秒Created
 */
package com.njcb.ams.repository.dao;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommCalendarMapper;
import com.njcb.ams.repository.entity.CommCalendar;
import com.njcb.ams.repository.entity.CommCalendarExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CommCalendarDAO extends BaseMyBatisDAO<CommCalendar, CommCalendarExample, Integer> {
    @Autowired
    private CommCalendarMapper mapper;

    public CommCalendarDAO() {
        this.entityClass = CommCalendar.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public CommCalendarMapper getMapper() {
        return mapper;
    }

    public List<String> getSundayAndSatuday(Map<Object,Object> map) {
        return mapper.getSundayAndSatuday(map);
    }
}