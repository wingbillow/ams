package com.njcb.ams.repository.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.AuthRUserRoleMapper;
import com.njcb.ams.repository.entity.AuthRUserRole;
import com.njcb.ams.repository.entity.AuthRUserRoleExample;

/**
 * @author njcbkf045
 *
 *         类功能描述：
 *
 */
@Repository
public class AuthRUserRoleDAO extends BaseMyBatisDAO<AuthRUserRole, AuthRUserRoleExample, Integer> {

	public AuthRUserRoleDAO() {
		this.entityClass = AuthRUserRole.class;
	}

	@Autowired
	private AuthRUserRoleMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public AuthRUserRoleMapper getMapper() {
		return mapper;
	}

	public List<Integer> findByRoleIdByUserId(Integer userId) {
		AuthRUserRoleExample example = new AuthRUserRoleExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<AuthRUserRole> authList = mapper.selectByExample(example);
		List<Integer> list = new ArrayList<Integer>();
		for (AuthRUserRole userRole : authList) {
			list.add(userRole.getRoleId());
		}
		return list;
	}

}
