/*
 * CommReportMetaMapper.java
 * Copyright(C) trythis.cn
 * 2020年01月10日 11时05分42秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommReportMeta;
import com.njcb.ams.repository.entity.CommReportMetaExample;
import org.springframework.stereotype.Component;

@Component
public interface CommReportMetaMapper extends BaseMapper<CommReportMeta, CommReportMetaExample, Integer> {
}