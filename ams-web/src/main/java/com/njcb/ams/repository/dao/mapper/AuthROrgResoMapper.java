package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.AuthROrgReso;
import com.njcb.ams.repository.entity.AuthROrgResoExample;
import org.springframework.stereotype.Component;

@Component
public interface AuthROrgResoMapper extends BaseMapper<AuthROrgReso, AuthROrgResoExample, Integer> {

}