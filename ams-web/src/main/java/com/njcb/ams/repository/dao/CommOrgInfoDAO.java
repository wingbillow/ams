package com.njcb.ams.repository.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommOrgInfoMapper;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommOrgInfoExample;

@Repository
public class CommOrgInfoDAO extends BaseMyBatisDAO<CommOrgInfo, CommOrgInfoExample, Integer> {

	public CommOrgInfoDAO() {
		this.entityClass = CommOrgInfo.class;
	}

	@Autowired
	private CommOrgInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public CommOrgInfoMapper getMapper() {
		return mapper;
	}


	public List<CommOrgInfo> selectAll() {
		return mapper.selectByExample(new CommOrgInfoExample());
	}

	public CommOrgInfo selectByOrgNo(String orgNo) {
		CommOrgInfoExample example = new CommOrgInfoExample();
		example.createCriteria().andOrgNoEqualTo(orgNo);
		return selectOneByExample(example);
	}

	public List<CommOrgInfo> selectByParentId(Integer parentId) {
		CommOrgInfoExample example = new CommOrgInfoExample();
		example.createCriteria().andParentIdEqualTo(parentId);
		return selectByExample(example);
	}

	/**
	 * @param orgInfo
	 */
	@Override
    public int saveOrUpdateByPrimaryKey(CommOrgInfo orgInfo) {
		if (null != orgInfo.getId() && 0 != orgInfo.getId()) {
			return getMapper().updateByPrimaryKey(orgInfo);
		} else {
			return getMapper().insert(orgInfo);
		}
	}

	/**
	 * 取本机构及所属机构ID
	 * 
	 * @param parentId
	 * @return
	 */
	public List<Integer> selectSelfAndSubById(Integer id) {
		return mapper.selectSelfAndSubById(id);
	}
	
	/**
	 * 取本机构及所属机构信息
	 * 
	 * @param parentId
	 * @return
	 */
	public List<CommOrgInfo> selectEntitySelfAndSubById(Integer id) {
		return mapper.selectEntitySelfAndSubById(id);
	}
	
	/**
	 * 取本机构及所属机构信息
	 * 
	 * @param orgNo
	 * @return
	 */
	public List<CommOrgInfo> selectEntitySelfAndSubByOrgNo(String orgNo) {
		CommOrgInfo orgInfo = selectByOrgNo(orgNo);
		return mapper.selectEntitySelfAndSubById(orgInfo.getId());
	}
}
