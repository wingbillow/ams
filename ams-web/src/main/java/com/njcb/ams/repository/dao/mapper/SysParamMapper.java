package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysParam;
import com.njcb.ams.repository.entity.SysParamExample;

public interface SysParamMapper extends BaseMapper<SysParam, SysParamExample, Integer> {

}