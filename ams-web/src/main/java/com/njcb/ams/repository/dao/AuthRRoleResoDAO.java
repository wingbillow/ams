package com.njcb.ams.repository.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.AuthRRoleResoMapper;
import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.repository.entity.AuthRRoleResoExample;

/**
 * @author njcbkf045
 *
 *         类功能描述：
 *
 */
@Repository
public class AuthRRoleResoDAO extends BaseMyBatisDAO<AuthRRoleReso, AuthRRoleResoExample, Integer> {

	public AuthRRoleResoDAO() {
		this.entityClass = AuthRRoleReso.class;
	}

	@Autowired
	private AuthRRoleResoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public AuthRRoleResoMapper getMapper() {
		return mapper;
	}

}
