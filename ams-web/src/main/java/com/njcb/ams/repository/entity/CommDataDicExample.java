package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class CommDataDicExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommDataDicExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDataNoIsNull() {
            addCriterion("DATA_NO is null");
            return (Criteria) this;
        }

        public Criteria andDataNoIsNotNull() {
            addCriterion("DATA_NO is not null");
            return (Criteria) this;
        }

        public Criteria andDataNoEqualTo(String value) {
            addCriterion("DATA_NO =", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoNotEqualTo(String value) {
            addCriterion("DATA_NO <>", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoGreaterThan(String value) {
            addCriterion("DATA_NO >", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_NO >=", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoLessThan(String value) {
            addCriterion("DATA_NO <", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoLessThanOrEqualTo(String value) {
            addCriterion("DATA_NO <=", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoLike(String value) {
            addCriterion("DATA_NO like", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoNotLike(String value) {
            addCriterion("DATA_NO not like", value, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoIn(List<String> values) {
            addCriterion("DATA_NO in", values, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoNotIn(List<String> values) {
            addCriterion("DATA_NO not in", values, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoBetween(String value1, String value2) {
            addCriterion("DATA_NO between", value1, value2, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNoNotBetween(String value1, String value2) {
            addCriterion("DATA_NO not between", value1, value2, "dataNo");
            return (Criteria) this;
        }

        public Criteria andDataNameIsNull() {
            addCriterion("DATA_NAME is null");
            return (Criteria) this;
        }

        public Criteria andDataNameIsNotNull() {
            addCriterion("DATA_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andDataNameEqualTo(String value) {
            addCriterion("DATA_NAME =", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameNotEqualTo(String value) {
            addCriterion("DATA_NAME <>", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameGreaterThan(String value) {
            addCriterion("DATA_NAME >", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_NAME >=", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameLessThan(String value) {
            addCriterion("DATA_NAME <", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameLessThanOrEqualTo(String value) {
            addCriterion("DATA_NAME <=", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameLike(String value) {
            addCriterion("DATA_NAME like", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameNotLike(String value) {
            addCriterion("DATA_NAME not like", value, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameIn(List<String> values) {
            addCriterion("DATA_NAME in", values, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameNotIn(List<String> values) {
            addCriterion("DATA_NAME not in", values, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameBetween(String value1, String value2) {
            addCriterion("DATA_NAME between", value1, value2, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataNameNotBetween(String value1, String value2) {
            addCriterion("DATA_NAME not between", value1, value2, "dataName");
            return (Criteria) this;
        }

        public Criteria andDataTypeIsNull() {
            addCriterion("DATA_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andDataTypeIsNotNull() {
            addCriterion("DATA_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andDataTypeEqualTo(String value) {
            addCriterion("DATA_TYPE =", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeNotEqualTo(String value) {
            addCriterion("DATA_TYPE <>", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeGreaterThan(String value) {
            addCriterion("DATA_TYPE >", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_TYPE >=", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeLessThan(String value) {
            addCriterion("DATA_TYPE <", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeLessThanOrEqualTo(String value) {
            addCriterion("DATA_TYPE <=", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeLike(String value) {
            addCriterion("DATA_TYPE like", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeNotLike(String value) {
            addCriterion("DATA_TYPE not like", value, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeIn(List<String> values) {
            addCriterion("DATA_TYPE in", values, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeNotIn(List<String> values) {
            addCriterion("DATA_TYPE not in", values, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeBetween(String value1, String value2) {
            addCriterion("DATA_TYPE between", value1, value2, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeNotBetween(String value1, String value2) {
            addCriterion("DATA_TYPE not between", value1, value2, "dataType");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameIsNull() {
            addCriterion("DATA_TYPE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameIsNotNull() {
            addCriterion("DATA_TYPE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameEqualTo(String value) {
            addCriterion("DATA_TYPE_NAME =", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameNotEqualTo(String value) {
            addCriterion("DATA_TYPE_NAME <>", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameGreaterThan(String value) {
            addCriterion("DATA_TYPE_NAME >", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_TYPE_NAME >=", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameLessThan(String value) {
            addCriterion("DATA_TYPE_NAME <", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameLessThanOrEqualTo(String value) {
            addCriterion("DATA_TYPE_NAME <=", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameLike(String value) {
            addCriterion("DATA_TYPE_NAME like", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameNotLike(String value) {
            addCriterion("DATA_TYPE_NAME not like", value, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameIn(List<String> values) {
            addCriterion("DATA_TYPE_NAME in", values, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameNotIn(List<String> values) {
            addCriterion("DATA_TYPE_NAME not in", values, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameBetween(String value1, String value2) {
            addCriterion("DATA_TYPE_NAME between", value1, value2, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataTypeNameNotBetween(String value1, String value2) {
            addCriterion("DATA_TYPE_NAME not between", value1, value2, "dataTypeName");
            return (Criteria) this;
        }

        public Criteria andDataAttrIsNull() {
            addCriterion("DATA_ATTR is null");
            return (Criteria) this;
        }

        public Criteria andDataAttrIsNotNull() {
            addCriterion("DATA_ATTR is not null");
            return (Criteria) this;
        }

        public Criteria andDataAttrEqualTo(String value) {
            addCriterion("DATA_ATTR =", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrNotEqualTo(String value) {
            addCriterion("DATA_ATTR <>", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrGreaterThan(String value) {
            addCriterion("DATA_ATTR >", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_ATTR >=", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrLessThan(String value) {
            addCriterion("DATA_ATTR <", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrLessThanOrEqualTo(String value) {
            addCriterion("DATA_ATTR <=", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrLike(String value) {
            addCriterion("DATA_ATTR like", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrNotLike(String value) {
            addCriterion("DATA_ATTR not like", value, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrIn(List<String> values) {
            addCriterion("DATA_ATTR in", values, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrNotIn(List<String> values) {
            addCriterion("DATA_ATTR not in", values, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrBetween(String value1, String value2) {
            addCriterion("DATA_ATTR between", value1, value2, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataAttrNotBetween(String value1, String value2) {
            addCriterion("DATA_ATTR not between", value1, value2, "dataAttr");
            return (Criteria) this;
        }

        public Criteria andDataLogicIsNull() {
            addCriterion("DATA_LOGIC is null");
            return (Criteria) this;
        }

        public Criteria andDataLogicIsNotNull() {
            addCriterion("DATA_LOGIC is not null");
            return (Criteria) this;
        }

        public Criteria andDataLogicEqualTo(String value) {
            addCriterion("DATA_LOGIC =", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicNotEqualTo(String value) {
            addCriterion("DATA_LOGIC <>", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicGreaterThan(String value) {
            addCriterion("DATA_LOGIC >", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_LOGIC >=", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicLessThan(String value) {
            addCriterion("DATA_LOGIC <", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicLessThanOrEqualTo(String value) {
            addCriterion("DATA_LOGIC <=", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicLike(String value) {
            addCriterion("DATA_LOGIC like", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicNotLike(String value) {
            addCriterion("DATA_LOGIC not like", value, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicIn(List<String> values) {
            addCriterion("DATA_LOGIC in", values, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicNotIn(List<String> values) {
            addCriterion("DATA_LOGIC not in", values, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicBetween(String value1, String value2) {
            addCriterion("DATA_LOGIC between", value1, value2, "dataLogic");
            return (Criteria) this;
        }

        public Criteria andDataLogicNotBetween(String value1, String value2) {
            addCriterion("DATA_LOGIC not between", value1, value2, "dataLogic");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}