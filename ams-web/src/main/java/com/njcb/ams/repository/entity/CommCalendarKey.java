/*
 * CommCalendarKey.java
 * Copyright(C) trythis.cn
 * 2021年03月02日 15时43分19秒Created
 */
package com.njcb.ams.repository.entity;

public class CommCalendarKey {
    private String sysId;

    private String dataStr;

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getDataStr() {
        return dataStr;
    }

    public void setDataStr(String dataStr) {
        this.dataStr = dataStr;
    }
}