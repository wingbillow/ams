package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.AuthRUserRole;
import com.njcb.ams.repository.entity.AuthRUserRoleExample;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public interface AuthRUserRoleMapper extends BaseMapper<AuthRUserRole, AuthRUserRoleExample, Integer> {
}