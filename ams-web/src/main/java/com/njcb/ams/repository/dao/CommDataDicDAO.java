package com.njcb.ams.repository.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommDataDicMapper;
import com.njcb.ams.repository.entity.CommDataDic;
import com.njcb.ams.repository.entity.CommDataDicExample;
import com.njcb.ams.portal.SysBaseDefine;

@Repository
public class CommDataDicDAO extends BaseMyBatisDAO<CommDataDic, CommDataDicExample, Integer> {
	public CommDataDicDAO() {
		this.entityClass = CommDataDic.class;
	}

	@Autowired
	private CommDataDicMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public CommDataDicMapper getMapper() {
		return mapper;
	}

	public List<CommDataDic> findAll() {
		CommDataDicExample example01 = new CommDataDicExample();
		example01.createCriteria().andDataAttrEqualTo(SysBaseDefine.DATADIC_TYPE_LOCAL_01);
		List<CommDataDic> ddlocal = selectByExample(example01);
		CommDataDicExample example02 = new CommDataDicExample();
		example02.createCriteria().andDataAttrEqualTo(SysBaseDefine.DATADIC_TYPE_EXTER_02);
		List<CommDataDic> ddlogic = selectByExample(example02);
		for (CommDataDic dataDic : ddlogic) {
			String logicSql = dataDic.getDataLogic();
			List<CommDataDic> subList = mapper.selectBySql(logicSql, dataDic);
			ddlocal.addAll(subList);
		}
		return ddlocal;
	}

	public List<CommDataDic> findDataNameByType(String dataType) {
		CommDataDic dataDic = new CommDataDic();
		dataDic.setDataType(dataType);
		return findDataNameByType(dataDic);
	}

	public List<CommDataDic> findDataNameByType(CommDataDic dataDic) {

		CommDataDicExample example01 = new CommDataDicExample();
		CommDataDicExample.Criteria criteria1 = example01.createCriteria().andDataAttrEqualTo(SysBaseDefine.DATADIC_TYPE_LOCAL_01);
		if (null != dataDic && StringUtils.isNotEmpty(dataDic.getDataType())) {
			criteria1.andDataTypeLike(dataDic.getDataType());
		}
		if (null != dataDic && StringUtils.isNotEmpty(dataDic.getDataTypeName())) {
			criteria1.andDataTypeNameLike(dataDic.getDataTypeName());
		}
		List<CommDataDic> ddlocal = selectByExample(example01);
		CommDataDicExample example02 = new CommDataDicExample();
		CommDataDicExample.Criteria criteria2 = example02.createCriteria().andDataAttrEqualTo(SysBaseDefine.DATADIC_TYPE_EXTER_02);
		if (null != dataDic && StringUtils.isNotEmpty(dataDic.getDataType())) {
			criteria2.andDataTypeLike(dataDic.getDataType());
		}
		if (null != dataDic && StringUtils.isNotEmpty(dataDic.getDataTypeName())) {
			criteria2.andDataTypeNameLike(dataDic.getDataTypeName());
		}
		List<CommDataDic> ddlogic = selectByExample(example02);
		for (CommDataDic itdataDic : ddlogic) {
			String logicSql = itdataDic.getDataLogic();
			List<CommDataDic> subList = mapper.selectBySql(logicSql, itdataDic);
			ddlocal.addAll(subList);
		}
		return ddlocal;
	}

	@Override
    public int saveOrUpdateByPrimaryKey(CommDataDic dataDic) {
		if (null != dataDic.getId()) {
			return getMapper().updateByPrimaryKey(dataDic);
		} else {
			return getMapper().insert(dataDic);
		}
	}

}
