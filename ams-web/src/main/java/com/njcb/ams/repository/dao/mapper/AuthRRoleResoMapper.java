package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.repository.entity.AuthRRoleResoExample;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public interface AuthRRoleResoMapper extends BaseMapper<AuthRRoleReso, AuthRRoleResoExample, Integer> {
}