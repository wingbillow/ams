package com.njcb.ams.repository.dao.mapper;

import java.util.List;

import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.CommOrgInfoExample;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public interface CommOrgInfoMapper extends BaseMapper<CommOrgInfo, CommOrgInfoExample, Integer> {
	List<Integer> selectSelfAndSubById(Integer parentId);

	List<CommOrgInfo> selectEntitySelfAndSubById(Integer parentId);
}