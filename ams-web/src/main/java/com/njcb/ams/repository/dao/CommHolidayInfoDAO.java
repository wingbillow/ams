package com.njcb.ams.repository.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.BaseMapper;
import com.njcb.ams.repository.dao.mapper.CommHolidayInfoMapper;
import com.njcb.ams.repository.entity.CommHolidayInfo;
import com.njcb.ams.repository.entity.CommHolidayInfoExample;

public class CommHolidayInfoDAO extends BaseMyBatisDAO<CommHolidayInfo, CommHolidayInfoExample, Integer> {

	public CommHolidayInfoDAO() {
		this.entityClass = CommHolidayInfo.class;
	}

	@Autowired
	private CommHolidayInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public BaseMapper<CommHolidayInfo, CommHolidayInfoExample, Integer> getMapper() {
		return mapper;
	}

	public CommHolidayInfo findByYear(String year) {
		CommHolidayInfoExample example = new CommHolidayInfoExample();
		example.createCriteria().andYearEqualTo(year);
		List<CommHolidayInfo> userList = mapper.selectByExample(example);
		if (userList.size() == 0) {
			return null;
		}
		return (CommHolidayInfo) userList.get(0);
	}

}
