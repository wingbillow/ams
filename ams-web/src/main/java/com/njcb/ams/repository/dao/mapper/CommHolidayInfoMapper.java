package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommHolidayInfo;
import com.njcb.ams.repository.entity.CommHolidayInfoExample;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public interface CommHolidayInfoMapper extends BaseMapper<CommHolidayInfo, CommHolidayInfoExample, Integer> {
}