package com.njcb.ams.store.mail.bean;

import com.njcb.ams.util.AmsStringUtils;

import java.io.Serializable;

public class Email implements Serializable {
	private static final long serialVersionUID = -8776256606336290889L;

	private UserGroups userGroups;

	private String addresses;

	// 抄送
	private String cc;

	private String subject;

	private String content;

	private transient MultipartFile[] attachment = new MultipartFile[0];

	public Email() {
	}
	
	public Email(String addresses,String subject) {
		this.addresses = addresses;
		this.subject = subject;
	}

	public Email(String addresses, String subject, String content) {
		this.addresses = addresses;
		this.subject = subject;
		this.content = content;
	}
	
	/**
	 * 解析邮件地址
	 */
	public String[] getAddress() {
		if (!AmsStringUtils.hasLength(this.addresses)) {
			return null;
		}
		addresses = addresses.trim();
		addresses = addresses.replaceAll("；", ";");
		addresses = addresses.replaceAll(" ", ";");
		addresses = addresses.replaceAll(",", ";");
		addresses = addresses.replaceAll("，", ";");
		addresses = addresses.replaceAll("\\|", ";");
		return addresses.split(";");
	}

	public UserGroups getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(UserGroups userGroups) {
		this.userGroups = userGroups;
	}

	public String getAddresses() {
		return addresses;
	}

	public void setAddresses(String addresses) {
		this.addresses = addresses;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public MultipartFile[] getAttachment() {
		return attachment;
	}

	public void setAttachment(MultipartFile[] attachment) {
		this.attachment = attachment;
	}

}
