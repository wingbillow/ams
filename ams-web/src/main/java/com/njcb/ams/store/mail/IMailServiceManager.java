package com.njcb.ams.store.mail;

import com.njcb.ams.store.mail.bean.Email;

import javax.mail.MessagingException;

public interface IMailServiceManager {
    public void sendMail(Email email) throws MessagingException;

    public void sendMailByAsynchronousMode(final Email email);

    public void sendMailBySynchronizationMode(Email email) throws MessagingException;
}
