package com.njcb.ams.store.esbmodule;

/**
 * webservice相关常量定义
 * @author LOONG
 */
public class Constants {
    /**
     * 接受语言
     */
    public static final String ACCEPT_LANG = "zh_CN";

    /**
     * 服务版本号 默认01.000
     */
    public static final String VERSION_DEFAULT = "01.000";

    /**
     * 请求运行优先级 默认00
     */

    public static final String PRIORITY_DEFAULT = "00";

    /**
     * 报文长度
     */
    public static final String PKGLENGTH_DEFAULT = "00000000";

    /**
     * MAC校验值
     */
    public static final String MAC_DEFAULT = "";

    /**
     * CXF临时存储日志属性变量
     */
    public static final String CXF_LOG_STORAGE_PROPERTY = "WebServiceClient_SysTradeLog";
}
