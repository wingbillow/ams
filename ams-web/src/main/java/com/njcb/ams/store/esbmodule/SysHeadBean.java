package com.njcb.ams.store.esbmodule;

public class SysHeadBean {

    protected String pkgLength;
    protected String tmlCd;
    protected String serviceID;
    protected String serviceId;
    protected String channelID;
    protected String channelId;
    protected String legOrgID;
    protected String legOrgId;
    protected String reqDate;
    protected String reqTime;
    protected String mac;
    protected String version;
    protected String priority;
    protected String reqSysID;
    protected String reqSysId;
    protected String domainRef;
    protected String acceptLang;
    protected String globalSeq;
    protected String orgSysID;
    protected String orgSysId;
    protected String respSeq;
    protected String respDate;
    protected String respTime;
    protected String transStatus;
    protected String retCode;
    protected String retMsg;
    protected String respSysID;
    protected String respSysId;
    protected String trackCode;
    protected String reseFlg;
    protected String reseFld;
    protected String svcScn;
    protected String reqSeq;

    public String getPkgLength() {
        return pkgLength;
    }

    public void setPkgLength(String value) {
        this.pkgLength = value;
    }

    public String getTmlCd() {
        return tmlCd;
    }

    public void setTmlCd(String value) {
        this.tmlCd = value;
    }

    public String getServiceID() {
        if(null == serviceID){
            serviceID = serviceId;
        }
        return serviceID;
    }

    public void setServiceID(String value) {
        this.serviceID = value;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getChannelID() {
        if(null == channelID){
            channelID = channelId;
        }
        return channelID;
    }

    public void setChannelID(String value) {
        this.channelID = value;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getLegOrgID() {
        if(null == legOrgID){
            legOrgID = legOrgId;
        }
        return legOrgID;
    }

    public void setLegOrgID(String value) {
        this.legOrgID = value;
    }

    public void setLegOrgId(String legOrgId) {
        this.legOrgId = legOrgId;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String value) {
        this.reqDate = value;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String value) {
        this.reqTime = value;
    }

    public String getMAC() {
        return mac;
    }

    public void setMAC(String value) {
        this.mac = value;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String value) {
        this.priority = value;
    }

    public String getReqSysID() {
        if(null == reqSysID){
            reqSysID = reqSysId;
        }
        return reqSysID;
    }

    public void setReqSysID(String reqSysID) {
        this.reqSysID = reqSysID;
    }

    public void setReqSysId(String reqSysId) {
        this.reqSysId = reqSysId;
    }

    public String getDomainRef() {
        return domainRef;
    }

    public void setDomainRef(String value) {
        this.domainRef = value;
    }

    public String getAcceptLang() {
        return acceptLang;
    }

    public void setAcceptLang(String value) {
        this.acceptLang = value;
    }

    public String getGlobalSeq() {
        return globalSeq;
    }

    public void setGlobalSeq(String value) {
        this.globalSeq = value;
    }

    public String getOrgSysID() {
        if(null == orgSysID){
            orgSysID = orgSysId;
        }
        return orgSysID;
    }

    public void setOrgSysID(String value) {
        this.orgSysID = value;
    }

    public void setOrgSysId(String orgSysId) {
        this.orgSysId = orgSysId;
    }

    public String getRespSeq() {
        return respSeq;
    }

    public void setRespSeq(String value) {
        this.respSeq = value;
    }

    public String getRespDate() {
        return respDate;
    }

    public void setRespDate(String value) {
        this.respDate = value;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String value) {
        this.respTime = value;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String value) {
        this.transStatus = value;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String value) {
        this.retCode = value;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String value) {
        this.retMsg = value;
    }

    public String getRespSysID() {
        if(null == respSysID){
            respSysID = respSysId;
        }
        return respSysID;
    }

    public void setRespSysID(String respSysID) {
        this.respSysID = respSysID;
    }

    public void setRespSysId(String respSysId) {
        this.respSysId = respSysId;
    }

    public String getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(String value) {
        this.trackCode = value;
    }

    public String getReseFlg() {
        return reseFlg;
    }

    public void setReseFlg(String value) {
        this.reseFlg = value;
    }

    public String getReseFld() {
        return reseFld;
    }

    public void setReseFld(String value) {
        this.reseFld = value;
    }

    public String getSvcScn() {
        return svcScn;
    }

    public void setSvcScn(String value) {
        this.svcScn = value;
    }

    public String getReqSeq() {
        return reqSeq;
    }

    public void setReqSeq(String value) {
        this.reqSeq = value;
    }

}
