package com.njcb.ams.store.esbmodule;

import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.pojo.bo.Head;

/**
 * @author LOONG
 */
public class EsbHeadUtil extends Head {
	private static String ESB_HEAD_UTIL = "ESB_HEAD_UTIL";
	
	private String channelID;
	
	private String serviceID;
	
	private String svcScn;
	
	private String reqSeq;
	
	private String globalSeq;
	
	private String reqSysID;
	
	private String retMsg;

	private boolean isHeadProxy = true;
	
	private SysHeadBean srcSysHead;

	public static void setIsHeadProxy(boolean isHeadProxy) {
		EsbHeadUtil head = commSetHead();
		head.isHeadProxy = isHeadProxy;
	}

	public static boolean getIsHeadProxy() {
		EsbHeadUtil head = commSetHead();
		return head.isHeadProxy;
	}
	
	public static void setReqSeq(String reqSeq) {
		EsbHeadUtil head = commSetHead();
		head.reqSeq = reqSeq;
	}
	
	public static String getReqSeq() {
		EsbHeadUtil head = commSetHead();
		return head.reqSeq;
	}
	
	public static void setGlobalSeq(String globalSeq) {
		EsbHeadUtil head = commSetHead();
		head.globalSeq = globalSeq;
	}
	
	public static String getGlobalSeq() {
		EsbHeadUtil head = commSetHead();
		return head.globalSeq;
	}

	public static String getChannelID() {
		return commSetHead().channelID;
	}
	
	public static void setChannelID(String channelID) {
		EsbHeadUtil head = commSetHead();
		head.channelID = channelID;
	}
	
	public static String getServiceID() {
		return commSetHead().serviceID;
	}

	public static void setServiceID(String serviceID) {
		EsbHeadUtil head = commSetHead();
		head.serviceID = serviceID;
	}

	public static String getSvcScn() {
		return commSetHead().svcScn;
	}

	public static void setSvcScn(String svcScn) {
		EsbHeadUtil head = commSetHead();
		head.svcScn = svcScn;
	}
	
	public static String getReqSysID() {
		return commSetHead().reqSysID;
	}

	public static void setReqSysID(String reqSysID) {
		EsbHeadUtil head = commSetHead();
		head.reqSysID = reqSysID;
	}
	
	public static String getRetMsg() {
		return commSetHead().retMsg;
	}

	public static void setRetMsg(String retMsg) {
		EsbHeadUtil head = commSetHead();
		head.retMsg = retMsg;
	}
	
	public static SysHeadBean getSrcSysHead() {
		return commSetHead().srcSysHead;
	}

	public static void setSrcSysHead(SysHeadBean srcSysHead) {
		EsbHeadUtil head = commSetHead();
		head.srcSysHead = srcSysHead;
	}

	/**
	 * 方法功能描述：初始化EsbHeadUtil
	 * @return
	 */
	private static EsbHeadUtil commSetHead() {
		if (DataBus.isNotNull()) {
			EsbHeadUtil head = (EsbHeadUtil) DataBus.getAttribute(ESB_HEAD_UTIL);
			if (null == head) {
				head = new EsbHeadUtil();
			}
			DataBus.setAttribute(ESB_HEAD_UTIL, head);
			return head;
		}else{
			EsbHeadUtil head = new EsbHeadUtil();
			DataBus.setAttribute(ESB_HEAD_UTIL, head);
			return head;
		}
	}
	
}
