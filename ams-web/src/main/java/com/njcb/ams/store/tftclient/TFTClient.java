package com.njcb.ams.store.tftclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TFTClient {

	private static final Logger logger = LoggerFactory.getLogger(TFTClient.class);

	/**
	 * 下载
	 * @param iHostNo
	 * @param sSrvFileName
	 * @param sClientFileName
	 * @param ddr
	 * @return
	 * @throws Exception
	 */
	public static String tftDownLoad(String iHostNo, String sSrvFileName, String sClientFileName, String ddr) throws Exception {
		logger.info("下载文件");
		String sysType = (String) System.getProperties().get("os.name");
		return sysType;
	}
	
	/**
	 * 上传
	 * @param iHostNo
	 * @param sSrvFileName
	 * @param sClientFileName
	 * @param ddr
	 * @return
	 * @throws Exception
	 */
	public static String tftUp(String iHostNo, String sSrvFileName, String sClientFileName, String ddr) throws Exception {
		logger.info("上传文件");
		String sysType = (String) System.getProperties().get("os.name");
		return sysType;
	}

}