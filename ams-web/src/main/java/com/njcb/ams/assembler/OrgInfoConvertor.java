package com.njcb.ams.assembler;

import org.springframework.stereotype.Component;

import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.pojo.vo.OrgInfoQueryInput;
import com.njcb.ams.pojo.vo.OrgInfoQueryOutput;
import com.njcb.ams.pojo.vo.OrgInfoSaveInput;
import com.njcb.ams.util.AmsBeanUtils;

@Component
public class OrgInfoConvertor implements Convertor<Object, Object, Object, Object> {
	public CommOrgInfo inputQueryVoToEntity(OrgInfoQueryInput input){
		CommOrgInfo clientObject = new CommOrgInfo();
		AmsBeanUtils.copyProperties(clientObject, input);
		return clientObject;
	}
	
	public OrgInfoQueryOutput entityToOutputQueryVo(CommOrgInfo commOrgInfo){
		OrgInfoQueryOutput clientObject = new OrgInfoQueryOutput();
		AmsBeanUtils.copyProperties(clientObject, commOrgInfo);
		return clientObject;
	}
	
	public CommOrgInfo inputSaveVoToEntity(OrgInfoSaveInput input){
		CommOrgInfo clientObject = new CommOrgInfo();
		AmsBeanUtils.copyProperties(clientObject, input);
		return clientObject;
	}
}
