package com.njcb.ams.assembler;

import com.njcb.ams.pojo.bo.DataDicConfig;
import com.njcb.ams.pojo.dto.SysTradeLogDTO;
import com.njcb.ams.repository.entity.CommDataDic;
import com.njcb.ams.repository.entity.SysTradeLog;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public class DataDicConvertor {

	private static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

	static{
		mapperFactory.classMap(CommDataDic.class,DataDicConfig.class)
				.byDefault().register();
	}

    public CommDataDic dtoToEntity(DataDicConfig clientObject) {
		CommDataDic entityObject = mapperFactory.getMapperFacade().map(clientObject,CommDataDic.class);
		entityObject.setDataAttr("01");
		return entityObject;
	}
}
