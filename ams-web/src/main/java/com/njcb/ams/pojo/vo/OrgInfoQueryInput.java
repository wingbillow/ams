package com.njcb.ams.pojo.vo;

import com.njcb.ams.store.page.Page;

import io.swagger.annotations.ApiModelProperty;

public class OrgInfoQueryInput extends Page{

	@ApiModelProperty(value = "机构号")
	private String orgNo;

	@ApiModelProperty(value = "机构名称")
	private String orgName;

	@ApiModelProperty(value = "机构级别")
	private Integer orgGrade;

	@ApiModelProperty(value = "机构状态")
	private String status;

	@ApiModelProperty(value = "机构类型")
	private String orgType;


	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getOrgGrade() {
		return orgGrade;
	}

	public void setOrgGrade(Integer orgGrade) {
		this.orgGrade = orgGrade;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	@Override
	public String toString() {
		return "[" + orgNo + "]@[" + orgName + "]";
	}
}