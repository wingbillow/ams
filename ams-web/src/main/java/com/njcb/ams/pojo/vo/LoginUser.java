package com.njcb.ams.pojo.vo;

import java.util.List;

public class LoginUser {
	private String token;
	//系统名称
	private String sysName;
	//交易日期
	private String busiDate;
	//用户登录名
	private String loginName;
	//用户姓名
	private String userName;
	//机构名称
	private String orgnName;
	//用户ID
	private String userId;
	//用户角色列表
	private List<String> roleCodes;

	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getSysName() {
		return sysName;
	}
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	
	public String getBusiDate() {
		return busiDate;
	}
	
	public void setBusiDate(String busiDate) {
		this.busiDate = busiDate;
	}

	public String getLoginName() {
		return loginName;
	}
	
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOrgnName() {
		return orgnName;
	}
	public void setOrgnName(String orgnName) {
		this.orgnName = orgnName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setRoleCodes(List<String> roleCodes) {
		this.roleCodes = roleCodes;
	}
	public List<String> getRoleCodes() {
		return roleCodes;
	}
}
