package com.njcb.ams.pojo.vo;

import com.njcb.ams.repository.entity.AuthRUserRole;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author LOONG
 */
public class UserRoleUpdate {
    @ApiModelProperty(value = "插入集合")
    private List<AuthRUserRole> insertList;
    @ApiModelProperty(value = "删除集合")
    private List<AuthRUserRole> deleteList;

    public List<AuthRUserRole> getInsertList() {
        return insertList;
    }

    public void setInsertList(List<AuthRUserRole> insertList) {
        this.insertList = insertList;
    }

    public List<AuthRUserRole> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<AuthRUserRole> deleteList) {
        this.deleteList = deleteList;
    }
}
