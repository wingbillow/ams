package com.njcb.ams.pojo.vo;

import com.njcb.ams.repository.entity.AuthROrgReso;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author LOONG
 */
public class OrgResoUpdate {
    @ApiModelProperty(value = "插入集合")
    private List<AuthROrgReso> insertList;
    @ApiModelProperty(value = "删除集合")
    private List<AuthROrgReso> deleteList;

    public List<AuthROrgReso> getInsertList() {
        return insertList;
    }

    public void setInsertList(List<AuthROrgReso> insertList) {
        this.insertList = insertList;
    }

    public List<AuthROrgReso> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<AuthROrgReso> deleteList) {
        this.deleteList = deleteList;
    }
}
