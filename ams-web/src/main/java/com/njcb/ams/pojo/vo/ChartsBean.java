package com.njcb.ams.pojo.vo;

import java.util.List;
import java.util.Map;

/**
 * @author xxjsb036
 *
 *
 */
public class ChartsBean {

	//数据说明
	private List<String> legend;
	//X轴说明
	private List<String> xAxis;
	//系列列表
	private List<List<String>> columnSeries;
	//饼状图数据
	private List<List<Map<String, String>>> pieSeries;

	/**
	 * @return the legend
	 */
	public List<String> getLegend() {
		return legend;
	}

	/**
	 * @param legend
	 *            the legend to set
	 */
	public void setLegend(List<String> legend) {
		this.legend = legend;
	}

	/**
	 * @return the xAxis
	 */
	public List<String> getxAxis() {
		return xAxis;
	}

	/**
	 * @param xAxis
	 *            the xAxis to set
	 */
	public void setxAxis(List<String> xAxis) {
		this.xAxis = xAxis;
	}

	/**
	 * @return the columnSeries
	 */
	public List<List<String>> getColumnSeries() {
		return columnSeries;
	}

	/**
	 * @param columnSeries
	 *            the columnSeries to set
	 */
	public void setColumnSeries(List<List<String>> columnSeries) {
		this.columnSeries = columnSeries;
	}

	/**
	 * @return the pieSeries
	 */
	public List<List<Map<String, String>>> getPieSeries() {
		return pieSeries;
	}

	/**
	 * @param pieSeries
	 *            the pieSeries to set
	 */
	public void setPieSeries(List<List<Map<String, String>>> pieSeries) {
		this.pieSeries = pieSeries;
	}

}
