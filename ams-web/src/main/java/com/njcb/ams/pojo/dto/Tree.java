package com.njcb.ams.pojo.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;

public class Tree {
	@ApiModelProperty(value = "节点ID")
	private int id;

	@ApiModelProperty(value = "父节点ID")
	private int pId;// 父节点id

	@ApiModelProperty(value = "节点文本描述")
	private String text;

	@ApiModelProperty(value = "节点状态")
	private String state;

	@ApiModelProperty(value = "节点URL")
	private String url;

	@ApiModelProperty(value = "节点选取状态")
	private String checked;

	@ApiModelProperty(value = "节点属性集合")
	private Map<String, Object> attributes = new HashMap<String, Object>(); // 添加到节点的自定义属性

	@ApiModelProperty(value = "子节点")
	private List<Tree> children; // 子节点数据

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUrl() {
		return url;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public List<Tree> getChildren() {
		return children;
	}

	public void setChildren(List<Tree> children) {
		this.children = children;
	}

	/**
	 * @param attributes
	 *            the attributes to set 改为追加
	 */
	public void setAttributes(Map<String, Object> attributes) {
		if (attributes.size() > 0) {
			if (this.getAttributes().size() > 0) {
				this.attributes.putAll(attributes);
			} else {
				this.attributes = attributes;
			}
		}
	}

}
