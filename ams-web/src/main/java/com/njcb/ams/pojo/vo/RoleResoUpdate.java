package com.njcb.ams.pojo.vo;

import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.util.AmsJsonUtils;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author LOONG
 */
public class RoleResoUpdate {
    @ApiModelProperty(value = "插入集合")
    private List<AuthRRoleReso> insertList;
    @ApiModelProperty(value = "删除集合")
    private List<AuthRRoleReso> deleteList;

    public List<AuthRRoleReso> getInsertList() {
        return insertList;
    }

    public void setInsertList(List<AuthRRoleReso> insertList) {
        this.insertList = insertList;
    }

    public List<AuthRRoleReso> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<AuthRRoleReso> deleteList) {
        this.deleteList = deleteList;
    }
}
