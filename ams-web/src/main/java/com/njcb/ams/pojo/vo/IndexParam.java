package com.njcb.ams.pojo.vo;

public class IndexParam {
	/**
	 * 系统名称
	 */
	private String sysName;
	/**
	 * 交易日期
	 */
	private String busiDate;
	/**
	 * 是否验证码
	 */
	private boolean checkCode;
	
	public String getSysName() {
		return sysName;
	}
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	public String getBusiDate() {
		return busiDate;
	}
	public void setBusiDate(String busiDate) {
		this.busiDate = busiDate;
	}
	public boolean isCheckCode() {
		return checkCode;
	}
	public void setCheckCode(boolean checkCode) {
		this.checkCode = checkCode;
	}

}
