package com.njcb.ams.pojo.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author LOONG
 */
public class ResourceParam {
    @ApiModelProperty(value = "机构ID")
    private Integer orgId;
    @ApiModelProperty(value = "角色ID")
    private Integer roleId;
    @ApiModelProperty(value = "用户ID")
    private Integer userId;

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
