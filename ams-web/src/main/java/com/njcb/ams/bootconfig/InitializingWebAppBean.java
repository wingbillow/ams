package com.njcb.ams.bootconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * 应用级bean初始化
 * @author LOONG
 *
 */
@Configuration
public class InitializingWebAppBean {

	/**
	 * 文件上传
	 * @return
	 */
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(1048576000);
		resolver.setMaxInMemorySize(1024);
		return resolver;
	}

}
