package com.njcb.ams.bootconfig;

import com.njcb.ams.store.esbmodule.annotation.EsbService;
import com.njcb.ams.support.annotation.AmsPlug;
import com.njcb.ams.support.plug.Plug;
import com.njcb.ams.util.AmsCollectionUtils;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * @author LOONG
 */
@AmsPlug
public class WebPlug implements Plug {

    @Override
    public String plugName() {
        return "WebPlug";
    }

    @Override
    public void start() {
        System.out.println();
    }

    @Override
    public void stop() {

    }

    @Override
    public List<Class> annotationType() {
        return AmsCollectionUtils.newArrayList(EsbService.class);
    }

    @Override
    public void annotationBean(Object bean, Annotation annotation) {
        System.out.println(bean);
    }
}
