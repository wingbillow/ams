package com.njcb.ams.web.controller.common;

import java.util.List;

import com.njcb.ams.pojo.bo.SysAlterRecordBO;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.store.page.PageHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessCoreManager;
import com.njcb.ams.repository.entity.SysAlterRecord;

/**
 * 公共信息处理
 * 
 * @author liuyanlong
 *
 */
@RestController
@RequestMapping("/commonInfo")
public class CommonInfoController {

	@Autowired
	private BusinessCoreManager businessManager;

	@RequestMapping(value = "alterRecordQuery")
	@ResponseBody
	public PageResponse<SysAlterRecordBO> alterRecordQuery(@RequestBody SysAlterRecord inBean) throws Exception {
		List<SysAlterRecordBO> rtList = businessManager.alterRecordQuery(inBean);
		return PageResponse.build(rtList,PageHandle.getResultPage().getTotal());
	}

}
