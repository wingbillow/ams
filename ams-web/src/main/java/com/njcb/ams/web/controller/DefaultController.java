package com.njcb.ams.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author LOONG
 */
@Controller
public class DefaultController {
	
	/**
	 * 	首页
	 */
	@RequestMapping(value = "/")
	public String index() {
		return "/html/index/login";
	}
	
	
	/**
	 * 	注册页
	 */
	@RequestMapping(value = "/register")
	public String register() {
		return "/html/systemManage/userManage/register";
	}
	
	/**
	 * 	跳转到页面
	 */
	@RequestMapping(value = "/goto", method = RequestMethod.POST)
	public String gotoHtml(String html) {
		return html;
	}
	
}
