package com.njcb.ams.web.controller.analyze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.pojo.vo.ChartsBean;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/analyze")
@Api(value = "analyze",tags = "analyze")
public class HomePageChartController {

	@Autowired
	private BusinessSystemManager businessManager;

	@ApiOperation(value = "交易量", notes = "交易量Top10")
	@RequestMapping(value = "countOfBusinessTop", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryCountOfBusinessTop() throws Exception {
		SysTradeLog sysTradeLog = new SysTradeLog();
		sysTradeLog.setTimeStamp(System.currentTimeMillis() - (24 * 60 * 60 * 1000));
		List<SysTradeLog> rtList = businessManager.queryCountOfBusinessTop(sysTradeLog);

		ChartsBean outBean = new ChartsBean();
		List<String> xAxis = new ArrayList<String>();
		List<List<String>> series = new ArrayList<List<String>>();
		ArrayList<String> seriesData = new ArrayList<String>();
		int count = 10;
		for (int i = 0; i < rtList.size() & i < count; i++) {
			SysTradeLog tmpSysTradeLog = rtList.get(i);
			if (isIgnore(tmpSysTradeLog)) {
				count++;
				continue;
			}
			xAxis.add(tmpSysTradeLog.getTradeName());
			seriesData.add(String.valueOf(tmpSysTradeLog.getUseTime()));
		}

		series.add(seriesData);
		outBean.setxAxis(xAxis);
		outBean.setColumnSeries(series);
		outBean.setPieSeries(new ArrayList<List<Map<String, String>>>());
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("success", true);
		modelMap.put("outBean", outBean);
		return modelMap;
	}
	
	@ApiOperation(value = "交易耗时", notes = "交易耗时(毫秒)Top10")
	@RequestMapping(value = "timeOfBusinessTop", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryTimeOfBusinessTop() throws Exception {
		SysTradeLog sysTradeLog = new SysTradeLog();
		sysTradeLog.setTimeStamp(System.currentTimeMillis() - (24 * 60 * 60 * 1000));
		List<SysTradeLog> rtList = businessManager.queryTimeOfBusinessTop(sysTradeLog);

		ChartsBean outBean = new ChartsBean();
		List<String> xAxis = new ArrayList<String>();
		List<List<String>> series = new ArrayList<List<String>>();
		ArrayList<String> seriesData = new ArrayList<String>();
		int count = 10;
		for (int i = 0; i < rtList.size() & i < count; i++) {
			SysTradeLog tmpSysTradeLog = rtList.get(i);
			if (isIgnore(tmpSysTradeLog)) {
				count++;
				continue;
			}
			xAxis.add(tmpSysTradeLog.getTradeName());
			seriesData.add(String.valueOf(tmpSysTradeLog.getUseTime()));
		}

		series.add(seriesData);
		outBean.setxAxis(xAxis);
		outBean.setColumnSeries(series);
		outBean.setPieSeries(new ArrayList<List<Map<String, String>>>());
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("success", true);
		modelMap.put("outBean", outBean);
		return modelMap;
	}
	
	@ApiOperation(value = "交易分布", notes = "交易分布Top10")
	@RequestMapping(value = "countDistribution", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryCountDistribution() throws Exception {
		ChartsBean resultBean = new ChartsBean();
		List<List<Map<String, String>>> pieSeries = new ArrayList<List<Map<String, String>>>();
		List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();
		List<String> legend = new ArrayList<String>();
		SysTradeLog sysTradeLog = new SysTradeLog();
		sysTradeLog.setTimeStamp(System.currentTimeMillis() - (24 * 60 * 60 * 1000));
		List<SysTradeLog> rtList = businessManager.queryCountOfBusinessTop(sysTradeLog);

		int count = 10;
		for (int i = 0; i < rtList.size() & i < count; i++) {
			SysTradeLog tmpSysTradeLog = rtList.get(i);
			if (isIgnore(tmpSysTradeLog)) {
				count++;
				continue;
			}
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("name", tmpSysTradeLog.getTradeName());
			dataMap.put("value", String.valueOf(tmpSysTradeLog.getUseTime()));
			dataList.add(dataMap);
			legend.add(tmpSysTradeLog.getTradeName());
		}
		pieSeries.add(dataList);
		resultBean.setLegend(legend);
		resultBean.setPieSeries(pieSeries);

		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("success", true);
		modelMap.put("outBean", resultBean);
		return modelMap;
	}
	
	
	@ApiOperation(value = "交易成功率", notes = "交易成功率TOP10")
	@RequestMapping(value = "succRateOfBusinessTop", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> querySuccRateOfBusinessTop() throws Exception {
		SysTradeLog sysTradeLog = new SysTradeLog();
		sysTradeLog.setTimeStamp(System.currentTimeMillis() - (24 * 60 * 60 * 1000));
		List<SysTradeLog> rtList = businessManager.querySuccRateOfBusinessTop(sysTradeLog);

		ChartsBean outBean = new ChartsBean();
		List<String> xAxis = new ArrayList<String>();
		List<List<String>> series = new ArrayList<List<String>>();
		ArrayList<String> seriesData = new ArrayList<String>();
		int count = 10;
		for (int i = 0; i < rtList.size() & i < count; i++) {
			SysTradeLog tmpSysTradeLog = rtList.get(i);
			if (isIgnore(tmpSysTradeLog)) {
				count++;
				continue;
			}
			xAxis.add(tmpSysTradeLog.getTradeName());
			seriesData.add(String.valueOf(tmpSysTradeLog.getUseTime()));
		}

		series.add(seriesData);
		outBean.setxAxis(xAxis);
		outBean.setColumnSeries(series);
		outBean.setPieSeries(new ArrayList<List<Map<String, String>>>());
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("success", true);
		modelMap.put("outBean", outBean);
		return modelMap;
	}

	private boolean isIgnore(SysTradeLog tmpSysTradeLog) {
		if ("CH1001".equals(tmpSysTradeLog.getTradeCode()) || "CH1002".equals(tmpSysTradeLog.getTradeCode())
				|| "RS1001".equals(tmpSysTradeLog.getTradeCode()) || "RS1002".equals(tmpSysTradeLog.getTradeCode())
				|| "UA1001".equals(tmpSysTradeLog.getTradeCode()) || "UA1002".equals(tmpSysTradeLog.getTradeCode())
				|| "CH1004".equals(tmpSysTradeLog.getTradeCode())) {
			return true;
		}
		return false;
	}

}
