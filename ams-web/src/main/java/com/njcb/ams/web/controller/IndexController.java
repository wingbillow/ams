package com.njcb.ams.web.controller;

import com.njcb.ams.application.BusinessSystemWebManager;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.pojo.vo.IndexParam;
import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.repository.entity.CommUserInfo;
import com.njcb.ams.store.authcode.ValidateCode;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsAssert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author LOONG
 */
@RestController
@Api(value = "index",tags = "index")
public class IndexController {
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private BusinessSystemWebManager businessManager;
	
	/**
	 * 首页参数
	 */
	@ApiOperation(value = "首页参数", notes = "首页参数")
	@RequestMapping(value = "index/param", method = RequestMethod.POST)
	@ResponseBody
	public EntityResponse<IndexParam> param() {
		IndexParam indexParam = new IndexParam();
		indexParam.setSysName(StringUtils.isEmpty(SysBaseDefine.SYS_NAME)?"管理系统":SysBaseDefine.SYS_NAME);
		indexParam.setBusiDate(SysBaseDefine.BUSI_DATE);
		indexParam.setCheckCode(SysBaseDefine.IS_CHECK_CODE_VALUE);
		return EntityResponse.build(indexParam);
	}
	
	/**
	 * 验证码
	 */
	@ApiOperation(value = "获取验证码", notes = "获取验证码")  
	@RequestMapping(value = "index/validcode", method = RequestMethod.GET)
	public void index(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/jpg;charset=utf-8");
		ValidateCode code = new ValidateCode(ValidateCode.TYPE_NUM_ONLY, "", 70, 30, 4, 120);
		request.getSession().setAttribute(SysBaseDefine.VALID_CODE_TEMP_SESSION, code.getTextCode());
		try {
			code.write(response.getOutputStream());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 注册
	 */
	@ApiOperation(value = "用户注册", notes = "用户注册")  
	@RequestMapping(value = "index/register", method = RequestMethod.POST)
	@ResponseBody
	public Response register(@RequestBody CommUserInfo userInfo) {
		AmsAssert.notNull(userInfo.getLoginName(), "用户名不能为空");
		AmsAssert.notNull(userInfo.getPassWord(), "密码不能为空");
		AmsAssert.notNull(userInfo.getEmail(), "邮箱不能为空");
		
		String emailRegex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean isEmail = userInfo.getEmail().matches(emailRegex);
        if(!isEmail){
        	ExceptionUtil.throwAppException("邮件格式不正确","CRM2001");
		}
        
		businessManager.registerUser(userInfo);
		return Response.buildSucc();
	}
	
}
