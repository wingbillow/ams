package com.njcb.ams.web.controller.system;

import java.util.List;

import com.njcb.ams.util.AmsCollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessSystemManager;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;

/**
 * @author LOONG
 */
@RestController
@RequestMapping("/system/sysinfo")
public class SystemManageController {
	@Autowired
	private BusinessSystemManager businessSysManager;

	@RequestMapping(value = "query")
	@ResponseBody
	public PageResponse<SysInfo> sysInfoQuery(@RequestBody SysInfo inBean) {
		SysInfo sysInfo = businessSysManager.sysInfoQuery(inBean);
		return PageResponse.build(AmsCollectionUtils.newArrayList(sysInfo), 1);
	}

	@RequestMapping(value = "save")
	@ResponseBody
	public Response sysInfoSave(@RequestBody SysInfo inBean) {
		businessSysManager.sysInfoSave(inBean);
		return Response.buildSucc();
	}
	
}
