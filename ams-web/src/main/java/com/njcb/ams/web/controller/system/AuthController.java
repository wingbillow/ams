package com.njcb.ams.web.controller.system;

import com.njcb.ams.application.BusinessSystemWebManager;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.pojo.vo.OrgResoUpdate;
import com.njcb.ams.pojo.vo.ResourceParam;
import com.njcb.ams.pojo.vo.RoleResoUpdate;
import com.njcb.ams.pojo.vo.UserRoleUpdate;
import com.njcb.ams.repository.entity.AuthROrgReso;
import com.njcb.ams.repository.entity.AuthRRoleReso;
import com.njcb.ams.repository.entity.AuthRUserRole;
import com.njcb.ams.support.exception.ExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 权限管理
 *
 * @author srxhx207
 */
@RequestMapping("/auth")
@RestController
@Api(value = "auth", tags = "auth")
public class AuthController {

    @Autowired
    private BusinessSystemWebManager businessManager;

    /**
     * 获取[机构的]功能资源树
     *
     * @return
     */
    @ApiOperation(value = "获取[机构的]功能资源树", notes = "获取[机构的]功能资源树")
    @RequestMapping(value = "org/resource", method = RequestMethod.POST)
    @ResponseBody
    public List<Tree> getOrgResoTree(@RequestBody ResourceParam resourceParam) {
        if(null == resourceParam || null == resourceParam.getOrgId()){
            ExceptionUtil.throwAppException("参数[orgId]不能为空");
        }
        return businessManager.orgResoQuery(resourceParam.getOrgId());
    }

    /**
     * 更新[机构的]功能资源树
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "更新[机构的]功能资源树", notes = "更新[机构的]功能资源树")
    @RequestMapping(value = "org/resource/update", method = RequestMethod.POST)
    @ResponseBody
    public Response updateResoTree(@RequestBody OrgResoUpdate orgResoUpdate) throws Exception {
        // 后台接收到的数据
        List<AuthROrgReso> insertList = orgResoUpdate.getInsertList();
        List<AuthROrgReso> deleteList = orgResoUpdate.getDeleteList();
        businessManager.orgResoSave(insertList, deleteList);
        return Response.buildSucc();
    }

    /**
     * 获取[角色的]功能资源树
     *
     * @return
     */
    @ApiOperation(value = "获取[角色的]功能资源树", notes = "获取[角色的]功能资源树")
    @RequestMapping(value = "role/resource", method = RequestMethod.POST)
    @ResponseBody
    public List<Tree> getRoleResoNodes(@RequestBody ResourceParam resourceParam) {
        if(null == resourceParam || null == resourceParam.getRoleId()){
            ExceptionUtil.throwAppException("参数[roleId]不能为空");
        }
        return businessManager.roleResoQuery(resourceParam.getRoleId());
    }

    @ApiOperation(value = "更新[角色的]功能资源树", notes = "更新[角色的]功能资源树")
    @RequestMapping(value = "role/resource/update", method = RequestMethod.POST)
    @ResponseBody
    public Response updateRoleResoNodes(@RequestBody RoleResoUpdate roleResoUpdate) throws Exception {
        List<AuthRRoleReso> insertList = roleResoUpdate.getInsertList();
        List<AuthRRoleReso> deleteList = roleResoUpdate.getDeleteList();
        businessManager.roleResoSave(insertList, deleteList);
        return Response.buildSucc();
    }

    /**
     * 获取[用户的]角色树
     *
     * @return
     */
    @ApiOperation(value = "获取[用户的]角色树", notes = "获取[用户的]角色树")
    @RequestMapping(value = "user/role", method = RequestMethod.POST)
    @ResponseBody
    public List<Tree> getUserRoleNodes(@RequestBody ResourceParam resourceParam) {
        if(null == resourceParam || null == resourceParam.getUserId()){
            ExceptionUtil.throwAppException("参数[userId]不能为空");
        }
        return businessManager.userRoleQuery(resourceParam.getUserId());
    }

    @ApiOperation(value = "更新[用户的]角色树", notes = "更新[用户的]角色树")
    @RequestMapping(value = "user/role/update", method = RequestMethod.POST)
    @ResponseBody
    public Response updateUserRoleNodes(@RequestBody UserRoleUpdate userRoleUpdate) throws Exception {
        List<AuthRUserRole> insertList = userRoleUpdate.getInsertList();
        List<AuthRUserRole> deleteList = userRoleUpdate.getDeleteList();
        businessManager.userRoleSave(insertList, deleteList);
        return Response.buildSucc();
    }
}
