package com.njcb.ams.web.controller.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessReportManager;
import com.njcb.ams.repository.entity.CommReportMeta;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LOONG
 */
@RestController
@Api(value = "report",tags = "report")
@RequestMapping("/report/meta")
public class ReportMetaController {

	@Autowired
	private BusinessReportManager businessManager;

	@ApiOperation(value = "获取报表配置列表", notes = "根据任意条件查询报表配置")
	@RequestMapping(value = "query", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<CommReportMeta> reportMetaQuery(@RequestBody CommReportMeta reportMeta) throws Exception {
		PageHandle.startPage(reportMeta);
		List<CommReportMeta> rtList = businessManager.reportMetaQuery(reportMeta);
		Page page = PageHandle.endPage();
		rtList.forEach( reportMetaTmp -> {
			reportMetaTmp.setPreviewUrl("/template/report?reportno="+reportMetaTmp.getReportNo());
		});
		return PageResponse.build(rtList, page.getTotal());
	}

	@ApiOperation(value = "保存报表配置", notes = "单笔保存报表配置")
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Response reportMetaSave(@RequestBody CommReportMeta reportMeta) throws Exception {
		businessManager.reportMetaSave(reportMeta);
		return Response.buildSucc();
	}

	@ApiOperation(value = "删除报表配置", notes = "单笔删除报表配置")
	@RequestMapping(value = "remove", method = RequestMethod.POST)
	@ResponseBody
	public Response reportMetaRemove(@RequestBody CommReportMeta reportMeta) throws Exception {
		businessManager.reportMetaRemove(reportMeta);
		return Response.buildSucc();
	}
}
