/**
 * 树结构工具包
 * 
 * @author njcbkf045
 * @version 1.0 2017年3月17日
 * @see history
 *      2017年3月17日
 */

package com.njcb.ams.util;

import java.util.ArrayList;
import java.util.List;

import com.njcb.ams.pojo.dto.Tree;

public class TreeNodeUtil {

	/**
	 * 获取有层级的树，填充children属性值
	 * @param treesList
	 * @return 层级树
	 */
	public static List<Tree> getTierTrees(List<Tree> treesList){
		List<Tree> tierTrees = new ArrayList<Tree>();
		//遍历找到跟节点
		for (Tree mt : treesList) {
			// 如果pId为空或为0 或上级节点等于自身节点，则该节点为根节点
			if (0 == mt.getpId() || mt.getId() == mt.getpId()) {
				// 递归获取父节点下的子节点
				mt.setChildren(getChildrenNode(mt.getId(), treesList));
				tierTrees.add(mt);
			}
		}
		setParentState(tierTrees);
		return tierTrees;
	}
	
	/**
	 * 递归获取子节点下的子节点
	 * @param pId 父节点ID
	 * @param treesList
	 * @return
	 */
	private static List<Tree> getChildrenNode(int pId, List<Tree> treesList) {
		List<Tree> childrenNode = new ArrayList<Tree>();
		for (Tree mt : treesList) {
			if (0 == mt.getpId() || mt.getId() == mt.getpId()) {
				continue;
			}
			if (mt.getpId() == pId) {
				// 递归获取子节点下的子节点，即设置树控件中的children
				mt.setChildren(getChildrenNode(mt.getId(), treesList));
				childrenNode.add(mt);
			}
		}
		return childrenNode;
	}

	
	/**
	 * 如果节点为父节点(包含子节点),则设置此节点的State默认为closed
	 * @param trees
	 */
	private static void setParentState(List<Tree> trees) {
		for (Tree mt : trees) {
			if (mt.getChildren().size() > 0) {
				setParentState(mt.getChildren());
				mt.setState("closed");
			}
		}
	}
}
