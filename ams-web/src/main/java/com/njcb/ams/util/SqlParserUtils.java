package com.njcb.ams.util;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.druid.sql.ast.statement.SQLSelect;
import com.alibaba.druid.sql.ast.statement.SQLSelectItem;
import com.alibaba.druid.sql.ast.statement.SQLSelectQuery;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleSelectQueryBlock;
import com.alibaba.druid.sql.dialect.oracle.parser.OracleStatementParser;

public class SqlParserUtils {
    public static List<SQLSelectItem> parserSelectItems(String sql){
        OracleStatementParser mySqlStatementParser = new OracleStatementParser(sql);
        SQLSelectStatement sqlSelectStatement = (SQLSelectStatement) mySqlStatementParser.parseSelect();
        SQLSelect sqlSelect = sqlSelectStatement.getSelect();
        SQLSelectQuery sqlSelectQuery = sqlSelect.getQuery();
        if (sqlSelectQuery instanceof OracleSelectQueryBlock) {
            OracleSelectQueryBlock mySqlSelectQueryBlock = (OracleSelectQueryBlock) sqlSelectQuery;
            List<SQLSelectItem> selectItem = mySqlSelectQueryBlock.getSelectList();
            return selectItem;
        }
        return new ArrayList<SQLSelectItem>();
    }

    public static void main(String[] args) {
        String sql = " select org_no as ddd, org_name from COMM_ORG_INFO where 1 = 1 ";
        List<?> list  =  SqlParserUtils.parserSelectItems(sql);
        System.out.println(list);
    }

}
