package com.njcb.ams.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.CommHolidayInfoDAO;
import com.njcb.ams.repository.entity.CommHolidayInfo;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.support.exception.ExceptionUtil;

@Service
public class HolidayInfoService {
	// private static final Logger logger =
	// LoggerFactory.getLogger(HolidayInfoService.class);

	public static final char WORK_DAY_CHAR = '1';
	public static final char HOLI_DAY_CHAR = '0';
	public static final String WORK_DAY_STRING = "1";
	public static final String HOLI_DAY_STRING = "0";

	public static HolidayInfoService getInstance() {
		return AppContext.getBean(HolidayInfoService.class);
	}

	/**
	 * 获取下一工作日
	 * 
	 * @param currDate
	 * @return
	 */
	public String getNextWorkDay(String currDate, int nextNum) {
		Calendar currCal = Calendar.getInstance();
		try {
			currCal.setTime(new SimpleDateFormat("yyyyMMdd").parse(currDate));
		} catch (ParseException e) {
			ExceptionUtil.throwAppException(e.getMessage());
		}
		int currIndex = currCal.get(Calendar.DAY_OF_YEAR);
		String yearStr = getCurrYearStr(currDate.substring(0, 4));
		if ((currIndex + nextNum) > yearStr.length()) {
			String nextYearStr = getNextYearStr(currDate.substring(0, 4));
			yearStr = yearStr + nextYearStr;
		}
		while (HOLI_DAY_CHAR == yearStr.charAt(currIndex + nextNum - 1)) {
			nextNum++;
		}
		currCal.set(Calendar.DAY_OF_YEAR, currIndex + nextNum);
		return new SimpleDateFormat("yyyyMMdd").format(currCal.getTime());
	}

	/**
	 * 几年两个日期之间的工作日
	 * 
	 * @param firstDate
	 * @param secondDate
	 * @return
	 */
	public int countWorkDay(String firstDate, String secondDate) {
		return countDay(firstDate, secondDate, WORK_DAY_STRING);
	}

	/**
	 * 几年两个日期之间的节假日
	 * 
	 * @param firstDate
	 * @param secondDate
	 * @return
	 */
	public int countHoliDay(String firstDate, String secondDate) {
		return countDay(firstDate, secondDate, HOLI_DAY_STRING);
	}

	private int countDay(String firstDate, String secondDate, String dayType) {
		Calendar firstCal = Calendar.getInstance();
		Calendar secondCal = Calendar.getInstance();
		// 调整日期的前后顺序
		try {
			if (firstDate.compareTo(secondDate) < 0) {
				firstCal.setTime(new SimpleDateFormat("yyyyMMdd").parse(firstDate));
				secondCal.setTime(new SimpleDateFormat("yyyyMMdd").parse(secondDate));
			} else {
				firstCal.setTime(new SimpleDateFormat("yyyyMMdd").parse(secondDate));
				secondCal.setTime(new SimpleDateFormat("yyyyMMdd").parse(firstDate));
				String temp = firstDate;
				firstDate = secondDate;
				secondDate = temp;
			}
		} catch (ParseException e) {
			ExceptionUtil.throwAppException(e.getMessage());
		}
		String amongStr = null;
		String firstYearStr = getCurrYearStr(firstDate.substring(0, 4));
		// 如果的同年
		if (firstCal.get(Calendar.YEAR) == secondCal.get(Calendar.YEAR)) {
			amongStr = firstYearStr.substring(firstCal.get(Calendar.DAY_OF_YEAR), secondCal.get(Calendar.DAY_OF_YEAR) - 1);
		} else if (secondCal.get(Calendar.YEAR) - firstCal.get(Calendar.YEAR) == 1) {
			// 如果的跨年
			String nextYearStr = getNextYearStr(firstDate.substring(0, 4));
			amongStr = firstYearStr.substring(firstCal.get(Calendar.DAY_OF_YEAR)) + nextYearStr.substring(0, secondCal.get(Calendar.DAY_OF_YEAR) - 1);
		} else { // 如果的隔年
			firstYearStr = firstYearStr.substring(firstCal.get(Calendar.DAY_OF_YEAR));
			for (int i = 1; i < secondCal.get(Calendar.YEAR) - firstCal.get(Calendar.YEAR); i++) {
				String yearStr = getCurrYearStr(String.valueOf(Integer.parseInt(firstDate.substring(0, 4)) + 1));
				firstYearStr += yearStr;
			}
			String lastYearStr = getCurrYearStr(secondDate.substring(0, 4));
			amongStr = firstYearStr + lastYearStr.substring(0, secondCal.get(Calendar.DAY_OF_YEAR) - 1);
			;
		}
		return StringUtils.countMatches(amongStr, WORK_DAY_STRING);
	}

	/**
	 * 获取当年字符串配置表
	 * 
	 * @param currYear
	 * @return
	 */
	public String getCurrYearStr(String currYear) {
		CommHolidayInfo holidayInfo = AppContext.getBean(CommHolidayInfoDAO.class).findByYear(currYear);
		return holidayInfo.getHolidayStr();
	}

	/**
	 * 获取明年字符串配置表
	 * 
	 * @param currYear
	 * @return
	 */
	public String getNextYearStr(String currYear) {
		String nextYear = String.valueOf((Integer.parseInt(currYear) + 1));
		return getCurrYearStr(nextYear);
	}

}
