package com.njcb.ams.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.njcb.ams.repository.dao.SysParamDAO;
import com.njcb.ams.repository.entity.SysParam;
import com.njcb.ams.factory.domain.AppContext;

@Component
public class ParamManageService{

	@Autowired
	private SysParamDAO sysParamDAO;

	public static ParamManageService getInstance() {
		return AppContext.getBean(ParamManageService.class);
	}
	
	public List<SysParam> queryAll() {
		List<SysParam> paramList = sysParamDAO.selectAll();
		return paramList;
	}

	public List<SysParam> queryParam(SysParam sysParam) {
		List<SysParam> paramList = sysParamDAO.selectBySelective(sysParam);
		return paramList;
	}

	@CacheEvict(value = "SysParamCache", key = "sysParam_?", allEntries = true)
	public void updateParam(List<SysParam> insertList, List<SysParam> deleteList, List<SysParam> updateList) {
		for (int i = 0; i < insertList.size(); i++) {
			SysParam in = insertList.get(i);
			sysParamDAO.insert(in);
		}
		for (int i = 0; i < deleteList.size(); i++) {
			SysParam del = deleteList.get(i);
			sysParamDAO.deleteByPrimaryKey(del.getId());
		}
		for (int i = 0; i < updateList.size(); i++) {
			SysParam upd = updateList.get(i);
			sysParamDAO.updateByPrimaryKey(upd);
		}
	}

	// 根据名称获取对应值
	@Cacheable(value = "SysParamCache", key = "'sysParam_'+#name")
	public String getValueByName(String name) {
		String value = sysParamDAO.getValueByName(name);
		return value;
	}

}
