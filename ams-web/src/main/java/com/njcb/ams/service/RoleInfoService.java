/**
 * 描述：
 * Copyright：Copyright （c） 2017
 * Company：南京银行
 *
 * @author njcbkf045
 * @version 1.0 2017年3月6日
 * @see history
 * 2017年3月6日
 */

package com.njcb.ams.service;

import java.util.List;

import com.njcb.ams.util.AmsAssert;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.CommRoleInfoDAO;
import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.repository.entity.CommRoleInfoExample;
import com.njcb.ams.factory.domain.AppContext;

import javax.management.relation.RoleInfo;

/**
 * @author lisr
 *
 *         类功能描述：
 *
 */

@Service
public class RoleInfoService {

    @Autowired
    private CommRoleInfoDAO roleInfoDAO;

    public static RoleInfoService getInstance() {
        return AppContext.getBean(RoleInfoService.class);
    }

    public List<CommRoleInfo> queryRoleInfo(CommRoleInfo roleInfo) {
        List<CommRoleInfo> paramList = null;
        String roleName = roleInfo.getRoleName();
        String status = roleInfo.getStatus();
        CommRoleInfoExample example = new CommRoleInfoExample();
        CommRoleInfoExample.Criteria criteria = example.createCriteria();
        if (StringUtils.isEmpty(roleName) && StringUtils.isEmpty(status)) {
            paramList = roleInfoDAO.selectAll();
        } else {
            if (!StringUtils.isEmpty(roleName)) {
                criteria = criteria.andRoleNameEqualTo(roleName);
            }
            if (!StringUtils.isEmpty(status)) {
                criteria.andStatusEqualTo(status);
            }
            paramList = roleInfoDAO.selectByExample(example);
        }
        return paramList;
    }

    public void addRoleInfo(CommRoleInfo roleInfo) {
        AmsAssert.notNull(roleInfo.getRoleCode(), roleInfo.getRoleName(), roleInfo.getStatus(), "角色代码、角色名称、角色状态都不能为空");
        CommRoleInfo orgRoleInfo = roleInfoDAO.selectByRoleCode(roleInfo.getRoleCode());
        AmsAssert.isNull(orgRoleInfo, "角色代码[" + roleInfo.getRoleCode() + "]已存在");
        roleInfoDAO.insert(roleInfo);
    }

    public void modifyRoleInfo(CommRoleInfo roleInfo) {
        AmsAssert.notNull(roleInfo.getRoleCode(), roleInfo.getRoleName(), roleInfo.getStatus(), "角色代码、角色名称、角色状态都不能为空");
        CommRoleInfo orgRoleInfo = roleInfoDAO.selectByPrimaryKey(roleInfo.getId());
        //如何修改了角色代码
        if(!roleInfo.getRoleCode().equals(orgRoleInfo.getRoleCode())){
            orgRoleInfo = roleInfoDAO.selectByRoleCode(roleInfo.getRoleCode());
            AmsAssert.isNull(orgRoleInfo, "角色代码[" + roleInfo.getRoleCode() + "]已存在");
        }
        roleInfoDAO.updateByPrimaryKey(roleInfo);
    }

    public void removeRoleInfo(CommRoleInfo roleInfo) {
        roleInfoDAO.deleteByPrimaryKey(roleInfo.getId());
    }
}
