package com.njcb.ams.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.CommOrgInfoDAO;
import com.njcb.ams.repository.dao.ResourceInfoDAO;
import com.njcb.ams.repository.entity.CommOrgInfo;
import com.njcb.ams.repository.entity.ResourceInfo;
import com.njcb.ams.repository.entity.ResourceInfoExample;
import com.njcb.ams.pojo.dto.Tree;
import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.factory.comm.DataBus;

@Service
public class ResourceManageService {

	@Autowired
	private ResourceInfoDAO resourceInfoDAO;
	
	@Autowired
	private CommOrgInfoDAO commOrgInfoDAO;

	public static ResourceManageService getInstance() {
		return AppContext.getBean(ResourceManageService.class);
	}

	public List<ResourceInfo> queryMenu(String resourceType) {
		List<ResourceInfo> funList = null;
		if (null != resourceType && (!resourceType.isEmpty())) {
			funList = resourceInfoDAO.findByResourceType(resourceType);
		} else {
			funList = resourceInfoDAO.selectAll();
		}
		return funList;
	}

	/**
	 * 方法功能描述：
	 */
	public List<Tree> selectMenu(String resourceType) {
		List<ResourceInfo> list = queryResoMenuByUser(resourceType);
		List<Tree> trees = this.convertToTree(list);
		return trees;
	}

	public List<Tree> selectAllMenu() {
		List<ResourceInfo> list = queryMenu(null);
		List<Tree> trees = this.convertToTree(list);
		return trees;
	}

	public List<Tree> convertToTree(List<ResourceInfo> resourceInfos) {
		List<Tree> newTrees = new ArrayList<Tree>();
		for (ResourceInfo resourceInfo : resourceInfos) {
			Tree tree = new Tree();
			tree.setId(resourceInfo.getId());
			tree.setpId(resourceInfo.getParentId());
			tree.setText(resourceInfo.getResourceName());
			// if (tree.getChildren().size() > 0) {
			// tree.setState("closed");
			// }
			newTrees.add(tree);
		}
		return newTrees;
	}

	/**
	 * 方法功能描述：
	 * 
	 * @param resourceType
	 * @param userId
	 * @return
	 */
	public List<ResourceInfo> queryResoMenu(String resourceType, int userId) {
		// 获取用户权限
		List<Integer> resos = resourceInfoDAO.getResoByUser(userId);
		// 查询权限表
		List<ResourceInfo> funList = new ArrayList<>();
		ResourceInfoExample example = new ResourceInfoExample();
		ResourceInfoExample.Criteria criteria = example.createCriteria();
		example.setOrderByClause("PARENT_ID,RESOURCE_ORDER");
		if (null != resourceType && !resourceType.isEmpty()) {
			criteria.andResourceTypeEqualTo(resourceType);
		}
		// 权限列表为空说明该用户没有权限
		if (null != resos && !resos.isEmpty()) {
			criteria.andIdIn(resos);
			funList = resourceInfoDAO.findByUser(example);
		}

		return funList;
	}

	public List<ResourceInfo> queryResoMenuByUser() {
		return queryResoMenuByUser(null);
	}

	public List<ResourceInfo> queryResoMenuByUser(String resourceType) {
		CommOrgInfo commOrgInfo = commOrgInfoDAO.selectByOrgNo(DataBus.getOrgNo());
		Integer userId = DataBus.getUserId();
		List<ResourceInfo> funList = resourceInfoDAO.findResourceInfoByUser(resourceType, commOrgInfo.getId(), userId);
		return funList;
	}
}
