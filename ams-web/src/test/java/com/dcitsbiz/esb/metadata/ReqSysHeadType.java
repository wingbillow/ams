
package com.dcitsbiz.esb.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ReqSysHeadType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ReqSysHeadType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PkgLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TmlCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServiceID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ChannelID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LegOrgID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ReqDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ReqTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MAC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReqSysID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DomainRef" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AcceptLang" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="GlobalSeq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OrgSysID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RespSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RespDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RespTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RespSysID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TrackCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReseFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReseFld" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SvcScn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ReqSeq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReqSysHeadType", propOrder = {
    "pkgLength",
    "tmlCd",
    "serviceID",
    "channelID",
    "legOrgID",
    "reqDate",
    "reqTime",
    "mac",
    "version",
    "priority",
    "reqSysID",
    "domainRef",
    "acceptLang",
    "globalSeq",
    "orgSysID",
    "respSeq",
    "respDate",
    "respTime",
    "transStatus",
    "retCode",
    "retMsg",
    "respSysID",
    "trackCode",
    "reseFlg",
    "reseFld",
    "svcScn",
    "reqSeq"
})
public class ReqSysHeadType {

    @XmlElement(name = "PkgLength")
    protected String pkgLength;
    @XmlElement(name = "TmlCd")
    protected String tmlCd;
    @XmlElement(name = "ServiceID", required = true)
    protected String serviceID;
    @XmlElement(name = "ChannelID", required = true)
    protected String channelID;
    @XmlElement(name = "LegOrgID", required = true)
    protected String legOrgID;
    @XmlElement(name = "ReqDate", required = true)
    protected String reqDate;
    @XmlElement(name = "ReqTime", required = true)
    protected String reqTime;
    @XmlElement(name = "MAC")
    protected String mac;
    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "ReqSysID", required = true)
    protected String reqSysID;
    @XmlElement(name = "DomainRef", required = true)
    protected String domainRef;
    @XmlElement(name = "AcceptLang", required = true)
    protected String acceptLang;
    @XmlElement(name = "GlobalSeq", required = true)
    protected String globalSeq;
    @XmlElement(name = "OrgSysID", required = true)
    protected String orgSysID;
    @XmlElement(name = "RespSeq")
    protected String respSeq;
    @XmlElement(name = "RespDate")
    protected String respDate;
    @XmlElement(name = "RespTime")
    protected String respTime;
    @XmlElement(name = "TransStatus")
    protected String transStatus;
    @XmlElement(name = "RetCode")
    protected String retCode;
    @XmlElement(name = "RetMsg")
    protected String retMsg;
    @XmlElement(name = "RespSysID")
    protected String respSysID;
    @XmlElement(name = "TrackCode")
    protected String trackCode;
    @XmlElement(name = "ReseFlg")
    protected String reseFlg;
    @XmlElement(name = "ReseFld")
    protected String reseFld;
    @XmlElement(name = "SvcScn", required = true)
    protected String svcScn;
    @XmlElement(name = "ReqSeq", required = true)
    protected String reqSeq;

    /**
     * 获取pkgLength属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkgLength() {
        return pkgLength;
    }

    /**
     * 设置pkgLength属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkgLength(String value) {
        this.pkgLength = value;
    }

    /**
     * 获取tmlCd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmlCd() {
        return tmlCd;
    }

    /**
     * 设置tmlCd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmlCd(String value) {
        this.tmlCd = value;
    }

    /**
     * 获取serviceID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceID() {
        return serviceID;
    }

    /**
     * 设置serviceID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceID(String value) {
        this.serviceID = value;
    }

    /**
     * 获取channelID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelID() {
        return channelID;
    }

    /**
     * 设置channelID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelID(String value) {
        this.channelID = value;
    }

    /**
     * 获取legOrgID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegOrgID() {
        return legOrgID;
    }

    /**
     * 设置legOrgID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegOrgID(String value) {
        this.legOrgID = value;
    }

    /**
     * 获取reqDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqDate() {
        return reqDate;
    }

    /**
     * 设置reqDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqDate(String value) {
        this.reqDate = value;
    }

    /**
     * 获取reqTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqTime() {
        return reqTime;
    }

    /**
     * 设置reqTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqTime(String value) {
        this.reqTime = value;
    }

    /**
     * 获取mac属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAC() {
        return mac;
    }

    /**
     * 设置mac属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAC(String value) {
        this.mac = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取priority属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * 设置priority属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * 获取reqSysID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqSysID() {
        return reqSysID;
    }

    /**
     * 设置reqSysID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqSysID(String value) {
        this.reqSysID = value;
    }

    /**
     * 获取domainRef属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainRef() {
        return domainRef;
    }

    /**
     * 设置domainRef属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainRef(String value) {
        this.domainRef = value;
    }

    /**
     * 获取acceptLang属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcceptLang() {
        return acceptLang;
    }

    /**
     * 设置acceptLang属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcceptLang(String value) {
        this.acceptLang = value;
    }

    /**
     * 获取globalSeq属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalSeq() {
        return globalSeq;
    }

    /**
     * 设置globalSeq属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalSeq(String value) {
        this.globalSeq = value;
    }

    /**
     * 获取orgSysID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgSysID() {
        return orgSysID;
    }

    /**
     * 设置orgSysID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgSysID(String value) {
        this.orgSysID = value;
    }

    /**
     * 获取respSeq属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespSeq() {
        return respSeq;
    }

    /**
     * 设置respSeq属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespSeq(String value) {
        this.respSeq = value;
    }

    /**
     * 获取respDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespDate() {
        return respDate;
    }

    /**
     * 设置respDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespDate(String value) {
        this.respDate = value;
    }

    /**
     * 获取respTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespTime() {
        return respTime;
    }

    /**
     * 设置respTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespTime(String value) {
        this.respTime = value;
    }

    /**
     * 获取transStatus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransStatus() {
        return transStatus;
    }

    /**
     * 设置transStatus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransStatus(String value) {
        this.transStatus = value;
    }

    /**
     * 获取retCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetCode() {
        return retCode;
    }

    /**
     * 设置retCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetCode(String value) {
        this.retCode = value;
    }

    /**
     * 获取retMsg属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetMsg() {
        return retMsg;
    }

    /**
     * 设置retMsg属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetMsg(String value) {
        this.retMsg = value;
    }

    /**
     * 获取respSysID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespSysID() {
        return respSysID;
    }

    /**
     * 设置respSysID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespSysID(String value) {
        this.respSysID = value;
    }

    /**
     * 获取trackCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackCode() {
        return trackCode;
    }

    /**
     * 设置trackCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackCode(String value) {
        this.trackCode = value;
    }

    /**
     * 获取reseFlg属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReseFlg() {
        return reseFlg;
    }

    /**
     * 设置reseFlg属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReseFlg(String value) {
        this.reseFlg = value;
    }

    /**
     * 获取reseFld属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReseFld() {
        return reseFld;
    }

    /**
     * 设置reseFld属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReseFld(String value) {
        this.reseFld = value;
    }

    /**
     * 获取svcScn属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSvcScn() {
        return svcScn;
    }

    /**
     * 设置svcScn属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSvcScn(String value) {
        this.svcScn = value;
    }

    /**
     * 获取reqSeq属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqSeq() {
        return reqSeq;
    }

    /**
     * 设置reqSeq属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqSeq(String value) {
        this.reqSeq = value;
    }

}
