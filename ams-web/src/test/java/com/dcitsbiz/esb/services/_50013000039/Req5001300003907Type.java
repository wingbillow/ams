
package com.dcitsbiz.esb.services._50013000039;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.ReqSysHeadType;


/**
 * <p>Req5001300003907Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Req5001300003907Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReqSysHead" type="{http://esb.dcitsbiz.com/metadata}ReqSysHeadType"/&gt;
 *         &lt;element name="ReqAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}ReqAppHeadType"/&gt;
 *         &lt;element name="ReqAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="bussinessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="channelId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="contractType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="remark1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="remark2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="remark3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="remark4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Req5001300003907Type", propOrder = {
    "reqSysHead",
    "reqAppHead",
    "reqAppBody"
})
public class Req5001300003907Type {

    @XmlElement(name = "ReqSysHead", required = true)
    protected ReqSysHeadType reqSysHead;
    @XmlElement(name = "ReqAppHead", required = true)
    protected ReqAppHeadType reqAppHead;
    @XmlElement(name = "ReqAppBody")
    protected Req5001300003907Type.ReqAppBody reqAppBody;

    /**
     * 获取reqSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqSysHeadType }
     *     
     */
    public ReqSysHeadType getReqSysHead() {
        return reqSysHead;
    }

    /**
     * 设置reqSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqSysHeadType }
     *     
     */
    public void setReqSysHead(ReqSysHeadType value) {
        this.reqSysHead = value;
    }

    /**
     * 获取reqAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqAppHeadType }
     *     
     */
    public ReqAppHeadType getReqAppHead() {
        return reqAppHead;
    }

    /**
     * 设置reqAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqAppHeadType }
     *     
     */
    public void setReqAppHead(ReqAppHeadType value) {
        this.reqAppHead = value;
    }

    /**
     * 获取reqAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Req5001300003907Type.ReqAppBody }
     *     
     */
    public Req5001300003907Type.ReqAppBody getReqAppBody() {
        return reqAppBody;
    }

    /**
     * 设置reqAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Req5001300003907Type.ReqAppBody }
     *     
     */
    public void setReqAppBody(Req5001300003907Type.ReqAppBody value) {
        this.reqAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="bussinessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="contractId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="channelId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="contractType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="remark1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="remark2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="remark3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="remark4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bussinessId",
        "contractId",
        "channelId",
        "contractType",
        "remark1",
        "remark2",
        "remark3",
        "remark4"
    })
    public static class ReqAppBody {

        protected String bussinessId;
        protected String contractId;
        protected String channelId;
        protected String contractType;
        protected String remark1;
        protected String remark2;
        protected String remark3;
        protected String remark4;

        /**
         * 获取bussinessId属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBussinessId() {
            return bussinessId;
        }

        /**
         * 设置bussinessId属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBussinessId(String value) {
            this.bussinessId = value;
        }

        /**
         * 获取contractId属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractId() {
            return contractId;
        }

        /**
         * 设置contractId属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractId(String value) {
            this.contractId = value;
        }

        /**
         * 获取channelId属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannelId() {
            return channelId;
        }

        /**
         * 设置channelId属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannelId(String value) {
            this.channelId = value;
        }

        /**
         * 获取contractType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractType() {
            return contractType;
        }

        /**
         * 设置contractType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractType(String value) {
            this.contractType = value;
        }

        /**
         * 获取remark1属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark1() {
            return remark1;
        }

        /**
         * 设置remark1属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark1(String value) {
            this.remark1 = value;
        }

        /**
         * 获取remark2属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark2() {
            return remark2;
        }

        /**
         * 设置remark2属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark2(String value) {
            this.remark2 = value;
        }

        /**
         * 获取remark3属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark3() {
            return remark3;
        }

        /**
         * 设置remark3属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark3(String value) {
            this.remark3 = value;
        }

        /**
         * 获取remark4属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark4() {
            return remark4;
        }

        /**
         * 设置remark4属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark4(String value) {
            this.remark4 = value;
        }

    }

}
