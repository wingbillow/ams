
package com.dcitsbiz.esb.services._50013000039;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.RspSysHeadType;


/**
 * <p>Rsp5001300003902Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Rsp5001300003902Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RspSysHead" type="{http://esb.dcitsbiz.com/metadata}RspSysHeadType"/&gt;
 *         &lt;element name="RspAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}RspAppHeadType"/&gt;
 *         &lt;element name="RspAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="approveState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rsp5001300003902Type", propOrder = {
    "rspSysHead",
    "rspAppHead",
    "rspAppBody"
})
public class Rsp5001300003902Type {

    @XmlElement(name = "RspSysHead", required = true)
    protected RspSysHeadType rspSysHead;
    @XmlElement(name = "RspAppHead", required = true)
    protected RspAppHeadType rspAppHead;
    @XmlElement(name = "RspAppBody")
    protected Rsp5001300003902Type.RspAppBody rspAppBody;

    /**
     * 获取rspSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspSysHeadType }
     *     
     */
    public RspSysHeadType getRspSysHead() {
        return rspSysHead;
    }

    /**
     * 设置rspSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspSysHeadType }
     *     
     */
    public void setRspSysHead(RspSysHeadType value) {
        this.rspSysHead = value;
    }

    /**
     * 获取rspAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspAppHeadType }
     *     
     */
    public RspAppHeadType getRspAppHead() {
        return rspAppHead;
    }

    /**
     * 设置rspAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspAppHeadType }
     *     
     */
    public void setRspAppHead(RspAppHeadType value) {
        this.rspAppHead = value;
    }

    /**
     * 获取rspAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Rsp5001300003902Type.RspAppBody }
     *     
     */
    public Rsp5001300003902Type.RspAppBody getRspAppBody() {
        return rspAppBody;
    }

    /**
     * 设置rspAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Rsp5001300003902Type.RspAppBody }
     *     
     */
    public void setRspAppBody(Rsp5001300003902Type.RspAppBody value) {
        this.rspAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="approveState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "approveState"
    })
    public static class RspAppBody {

        protected String approveState;

        /**
         * 获取approveState属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApproveState() {
            return approveState;
        }

        /**
         * 设置approveState属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApproveState(String value) {
            this.approveState = value;
        }

    }

}
