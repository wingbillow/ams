
package com.dcitsbiz.esb.services._50013000039;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.metadata.RspSysHeadType;


/**
 * <p>Rsp5001300003906Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Rsp5001300003906Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RspSysHead" type="{http://esb.dcitsbiz.com/metadata}RspSysHeadType"/&gt;
 *         &lt;element name="RspAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}RspAppHeadType"/&gt;
 *         &lt;element name="RspAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="vbillcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ctname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ctrantypeid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="personnelid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="depid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="subscribedate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ntotaltaxmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="vdef6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="vdef7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="cvendorid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="vdef5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tby1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tby2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tby3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tby4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="tby5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="pk_ct_pu_b" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="vbdef14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="vbdef11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="vbdef12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="vbdef13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="norigtaxmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ntaxrate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ntax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="norigmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="vbdef9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="vbdef10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="by1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="by2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="by3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="by4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="by5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rsp5001300003906Type", propOrder = {
    "rspSysHead",
    "rspAppHead",
    "rspAppBody"
})
public class Rsp5001300003906Type {

    @XmlElement(name = "RspSysHead", required = true)
    protected RspSysHeadType rspSysHead;
    @XmlElement(name = "RspAppHead", required = true)
    protected RspAppHeadType rspAppHead;
    @XmlElement(name = "RspAppBody")
    protected Rsp5001300003906Type.RspAppBody rspAppBody;

    /**
     * 获取rspSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspSysHeadType }
     *     
     */
    public RspSysHeadType getRspSysHead() {
        return rspSysHead;
    }

    /**
     * 设置rspSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspSysHeadType }
     *     
     */
    public void setRspSysHead(RspSysHeadType value) {
        this.rspSysHead = value;
    }

    /**
     * 获取rspAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspAppHeadType }
     *     
     */
    public RspAppHeadType getRspAppHead() {
        return rspAppHead;
    }

    /**
     * 设置rspAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspAppHeadType }
     *     
     */
    public void setRspAppHead(RspAppHeadType value) {
        this.rspAppHead = value;
    }

    /**
     * 获取rspAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Rsp5001300003906Type.RspAppBody }
     *     
     */
    public Rsp5001300003906Type.RspAppBody getRspAppBody() {
        return rspAppBody;
    }

    /**
     * 设置rspAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Rsp5001300003906Type.RspAppBody }
     *     
     */
    public void setRspAppBody(Rsp5001300003906Type.RspAppBody value) {
        this.rspAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="vbillcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ctname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ctrantypeid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="personnelid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="depid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="subscribedate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ntotaltaxmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="vdef6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="vdef7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="cvendorid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="vdef5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tby1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tby2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tby3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tby4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="tby5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="pk_ct_pu_b" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="vbdef14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="vbdef11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="vbdef12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="vbdef13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="norigtaxmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ntaxrate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ntax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="norigmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="vbdef9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="vbdef10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="by1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="by2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="by3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="by4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="by5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vbillcode",
        "ctname",
        "ctrantypeid",
        "personnelid",
        "depid",
        "subscribedate",
        "ntotaltaxmny",
        "vdef6",
        "vdef7",
        "cvendorid",
        "vdef5",
        "tby1",
        "tby2",
        "tby3",
        "tby4",
        "tby5",
        "pkCtPuB"
    })
    public static class RspAppBody {

        protected String vbillcode;
        protected String ctname;
        protected String ctrantypeid;
        protected String personnelid;
        protected String depid;
        protected String subscribedate;
        protected String ntotaltaxmny;
        protected String vdef6;
        protected String vdef7;
        protected String cvendorid;
        protected String vdef5;
        protected String tby1;
        protected String tby2;
        protected String tby3;
        protected String tby4;
        protected String tby5;
        @XmlElement(name = "pk_ct_pu_b", required = true)
        protected List<Rsp5001300003906Type.RspAppBody.PkCtPuB> pkCtPuB;

        /**
         * 获取vbillcode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVbillcode() {
            return vbillcode;
        }

        /**
         * 设置vbillcode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVbillcode(String value) {
            this.vbillcode = value;
        }

        /**
         * 获取ctname属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCtname() {
            return ctname;
        }

        /**
         * 设置ctname属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCtname(String value) {
            this.ctname = value;
        }

        /**
         * 获取ctrantypeid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCtrantypeid() {
            return ctrantypeid;
        }

        /**
         * 设置ctrantypeid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCtrantypeid(String value) {
            this.ctrantypeid = value;
        }

        /**
         * 获取personnelid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonnelid() {
            return personnelid;
        }

        /**
         * 设置personnelid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonnelid(String value) {
            this.personnelid = value;
        }

        /**
         * 获取depid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepid() {
            return depid;
        }

        /**
         * 设置depid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepid(String value) {
            this.depid = value;
        }

        /**
         * 获取subscribedate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscribedate() {
            return subscribedate;
        }

        /**
         * 设置subscribedate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscribedate(String value) {
            this.subscribedate = value;
        }

        /**
         * 获取ntotaltaxmny属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNtotaltaxmny() {
            return ntotaltaxmny;
        }

        /**
         * 设置ntotaltaxmny属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNtotaltaxmny(String value) {
            this.ntotaltaxmny = value;
        }

        /**
         * 获取vdef6属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVdef6() {
            return vdef6;
        }

        /**
         * 设置vdef6属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVdef6(String value) {
            this.vdef6 = value;
        }

        /**
         * 获取vdef7属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVdef7() {
            return vdef7;
        }

        /**
         * 设置vdef7属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVdef7(String value) {
            this.vdef7 = value;
        }

        /**
         * 获取cvendorid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCvendorid() {
            return cvendorid;
        }

        /**
         * 设置cvendorid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCvendorid(String value) {
            this.cvendorid = value;
        }

        /**
         * 获取vdef5属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVdef5() {
            return vdef5;
        }

        /**
         * 设置vdef5属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVdef5(String value) {
            this.vdef5 = value;
        }

        /**
         * 获取tby1属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTby1() {
            return tby1;
        }

        /**
         * 设置tby1属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTby1(String value) {
            this.tby1 = value;
        }

        /**
         * 获取tby2属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTby2() {
            return tby2;
        }

        /**
         * 设置tby2属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTby2(String value) {
            this.tby2 = value;
        }

        /**
         * 获取tby3属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTby3() {
            return tby3;
        }

        /**
         * 设置tby3属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTby3(String value) {
            this.tby3 = value;
        }

        /**
         * 获取tby4属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTby4() {
            return tby4;
        }

        /**
         * 设置tby4属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTby4(String value) {
            this.tby4 = value;
        }

        /**
         * 获取tby5属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTby5() {
            return tby5;
        }

        /**
         * 设置tby5属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTby5(String value) {
            this.tby5 = value;
        }

        /**
         * Gets the value of the pkCtPuB property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pkCtPuB property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPkCtPuB().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rsp5001300003906Type.RspAppBody.PkCtPuB }
         * 
         * 
         */
        public List<Rsp5001300003906Type.RspAppBody.PkCtPuB> getPkCtPuB() {
            if (pkCtPuB == null) {
                pkCtPuB = new ArrayList<Rsp5001300003906Type.RspAppBody.PkCtPuB>();
            }
            return this.pkCtPuB;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="vbdef14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="vbdef11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="vbdef12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="vbdef13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="norigtaxmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ntaxrate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ntax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="norigmny" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="vbdef9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="vbdef10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="by1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="by2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="by3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="by4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="by5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vbdef14",
            "vbdef11",
            "vbdef12",
            "vbdef13",
            "norigtaxmny",
            "ntaxrate",
            "ntax",
            "norigmny",
            "vbdef9",
            "vbdef10",
            "by1",
            "by2",
            "by3",
            "by4",
            "by5"
        })
        public static class PkCtPuB {

            protected String vbdef14;
            protected String vbdef11;
            protected String vbdef12;
            protected String vbdef13;
            protected String norigtaxmny;
            protected String ntaxrate;
            protected String ntax;
            protected String norigmny;
            protected String vbdef9;
            protected String vbdef10;
            protected String by1;
            protected String by2;
            protected String by3;
            protected String by4;
            protected String by5;

            /**
             * 获取vbdef14属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef14() {
                return vbdef14;
            }

            /**
             * 设置vbdef14属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef14(String value) {
                this.vbdef14 = value;
            }

            /**
             * 获取vbdef11属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef11() {
                return vbdef11;
            }

            /**
             * 设置vbdef11属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef11(String value) {
                this.vbdef11 = value;
            }

            /**
             * 获取vbdef12属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef12() {
                return vbdef12;
            }

            /**
             * 设置vbdef12属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef12(String value) {
                this.vbdef12 = value;
            }

            /**
             * 获取vbdef13属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef13() {
                return vbdef13;
            }

            /**
             * 设置vbdef13属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef13(String value) {
                this.vbdef13 = value;
            }

            /**
             * 获取norigtaxmny属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNorigtaxmny() {
                return norigtaxmny;
            }

            /**
             * 设置norigtaxmny属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNorigtaxmny(String value) {
                this.norigtaxmny = value;
            }

            /**
             * 获取ntaxrate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNtaxrate() {
                return ntaxrate;
            }

            /**
             * 设置ntaxrate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNtaxrate(String value) {
                this.ntaxrate = value;
            }

            /**
             * 获取ntax属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNtax() {
                return ntax;
            }

            /**
             * 设置ntax属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNtax(String value) {
                this.ntax = value;
            }

            /**
             * 获取norigmny属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNorigmny() {
                return norigmny;
            }

            /**
             * 设置norigmny属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNorigmny(String value) {
                this.norigmny = value;
            }

            /**
             * 获取vbdef9属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef9() {
                return vbdef9;
            }

            /**
             * 设置vbdef9属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef9(String value) {
                this.vbdef9 = value;
            }

            /**
             * 获取vbdef10属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVbdef10() {
                return vbdef10;
            }

            /**
             * 设置vbdef10属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVbdef10(String value) {
                this.vbdef10 = value;
            }

            /**
             * 获取by1属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBy1() {
                return by1;
            }

            /**
             * 设置by1属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBy1(String value) {
                this.by1 = value;
            }

            /**
             * 获取by2属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBy2() {
                return by2;
            }

            /**
             * 设置by2属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBy2(String value) {
                this.by2 = value;
            }

            /**
             * 获取by3属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBy3() {
                return by3;
            }

            /**
             * 设置by3属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBy3(String value) {
                this.by3 = value;
            }

            /**
             * 获取by4属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBy4() {
                return by4;
            }

            /**
             * 设置by4属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBy4(String value) {
                this.by4 = value;
            }

            /**
             * 获取by5属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBy5() {
                return by5;
            }

            /**
             * 设置by5属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBy5(String value) {
                this.by5 = value;
            }

        }

    }

}
