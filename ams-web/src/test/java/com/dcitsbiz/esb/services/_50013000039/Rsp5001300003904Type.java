
package com.dcitsbiz.esb.services._50013000039;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.fds.RspLocalHeadType;
import com.dcitsbiz.esb.metadata.RspSysHeadType;


/**
 * <p>Rsp5001300003904Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Rsp5001300003904Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RspSysHead" type="{http://esb.dcitsbiz.com/metadata}RspSysHeadType"/&gt;
 *         &lt;element name="RspAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}RspAppHeadType"/&gt;
 *         &lt;element name="RspLocalHead" type="{http://esb.dcitsbiz.com/FDS}RspLocalHeadType"/&gt;
 *         &lt;element name="RspAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TotNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="RetNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="Acc_List" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TACode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TAName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="AssetAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="BankAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Machine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="RiskMatchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ModifyDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ManagerDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ManagerStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TrusteeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TrusteeStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ManagerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TrusteeInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="SellerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rsp5001300003904Type", propOrder = {
    "rspSysHead",
    "rspAppHead",
    "rspLocalHead",
    "rspAppBody"
})
public class Rsp5001300003904Type {

    @XmlElement(name = "RspSysHead", required = true)
    protected RspSysHeadType rspSysHead;
    @XmlElement(name = "RspAppHead", required = true)
    protected RspAppHeadType rspAppHead;
    @XmlElement(name = "RspLocalHead", required = true)
    protected RspLocalHeadType rspLocalHead;
    @XmlElement(name = "RspAppBody")
    protected Rsp5001300003904Type.RspAppBody rspAppBody;

    /**
     * 获取rspSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspSysHeadType }
     *     
     */
    public RspSysHeadType getRspSysHead() {
        return rspSysHead;
    }

    /**
     * 设置rspSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspSysHeadType }
     *     
     */
    public void setRspSysHead(RspSysHeadType value) {
        this.rspSysHead = value;
    }

    /**
     * 获取rspAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspAppHeadType }
     *     
     */
    public RspAppHeadType getRspAppHead() {
        return rspAppHead;
    }

    /**
     * 设置rspAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspAppHeadType }
     *     
     */
    public void setRspAppHead(RspAppHeadType value) {
        this.rspAppHead = value;
    }

    /**
     * 获取rspLocalHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RspLocalHeadType }
     *     
     */
    public RspLocalHeadType getRspLocalHead() {
        return rspLocalHead;
    }

    /**
     * 设置rspLocalHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RspLocalHeadType }
     *     
     */
    public void setRspLocalHead(RspLocalHeadType value) {
        this.rspLocalHead = value;
    }

    /**
     * 获取rspAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Rsp5001300003904Type.RspAppBody }
     *     
     */
    public Rsp5001300003904Type.RspAppBody getRspAppBody() {
        return rspAppBody;
    }

    /**
     * 设置rspAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Rsp5001300003904Type.RspAppBody }
     *     
     */
    public void setRspAppBody(Rsp5001300003904Type.RspAppBody value) {
        this.rspAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TotNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="RetNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="Acc_List" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TACode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TAName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="AssetAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="BankAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Machine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="RiskMatchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ModifyDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ManagerDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ManagerStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TrusteeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TrusteeStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="ManagerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TrusteeInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="SellerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totNum",
        "retNum",
        "offSet",
        "accList"
    })
    public static class RspAppBody {

        @XmlElement(name = "TotNum")
        protected Integer totNum;
        @XmlElement(name = "RetNum")
        protected Integer retNum;
        @XmlElement(name = "OffSet")
        protected Integer offSet;
        @XmlElement(name = "Acc_List", required = true)
        protected List<Rsp5001300003904Type.RspAppBody.AccList> accList;

        /**
         * 获取totNum属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTotNum() {
            return totNum;
        }

        /**
         * 设置totNum属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTotNum(Integer value) {
            this.totNum = value;
        }

        /**
         * 获取retNum属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRetNum() {
            return retNum;
        }

        /**
         * 设置retNum属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRetNum(Integer value) {
            this.retNum = value;
        }

        /**
         * 获取offSet属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOffSet() {
            return offSet;
        }

        /**
         * 设置offSet属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOffSet(Integer value) {
            this.offSet = value;
        }

        /**
         * Gets the value of the accList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rsp5001300003904Type.RspAppBody.AccList }
         * 
         * 
         */
        public List<Rsp5001300003904Type.RspAppBody.AccList> getAccList() {
            if (accList == null) {
                accList = new ArrayList<Rsp5001300003904Type.RspAppBody.AccList>();
            }
            return this.accList;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TACode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TAName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdManager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="PrdManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="AssetAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="BankAcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TransDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Machine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="RiskMatchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ModifyDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ManagerDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ManagerStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TrusteeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TrusteeStatusName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="ManagerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TrusteeInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="SellerInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contractNo",
            "clientNo",
            "clientName",
            "taCode",
            "taName",
            "prdCode",
            "prdName",
            "prdManager",
            "prdManagerName",
            "assetAcc",
            "bankAcc",
            "transDate",
            "transTime",
            "machine",
            "riskMatchName",
            "modifyDate",
            "status",
            "statusName",
            "managerDate",
            "managerStatusName",
            "trusteeDate",
            "trusteeStatusName",
            "managerInfo",
            "trusteeInfo",
            "sellerInfo"
        })
        public static class AccList {

            @XmlElement(name = "ContractNo")
            protected String contractNo;
            @XmlElement(name = "ClientNo")
            protected String clientNo;
            @XmlElement(name = "ClientName")
            protected String clientName;
            @XmlElement(name = "TACode")
            protected String taCode;
            @XmlElement(name = "TAName")
            protected String taName;
            @XmlElement(name = "PrdCode")
            protected String prdCode;
            @XmlElement(name = "PrdName")
            protected String prdName;
            @XmlElement(name = "PrdManager")
            protected String prdManager;
            @XmlElement(name = "PrdManagerName")
            protected String prdManagerName;
            @XmlElement(name = "AssetAcc")
            protected String assetAcc;
            @XmlElement(name = "BankAcc")
            protected String bankAcc;
            @XmlElement(name = "TransDate")
            protected String transDate;
            @XmlElement(name = "TransTime")
            protected String transTime;
            @XmlElement(name = "Machine")
            protected String machine;
            @XmlElement(name = "RiskMatchName")
            protected String riskMatchName;
            @XmlElement(name = "ModifyDate")
            protected String modifyDate;
            @XmlElement(name = "Status")
            protected String status;
            @XmlElement(name = "StatusName")
            protected String statusName;
            @XmlElement(name = "ManagerDate")
            protected String managerDate;
            @XmlElement(name = "ManagerStatusName")
            protected String managerStatusName;
            @XmlElement(name = "TrusteeDate")
            protected String trusteeDate;
            @XmlElement(name = "TrusteeStatusName")
            protected String trusteeStatusName;
            @XmlElement(name = "ManagerInfo")
            protected String managerInfo;
            @XmlElement(name = "TrusteeInfo")
            protected String trusteeInfo;
            @XmlElement(name = "SellerInfo")
            protected String sellerInfo;

            /**
             * 获取contractNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractNo() {
                return contractNo;
            }

            /**
             * 设置contractNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractNo(String value) {
                this.contractNo = value;
            }

            /**
             * 获取clientNo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClientNo() {
                return clientNo;
            }

            /**
             * 设置clientNo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClientNo(String value) {
                this.clientNo = value;
            }

            /**
             * 获取clientName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClientName() {
                return clientName;
            }

            /**
             * 设置clientName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClientName(String value) {
                this.clientName = value;
            }

            /**
             * 获取taCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTACode() {
                return taCode;
            }

            /**
             * 设置taCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTACode(String value) {
                this.taCode = value;
            }

            /**
             * 获取taName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTAName() {
                return taName;
            }

            /**
             * 设置taName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTAName(String value) {
                this.taName = value;
            }

            /**
             * 获取prdCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdCode() {
                return prdCode;
            }

            /**
             * 设置prdCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdCode(String value) {
                this.prdCode = value;
            }

            /**
             * 获取prdName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdName() {
                return prdName;
            }

            /**
             * 设置prdName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdName(String value) {
                this.prdName = value;
            }

            /**
             * 获取prdManager属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdManager() {
                return prdManager;
            }

            /**
             * 设置prdManager属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdManager(String value) {
                this.prdManager = value;
            }

            /**
             * 获取prdManagerName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrdManagerName() {
                return prdManagerName;
            }

            /**
             * 设置prdManagerName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrdManagerName(String value) {
                this.prdManagerName = value;
            }

            /**
             * 获取assetAcc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAssetAcc() {
                return assetAcc;
            }

            /**
             * 设置assetAcc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAssetAcc(String value) {
                this.assetAcc = value;
            }

            /**
             * 获取bankAcc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBankAcc() {
                return bankAcc;
            }

            /**
             * 设置bankAcc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBankAcc(String value) {
                this.bankAcc = value;
            }

            /**
             * 获取transDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransDate() {
                return transDate;
            }

            /**
             * 设置transDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransDate(String value) {
                this.transDate = value;
            }

            /**
             * 获取transTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransTime() {
                return transTime;
            }

            /**
             * 设置transTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransTime(String value) {
                this.transTime = value;
            }

            /**
             * 获取machine属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMachine() {
                return machine;
            }

            /**
             * 设置machine属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMachine(String value) {
                this.machine = value;
            }

            /**
             * 获取riskMatchName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRiskMatchName() {
                return riskMatchName;
            }

            /**
             * 设置riskMatchName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRiskMatchName(String value) {
                this.riskMatchName = value;
            }

            /**
             * 获取modifyDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModifyDate() {
                return modifyDate;
            }

            /**
             * 设置modifyDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModifyDate(String value) {
                this.modifyDate = value;
            }

            /**
             * 获取status属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * 设置status属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * 获取statusName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * 设置statusName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * 获取managerDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getManagerDate() {
                return managerDate;
            }

            /**
             * 设置managerDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setManagerDate(String value) {
                this.managerDate = value;
            }

            /**
             * 获取managerStatusName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getManagerStatusName() {
                return managerStatusName;
            }

            /**
             * 设置managerStatusName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setManagerStatusName(String value) {
                this.managerStatusName = value;
            }

            /**
             * 获取trusteeDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTrusteeDate() {
                return trusteeDate;
            }

            /**
             * 设置trusteeDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTrusteeDate(String value) {
                this.trusteeDate = value;
            }

            /**
             * 获取trusteeStatusName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTrusteeStatusName() {
                return trusteeStatusName;
            }

            /**
             * 设置trusteeStatusName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTrusteeStatusName(String value) {
                this.trusteeStatusName = value;
            }

            /**
             * 获取managerInfo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getManagerInfo() {
                return managerInfo;
            }

            /**
             * 设置managerInfo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setManagerInfo(String value) {
                this.managerInfo = value;
            }

            /**
             * 获取trusteeInfo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTrusteeInfo() {
                return trusteeInfo;
            }

            /**
             * 设置trusteeInfo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTrusteeInfo(String value) {
                this.trusteeInfo = value;
            }

            /**
             * 获取sellerInfo属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSellerInfo() {
                return sellerInfo;
            }

            /**
             * 设置sellerInfo属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSellerInfo(String value) {
                this.sellerInfo = value;
            }

        }

    }

}
