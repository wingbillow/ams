
package com.dcitsbiz.esb.services._50013000039;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.dcitsbiz.esb.fds.ReqLocalHeadType;
import com.dcitsbiz.esb.metadata.ReqSysHeadType;


/**
 * <p>Req5001300003905Type complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Req5001300003905Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReqSysHead" type="{http://esb.dcitsbiz.com/metadata}ReqSysHeadType"/&gt;
 *         &lt;element name="ReqAppHead" type="{http://esb.dcitsbiz.com/services/50013000039}ReqAppHeadType"/&gt;
 *         &lt;element name="ReqLocalHead" type="{http://esb.dcitsbiz.com/FDS}ReqLocalHeadType"/&gt;
 *         &lt;element name="ReqAppBody" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="AccType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="QueryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Req5001300003905Type", propOrder = {
    "reqSysHead",
    "reqAppHead",
    "reqLocalHead",
    "reqAppBody"
})
public class Req5001300003905Type {

    @XmlElement(name = "ReqSysHead", required = true)
    protected ReqSysHeadType reqSysHead;
    @XmlElement(name = "ReqAppHead", required = true)
    protected ReqAppHeadType reqAppHead;
    @XmlElement(name = "ReqLocalHead", required = true)
    protected ReqLocalHeadType reqLocalHead;
    @XmlElement(name = "ReqAppBody")
    protected Req5001300003905Type.ReqAppBody reqAppBody;

    /**
     * 获取reqSysHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqSysHeadType }
     *     
     */
    public ReqSysHeadType getReqSysHead() {
        return reqSysHead;
    }

    /**
     * 设置reqSysHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqSysHeadType }
     *     
     */
    public void setReqSysHead(ReqSysHeadType value) {
        this.reqSysHead = value;
    }

    /**
     * 获取reqAppHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqAppHeadType }
     *     
     */
    public ReqAppHeadType getReqAppHead() {
        return reqAppHead;
    }

    /**
     * 设置reqAppHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqAppHeadType }
     *     
     */
    public void setReqAppHead(ReqAppHeadType value) {
        this.reqAppHead = value;
    }

    /**
     * 获取reqLocalHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReqLocalHeadType }
     *     
     */
    public ReqLocalHeadType getReqLocalHead() {
        return reqLocalHead;
    }

    /**
     * 设置reqLocalHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReqLocalHeadType }
     *     
     */
    public void setReqLocalHead(ReqLocalHeadType value) {
        this.reqLocalHead = value;
    }

    /**
     * 获取reqAppBody属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Req5001300003905Type.ReqAppBody }
     *     
     */
    public Req5001300003905Type.ReqAppBody getReqAppBody() {
        return reqAppBody;
    }

    /**
     * 设置reqAppBody属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Req5001300003905Type.ReqAppBody }
     *     
     */
    public void setReqAppBody(Req5001300003905Type.ReqAppBody value) {
        this.reqAppBody = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AccType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OffSet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="QueryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accType",
        "account",
        "idType",
        "offSet",
        "queryNum"
    })
    public static class ReqAppBody {

        @XmlElement(name = "AccType")
        protected String accType;
        @XmlElement(name = "Account")
        protected String account;
        @XmlElement(name = "IdType")
        protected String idType;
        @XmlElement(name = "OffSet")
        protected Integer offSet;
        @XmlElement(name = "QueryNum")
        protected Integer queryNum;

        /**
         * 获取accType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccType() {
            return accType;
        }

        /**
         * 设置accType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccType(String value) {
            this.accType = value;
        }

        /**
         * 获取account属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccount() {
            return account;
        }

        /**
         * 设置account属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccount(String value) {
            this.account = value;
        }

        /**
         * 获取idType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdType() {
            return idType;
        }

        /**
         * 设置idType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdType(String value) {
            this.idType = value;
        }

        /**
         * 获取offSet属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOffSet() {
            return offSet;
        }

        /**
         * 设置offSet属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOffSet(Integer value) {
            this.offSet = value;
        }

        /**
         * 获取queryNum属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getQueryNum() {
            return queryNum;
        }

        /**
         * 设置queryNum属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setQueryNum(Integer value) {
            this.queryNum = value;
        }

    }

}
