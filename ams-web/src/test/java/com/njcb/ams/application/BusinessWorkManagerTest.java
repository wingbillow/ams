package com.njcb.ams.application;

import com.njcb.ams.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import com.njcb.ams.repository.entity.CommRoleInfo;
import com.njcb.ams.store.stable.TradeConsoleService;

@ActiveProfiles("test") // 指定使用application-test.yml
public class BusinessWorkManagerTest extends BaseTest {

	@Autowired
	private TradeConsoleService tradeConsoleService;

	@Autowired
	private BusinessSystemManager BusinessManager;

	@Before
	public void setContext() throws Exception {
		tradeConsoleService.loadServerConsole();
	}
	
	@Test
	public void testQueryRoleInfo(){
		CommRoleInfo roleInfo = new CommRoleInfo();
		roleInfo.setRoleName("管理员");
		BusinessManager.queryRoleInfo(roleInfo);
	}

}
