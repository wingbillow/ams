package com.njcb.ams.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.njcb.ams.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.portal.WebApplication;
import com.njcb.ams.store.stable.TradeConsoleService;

@ActiveProfiles("test") // 指定使用application-test.yml
public class IndexControllerTest extends BaseTest {

	@Autowired
    private WebApplicationContext context;

	@Autowired
	private TradeConsoleService tradeConsoleService;
	
    private MockMvc mockMvc;

    @Before
    public void setupMockMvc() throws Exception {
    	tradeConsoleService.loadServerConsole();
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
    

	@Test
	public void testIndex() throws Exception {
		mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk());
	}

}
