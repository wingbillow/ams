package com.njcb.ams.service;

import javax.annotation.Resource;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.njcb.ams.portal.WebApplication;

/**
 * 权限控制服务 业务条线权限及机构归属权限
 * 
 * @author liuyanlong
 *
 */
@ActiveProfiles("test") // 指定使用application-test.yml
public class BusinessAuthorityServiceTest extends BaseTest {

	@Resource
	public BusinessAuthorityService service;

	@Test
	public void getOwnerOrg() {
		System.out.println(service.getOwnerOrg(12).size());
		System.out.println(service.getOwnerOrg(12));
		System.out.println("");
	}

}
