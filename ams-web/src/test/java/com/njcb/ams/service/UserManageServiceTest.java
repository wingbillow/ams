package com.njcb.ams.service;

import java.util.Random;

import javax.annotation.Resource;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import com.njcb.ams.repository.entity.CommUserInfo;
import com.njcb.ams.util.AmsDateUtils;
import com.njcb.ams.util.AmsStringUtils;

@ActiveProfiles("test") // 指定使用application-test.yml
public class UserManageServiceTest extends BaseTest {

	@Resource
	private UserManageService userManageService;

	@Test
	@Rollback
	public void testRegisterUser() {
		CommUserInfo userInfo = new CommUserInfo();
		userInfo.setId(999);
		String demoName = AmsDateUtils.getCurrentDate8() + AmsStringUtils.leftPad(String.valueOf(new Random(100).nextInt(100)),3,"0");
		userInfo.setLoginName("test"+demoName);
		userInfo.setUserName("test"+demoName);
		userInfo.setPassWord("test");
		userInfo.setEmpNo("1007329");
		userManageService.registerUser(userInfo);
	}


}
