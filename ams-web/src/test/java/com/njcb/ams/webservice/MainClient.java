package com.njcb.ams.webservice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * @author fxyy025
 *
 */
public class MainClient {
	
	public static void main(String[] args) throws Exception {
	}

	public static void testMore(int sum, int threadCount) throws Exception {
		if (threadCount > 100) {
			threadCount = 100;
		}
		long start = System.currentTimeMillis();
		CountDownLatch cd = new CountDownLatch(threadCount);
		for (int i = 0; i < threadCount; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					boolean isTure = true;
					try {
						while (isTure) {
							System.out.println(System.currentTimeMillis());
							isTure = false;
						}
					} finally {
						cd.countDown();
					}
				}
			}).start();
		}
		cd.await();
		System.out.println("耗时:" + (System.currentTimeMillis() - start) + "毫秒");
	}




	@SuppressWarnings("unused")


	private static Connection getConnection() throws Exception {
		String url = "jdbc:oracle:thin:@//159.1.33.52:1521/creddb";
		String drive = "oracle.jdbc.driver.OracleDriver";
		Properties prop = new Properties();
		prop.setProperty("user", "cred");
		prop.setProperty("password", "cred");
		prop.setProperty("remarks", "true");
		Class.forName(drive);
		Connection conn = DriverManager.getConnection(url, prop);
		return conn;
	}

}
