package com.njcb.ams.repository.dao;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test") //指定使用application-test.yml
public class DataDicDAOTest extends BaseTest {

	@Autowired
	private CommDataDicDAO dao;

	@Test
	public void findAll() {
		System.out.println(dao.findAll().size());
	}

}
