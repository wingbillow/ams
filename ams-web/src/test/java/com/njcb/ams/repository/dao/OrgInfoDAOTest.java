package com.njcb.ams.repository.dao;

import com.njcb.ams.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test") //指定使用application-test.yml
public class OrgInfoDAOTest extends BaseTest  {

	@Autowired
	private CommOrgInfoDAO dao;

	@Test
	public void testGetOrgSet() {
		System.out.println(dao.selectEntitySelfAndSubByOrgNo("0100"));
	}

}
