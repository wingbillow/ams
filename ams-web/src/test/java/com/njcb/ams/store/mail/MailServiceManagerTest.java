package com.njcb.ams.store.mail;

import com.njcb.ams.portal.WebApplication;
import com.njcb.ams.store.mail.bean.Email;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 邮件发送
 * @author liuyanlong
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { WebApplication.class })
@ActiveProfiles("test") // 指定使用application-test.yml
public class MailServiceManagerTest {

	@Autowired
	private MailServiceManager mailServiceManager;

	@Test
	public void sendMail() {
		Email email = new Email("23562656@qq.com","注册通知");
		email.setContent("尊敬的用户，您好！\n恭喜你注册成功@trythis.cn");
		mailServiceManager.sendMail(email);
	}

	public void sendMailByAsynchronousMode(Email email) {

	}


}
