package com.njcb.ams.web.controller;

import java.util.List;

import com.njcb.ams.util.AmsDateUtils;
import com.njcb.ams.util.AmsUtils;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.HistoricTaskInstanceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessWrokflowManager;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.vo.HistoricTaskInstanceVO;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 历史任务处理
 *
 * @author liuyanlong
 */
@RestController
@RequestMapping("/workflow/history")
@Api(value = "task", tags = "task")
public class TaskHistoryController {
    @Autowired
    private BusinessWrokflowManager businessworkManager;

    @ApiOperation(value = "已办任务查询")
    @RequestMapping(value = "task/list/done", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<HistoricTaskInstanceVO> actHiTaskinstQuery(@RequestBody HistoricTaskInstanceVO inBean) throws Exception {
        HistoricTaskInstance queryBean = transHistoricTaskInstanceBean(inBean);
        PageHandle.startPage(inBean);
        List<HistoricTaskInstanceVO> list = businessworkManager.queryHaveDone(queryBean);
        Page page = PageHandle.endPage();
        return PageResponse.build(list, page.getTotal());
    }

    @ApiOperation(value = "当前用户已办任务查询")
    @RequestMapping(value = "task/list/curruserdone", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<HistoricTaskInstanceVO> actHiTaskinstQueryByCurrUser(@RequestBody HistoricTaskInstanceVO inBean) throws Exception {
        inBean.setAssignee(DataBus.getLoginName());
        return actHiTaskinstQuery(inBean);
    }


    private HistoricTaskInstance transHistoricTaskInstanceBean(HistoricTaskInstanceVO inBean) {
        HistoricTaskInstanceEntity queryBean = new HistoricTaskInstanceEntity();
        queryBean.setId(inBean.getId());
        queryBean.setProcessDefinitionId(inBean.getProcDefId());
        queryBean.setTaskDefinitionKey(inBean.getTaskDefKey());
        queryBean.setProcessInstanceId(inBean.getProcInstId());
        queryBean.setExecutionId(inBean.getExecutionId());
        queryBean.setParentTaskId(inBean.getParentTaskId());
        queryBean.setName(inBean.getName());
        queryBean.setDescription(inBean.getDescription());
        queryBean.setOwner(null != inBean.getOwner() ? inBean.getOwner() : "");
        queryBean.setAssignee(null != inBean.getAssignee() ? inBean.getAssignee() : "");
        queryBean.setStartTime(AmsDateUtils.getDateFromString18(inBean.getStartTime()));
        queryBean.setClaimTime(AmsDateUtils.getDateFromString18(inBean.getClaimTime()));
        queryBean.setEndTime(AmsDateUtils.getDateFromString18(inBean.getEndTime()));
        queryBean.setDeleteReason(inBean.getDeleteReason());
        queryBean.setPriority(AmsUtils.isNotNull(inBean.getPriority()) ? inBean.getPriority() : 0);
        queryBean.setDueDate(AmsDateUtils.getDateFromString18(inBean.getDueDate()));
        queryBean.setFormKey(null != inBean.getFormKey() ? inBean.getFormKey() : "");
        queryBean.setCategory(null != inBean.getCategory() ? inBean.getCategory() : "");
        queryBean.setTenantId(null != inBean.getTenantId() ? inBean.getTenantId() : "");
        return queryBean;
    }

}
