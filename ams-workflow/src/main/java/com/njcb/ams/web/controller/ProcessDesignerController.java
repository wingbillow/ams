package com.njcb.ams.web.controller;

import java.io.File;
import java.util.List;

import org.activiti.engine.repository.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.njcb.ams.application.BusinessProcessManager;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.portal.SysBaseDefine;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 	流程设计器
 * 
 * @author liuyanlong
 *
 */
@RestController
@RequestMapping("/workflow/designer")
@Api(value = "workflow",tags = "workflow")
public class ProcessDesignerController {

	@Autowired
	private BusinessProcessManager businessManager;

	/**
	 * 新建一个空模型
	 * 
	 * @return
	 */
	@ApiOperation(value = "创建流程图", notes = "创建流程图")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public Response newModel() {
		String modelId = businessManager.newModel();
		Response response = Response.buildSucc();
		response.putExtField("id",modelId);
		return response;
	}

	@ApiOperation(value = "获取流程模型", notes = "获取流程模型")
	@RequestMapping(value = "match/{id}", method = RequestMethod.POST)
	@ResponseBody
	public PageResponse<Model> modelList(@PathVariable("id") String id) {
		List<Model> models = businessManager.modelList();
		return PageResponse.build(models);
	}

	@ApiOperation(value = "删除流程模型", notes = "删除流程模型")
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Response deleteModel(@PathVariable("id") String id) {
		businessManager.deleteModel(id);
		return Response.buildSucc();
	}

	@ApiOperation(value = "发布流程模型", notes = "发布流程模型")
	@RequestMapping(value = "{id}/deployment", method = RequestMethod.POST)
	@ResponseBody
	public Response deploy(@PathVariable("id") String id) throws Exception {
		businessManager.deploy(id);
		return Response.buildSucc();
	}
	
	@ApiOperation(value = "导出流程模型", notes = "导出流程模型")
	@RequestMapping(value = "{id}/export", method = RequestMethod.POST)
	@ResponseBody
	public Response export(@PathVariable("id") String id) throws Exception {
		String filePath = businessManager.export(id);
		return EntityResponse.build(filePath);
	}
	
	@ApiOperation(value = "导入流程模型", notes = "导入流程模型")
	@RequestMapping(value = "import", method = RequestMethod.POST)
	@ResponseBody
	public Response importModel(String fileName, @RequestParam("bpmnfile") MultipartFile bpmnfile) throws Exception {
		String orgFileName = bpmnfile.getOriginalFilename();
		File tmpbpmnfile = new File(SysBaseDefine.SYS_FILE_TMP_PATH , orgFileName);
		if (!bpmnfile.isEmpty()) {
			// 将上传文件保存到一个目标文件当中
			bpmnfile.transferTo(tmpbpmnfile);
		}
		businessManager.importModel(orgFileName, tmpbpmnfile.getPath());
		return Response.buildSucc();
	}
}
