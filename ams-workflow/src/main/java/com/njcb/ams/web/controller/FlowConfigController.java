package com.njcb.ams.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.application.BusinessWrokflowManager;
import com.njcb.ams.repository.entity.CommFlowHandleview;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.vo.FlowHandleViewVO;

import io.swagger.annotations.Api;

/**
 * 流程配置服务
 * 
 * @author liuyanlong
 *
 */
@RestController
@RequestMapping("/workflow/config")
@Api(value = "workflow", tags = "workflow")
public class FlowConfigController {

	@Autowired
	private BusinessWrokflowManager businessworkManager;
	/**
	 * 代办任务查询
	 * 
	 * @param inPage
	 * @param inBean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "handleview", method = RequestMethod.POST)
	@ResponseBody
	public EntityResponse<String> handleView(@RequestBody FlowHandleViewVO handleView) throws Exception {
		String proKey = handleView.getProcessDefKey().substring(0, handleView.getProcessDefKey().indexOf(":"));
		CommFlowHandleview config = businessworkManager.queryHandleView(proKey, handleView.getTaskDefKey());
		if(null != config && null != config.getHandleView() && config.getHandleView().length() > 0){
			return EntityResponse.build(config.getHandleView());
		}else{
			return EntityResponse.build("/html/workflow/dowork.html");
		}
	}


}
