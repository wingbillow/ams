/*
 * CommFlowHandleviewMapper.java
 * Copyright(C) trythis.cn
 * 2019年09月27日 15时46分18秒Created
 */
package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.CommFlowHandleview;
import com.njcb.ams.repository.entity.CommFlowHandleviewExample;
import org.springframework.stereotype.Component;

@Component
public interface CommFlowHandleviewMapper extends BaseMapper<CommFlowHandleview, CommFlowHandleviewExample, Integer> {
}