/*
 * CommFlowHandleview.java
 * Copyright(C) trythis.cn
 * 2019年09月27日 15时46分18秒Created
 */
package com.njcb.ams.repository.entity;

public class CommFlowHandleview {
    private Integer id;

    private String processDefKey;

    private String taskDefKey;

    private String handleView;

    private String conductUser;

    private String conductTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProcessDefKey() {
        return processDefKey;
    }

    public void setProcessDefKey(String processDefKey) {
        this.processDefKey = processDefKey;
    }

    public String getTaskDefKey() {
        return taskDefKey;
    }

    public void setTaskDefKey(String taskDefKey) {
        this.taskDefKey = taskDefKey;
    }

    public String getHandleView() {
        return handleView;
    }

    public void setHandleView(String handleView) {
        this.handleView = handleView;
    }

    public String getConductUser() {
        return conductUser;
    }

    public void setConductUser(String conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }
}