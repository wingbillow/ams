/*
 * CommFlowHandleviewExample.java
 * Copyright(C) trythis.cn
 * 2019年09月27日 15时46分18秒Created
 */
package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class CommFlowHandleviewExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommFlowHandleviewExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyIsNull() {
            addCriterion("PROCESS_DEF_KEY is null");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyIsNotNull() {
            addCriterion("PROCESS_DEF_KEY is not null");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyEqualTo(String value) {
            addCriterion("PROCESS_DEF_KEY =", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyNotEqualTo(String value) {
            addCriterion("PROCESS_DEF_KEY <>", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyGreaterThan(String value) {
            addCriterion("PROCESS_DEF_KEY >", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyGreaterThanOrEqualTo(String value) {
            addCriterion("PROCESS_DEF_KEY >=", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyLessThan(String value) {
            addCriterion("PROCESS_DEF_KEY <", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyLessThanOrEqualTo(String value) {
            addCriterion("PROCESS_DEF_KEY <=", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyLike(String value) {
            addCriterion("PROCESS_DEF_KEY like", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyNotLike(String value) {
            addCriterion("PROCESS_DEF_KEY not like", value, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyIn(List<String> values) {
            addCriterion("PROCESS_DEF_KEY in", values, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyNotIn(List<String> values) {
            addCriterion("PROCESS_DEF_KEY not in", values, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyBetween(String value1, String value2) {
            addCriterion("PROCESS_DEF_KEY between", value1, value2, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andProcessDefKeyNotBetween(String value1, String value2) {
            addCriterion("PROCESS_DEF_KEY not between", value1, value2, "processDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyIsNull() {
            addCriterion("TASK_DEF_KEY is null");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyIsNotNull() {
            addCriterion("TASK_DEF_KEY is not null");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyEqualTo(String value) {
            addCriterion("TASK_DEF_KEY =", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyNotEqualTo(String value) {
            addCriterion("TASK_DEF_KEY <>", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyGreaterThan(String value) {
            addCriterion("TASK_DEF_KEY >", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyGreaterThanOrEqualTo(String value) {
            addCriterion("TASK_DEF_KEY >=", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyLessThan(String value) {
            addCriterion("TASK_DEF_KEY <", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyLessThanOrEqualTo(String value) {
            addCriterion("TASK_DEF_KEY <=", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyLike(String value) {
            addCriterion("TASK_DEF_KEY like", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyNotLike(String value) {
            addCriterion("TASK_DEF_KEY not like", value, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyIn(List<String> values) {
            addCriterion("TASK_DEF_KEY in", values, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyNotIn(List<String> values) {
            addCriterion("TASK_DEF_KEY not in", values, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyBetween(String value1, String value2) {
            addCriterion("TASK_DEF_KEY between", value1, value2, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andTaskDefKeyNotBetween(String value1, String value2) {
            addCriterion("TASK_DEF_KEY not between", value1, value2, "taskDefKey");
            return (Criteria) this;
        }

        public Criteria andHandleViewIsNull() {
            addCriterion("HANDLE_VIEW is null");
            return (Criteria) this;
        }

        public Criteria andHandleViewIsNotNull() {
            addCriterion("HANDLE_VIEW is not null");
            return (Criteria) this;
        }

        public Criteria andHandleViewEqualTo(String value) {
            addCriterion("HANDLE_VIEW =", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewNotEqualTo(String value) {
            addCriterion("HANDLE_VIEW <>", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewGreaterThan(String value) {
            addCriterion("HANDLE_VIEW >", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewGreaterThanOrEqualTo(String value) {
            addCriterion("HANDLE_VIEW >=", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewLessThan(String value) {
            addCriterion("HANDLE_VIEW <", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewLessThanOrEqualTo(String value) {
            addCriterion("HANDLE_VIEW <=", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewLike(String value) {
            addCriterion("HANDLE_VIEW like", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewNotLike(String value) {
            addCriterion("HANDLE_VIEW not like", value, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewIn(List<String> values) {
            addCriterion("HANDLE_VIEW in", values, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewNotIn(List<String> values) {
            addCriterion("HANDLE_VIEW not in", values, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewBetween(String value1, String value2) {
            addCriterion("HANDLE_VIEW between", value1, value2, "handleView");
            return (Criteria) this;
        }

        public Criteria andHandleViewNotBetween(String value1, String value2) {
            addCriterion("HANDLE_VIEW not between", value1, value2, "handleView");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNull() {
            addCriterion("CONDUCT_USER is null");
            return (Criteria) this;
        }

        public Criteria andConductUserIsNotNull() {
            addCriterion("CONDUCT_USER is not null");
            return (Criteria) this;
        }

        public Criteria andConductUserEqualTo(String value) {
            addCriterion("CONDUCT_USER =", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotEqualTo(String value) {
            addCriterion("CONDUCT_USER <>", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThan(String value) {
            addCriterion("CONDUCT_USER >", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserGreaterThanOrEqualTo(String value) {
            addCriterion("CONDUCT_USER >=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThan(String value) {
            addCriterion("CONDUCT_USER <", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLessThanOrEqualTo(String value) {
            addCriterion("CONDUCT_USER <=", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserLike(String value) {
            addCriterion("CONDUCT_USER like", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotLike(String value) {
            addCriterion("CONDUCT_USER not like", value, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserIn(List<String> values) {
            addCriterion("CONDUCT_USER in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotIn(List<String> values) {
            addCriterion("CONDUCT_USER not in", values, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserBetween(String value1, String value2) {
            addCriterion("CONDUCT_USER between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductUserNotBetween(String value1, String value2) {
            addCriterion("CONDUCT_USER not between", value1, value2, "conductUser");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNull() {
            addCriterion("CONDUCT_TIME is null");
            return (Criteria) this;
        }

        public Criteria andConductTimeIsNotNull() {
            addCriterion("CONDUCT_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andConductTimeEqualTo(String value) {
            addCriterion("CONDUCT_TIME =", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotEqualTo(String value) {
            addCriterion("CONDUCT_TIME <>", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThan(String value) {
            addCriterion("CONDUCT_TIME >", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeGreaterThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME >=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThan(String value) {
            addCriterion("CONDUCT_TIME <", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLessThanOrEqualTo(String value) {
            addCriterion("CONDUCT_TIME <=", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeLike(String value) {
            addCriterion("CONDUCT_TIME like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotLike(String value) {
            addCriterion("CONDUCT_TIME not like", value, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeIn(List<String> values) {
            addCriterion("CONDUCT_TIME in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotIn(List<String> values) {
            addCriterion("CONDUCT_TIME not in", values, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME between", value1, value2, "conductTime");
            return (Criteria) this;
        }

        public Criteria andConductTimeNotBetween(String value1, String value2) {
            addCriterion("CONDUCT_TIME not between", value1, value2, "conductTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}