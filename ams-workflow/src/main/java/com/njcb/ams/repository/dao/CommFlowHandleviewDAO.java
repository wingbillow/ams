/*
 * CommFlowHandleview.java
 * Copyright(C) trythis.cn
 * 2019年09月27日 15时46分18秒Created
 */
package com.njcb.ams.repository.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.CommFlowHandleviewMapper;
import com.njcb.ams.repository.entity.CommFlowHandleview;
import com.njcb.ams.repository.entity.CommFlowHandleviewExample;

/**
 * @author LOONG
 */
@Repository
public class CommFlowHandleviewDAO extends BaseMyBatisDAO<CommFlowHandleview, CommFlowHandleviewExample, Integer> {
    @Autowired
    private CommFlowHandleviewMapper mapper;

    public CommFlowHandleviewDAO() {
        this.entityClass = CommFlowHandleview.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public CommFlowHandleviewMapper getMapper() {
        return mapper;
    }
    
    public CommFlowHandleview queryByProcessAndTask(String processDefKey,String taskDefKey){
    	CommFlowHandleviewExample example = new CommFlowHandleviewExample();
		example.createCriteria().andProcessDefKeyEqualTo(processDefKey).andTaskDefKeyEqualTo(taskDefKey);
		List<CommFlowHandleview> list = selectByExample(example);
		if(null == list || list.size() == 0){
			CommFlowHandleviewExample example2 = new CommFlowHandleviewExample();
			example2.createCriteria().andProcessDefKeyEqualTo(processDefKey);
			List<CommFlowHandleview> list2 = selectByExample(example2);
			if(null == list2 || list2.size() == 0){
				return new CommFlowHandleview();
			}else{
				return list2.get(0);
			}
		}else{
			return list.get(0);
		}
		
    }
}