package com.njcb.ams.service.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njcb.ams.repository.dao.CommFlowHandleviewDAO;
import com.njcb.ams.repository.entity.CommFlowHandleview;

/**
 * 流程配置服务，配置处理页面等业务相关参数
 * @author liuyanlong
 *
 */
@Service
public class WrokflowConfigService {
	@Autowired
	private CommFlowHandleviewDAO handleviewDAO;

	public CommFlowHandleview queryHandleView(String processDefKey,String taskDefKey) {
		CommFlowHandleview config = handleviewDAO.queryByProcessAndTask(processDefKey, taskDefKey);
		return config;
	}
}
