package com.njcb.ams.util;

public class WorkFlowVariable {
	
	
	public static final String APPROVAL_RESULT = "result";
	
	/**
	 * 审批意见枚举值
	 */
	public enum ApprovalResult {

		agree("同意"), refuse("拒绝"), disagree("不同意"), nothing("无关");

		/**
		 * 枚举中文含义
		 */
		public final String value;

		private ApprovalResult(String value) {
			this.value = value;
		}

		public String get() {
			return value;
		}

		/**
		 * 根据值初始化枚举
		 * 
		 * @param value
		 * @return
		 */
		public static ApprovalResult init(String value) {
			for (ApprovalResult result : ApprovalResult.values()) {
				if (result.value.equals(value)) {
					return result;
				}
			}
			return ApprovalResult.nothing;
		}
	}
}
