package com.njcb.ams.util;


import java.util.List;

import org.activiti.engine.query.NativeQuery;
import org.activiti.engine.query.Query;

import com.njcb.ams.store.page.Page;
import com.njcb.ams.store.page.PageHandle;

/**
 * 流程任务处理服务
 * 
 * @author liuyanlong
 *
 */
public class WrokflowServiceUtils {
	
	public static <T extends Query< ? , ? >, U extends Object> List<U> pageQuery(Query<T, U> query) {
		Page page = PageHandle.getPlatformPage();
		if(null != page){
			page.setTotal(query.count());
			return query.listPage(page.getStartRow(), page.getRows());
		}else{
			return query.list();
		}
	}
	
	
	public static <T extends NativeQuery< ? , ? >, U extends Object> List<U> pageQuery(long count, NativeQuery<T, U> query) {
		Page page = PageHandle.getPlatformPage();
		if(null != page){
			page.setTotal(count);
			return query.listPage(page.getStartRow(), page.getRows());
		}else{
			return query.list();
		}
	}
}
