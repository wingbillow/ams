package com.njcb.ams.bootconfig;

import org.activiti.engine.*;
import org.activiti.engine.impl.history.HistoryLevel;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author LOONG
 */
@Configuration
public class SpringActivitiConfig {
	private static final Logger logger = LoggerFactory.getLogger(SpringActivitiConfig.class);
	
	@Autowired
	DataSource dataSource;
	@Autowired
	PlatformTransactionManager transactionManager;

	@Bean(name = "processEngineConfiguration")
    public SpringProcessEngineConfiguration getProcessEngineConfiguration() throws IOException, FontFormatException {
		SpringProcessEngineConfiguration springProcessEngineConfiguration = new SpringProcessEngineConfiguration();
		springProcessEngineConfiguration.setDataSource(dataSource);
		springProcessEngineConfiguration.setTransactionManager(transactionManager);
		springProcessEngineConfiguration.setDatabaseSchemaUpdate("true");
		springProcessEngineConfiguration.setJobExecutorActivate(false);
		//保存完整历史
		springProcessEngineConfiguration.setHistoryLevel(HistoryLevel.FULL);
//		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//		springProcessEngineConfiguration.setDeploymentResources(resolver.getResources("classpath:resources/workflow/*.bpmn"));
		//注册字体
		ClassPathResource classPathResource = new ClassPathResource("simkai.ttf");
		InputStream inputStream =classPathResource.getInputStream();
		Font font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
		GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
		logger.info("字体注册成功，字体名称["+font.getName()+"]");
		springProcessEngineConfiguration.setActivityFontName(font.getName());
		springProcessEngineConfiguration.setAnnotationFontName(font.getName());
		springProcessEngineConfiguration.setLabelFontName(font.getName());
		return springProcessEngineConfiguration;
    }
	
	@Bean(name = "processEngine")
    public ProcessEngineFactoryBean getProcessEngine() throws Exception {
		ProcessEngineFactoryBean processEngineFactoryBean = new ProcessEngineFactoryBean();
		processEngineFactoryBean.setProcessEngineConfiguration(getProcessEngineConfiguration());
		return processEngineFactoryBean;  
    }
	
	@Bean(name = "repositoryService")
    public RepositoryService getRepositoryService(ProcessEngineFactoryBean processEngine) throws Exception {
		return processEngine.getObject().getRepositoryService();
    }
	
	@Bean(name = "runtimeService")
    public RuntimeService getRuntimeService() throws Exception {
		return getProcessEngine().getObject().getRuntimeService();
    }
	
	@Bean(name = "taskService")
    public TaskService getTaskService() throws Exception {
		return getProcessEngine().getObject().getTaskService();
    }
	
	@Bean(name = "historyService")
    public HistoryService getHistoryService() throws Exception {
		return getProcessEngine().getObject().getHistoryService();
    }
	
	@Bean(name = "managementService")
    public ManagementService getManagementService() throws Exception {
		return getProcessEngine().getObject().getManagementService();
    }
	
	@Bean(name = "identityService")
    public IdentityService getIdentityService() throws Exception {
		return getProcessEngine().getObject().getIdentityService();
    }
	
}
