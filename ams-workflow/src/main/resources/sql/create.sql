
insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3100, '业务流程', null, 0, 1, 1, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3110, '待办任务', 'html/workflow/todoTask.html', 3100, 2, 1, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3120, '已办任务', 'html/workflow/doneTask.html', 3100, 2, 2, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3200, '流程设置', null, 0, 1, 2, null, 'QITA', null, '1 ', null);


insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3240, '流程设计器', 'html/workflowManage/process/processDesigner.html', 3200, 2, 1, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3210, '已部署流程', 'html/workflowManage/process/processDefinition.html', 3200, 2, 2, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3220, '待办任务处理', 'html/workflowManage/taskinfo/todoTaskAll.html', 3200, 2, 3, null, 'QITA', null, '1 ', null);

insert into resource_info (ID, RESOURCE_NAME, ACCESS_PATH, PARENT_ID, RESOURCE_GRADE, RESOURCE_ORDER, RESOURCE_ATTR, RESOURCE_TYPE, RESOURCE_IMAGE, RESOURCE_STATUS, RESOURCE_DESC)
values (3230, '已办任务查询', 'html/workflowManage/taskinfo/doneTaskAll.html', 3200, 2, 4, null, 'QITA', null, '1 ', null);

-- Create table
create table COMM_FLOW_HANDLEVIEW
(
  id           INTEGER primary key,
  PROCESS_DEF_KEY  VARCHAR2(400 CHAR),
  TASK_DEF_KEY  VARCHAR2(400 CHAR),
  HANDLE_VIEW  VARCHAR2(400 CHAR),
  conduct_user VARCHAR2(255 CHAR),
  conduct_time CHAR(14)
);
-- Add comments to the table 
comment on table COMM_FLOW_HANDLEVIEW
  is '流程处理视图配置';
-- Add comments to the columns 
comment on column COMM_FLOW_HANDLEVIEW.id
  is '主键';
comment on column COMM_FLOW_HANDLEVIEW.PROCESS_DEF_KEY
  is '流程定义Key';
comment on column COMM_FLOW_HANDLEVIEW.TASK_DEF_KEY
  is '节点定义Key';
comment on column COMM_FLOW_HANDLEVIEW.HANDLE_VIEW
  is '处理页面';
comment on column COMM_FLOW_HANDLEVIEW.conduct_user
  is '操作人';
comment on column COMM_HOLIDAY_INFO.conduct_time
  is '操作时间';

