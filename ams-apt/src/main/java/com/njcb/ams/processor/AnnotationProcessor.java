package com.njcb.ams.processor;

import com.njcb.ams.annotation.TradeEnum;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author LOONG
 */
public class AnnotationProcessor extends AbstractProcessor {
    /**
     * 新生成文件后缀
     */
    private static final String GEN_CLASS_SUFFIX = "Enum";
    /**
     * 文件相关的辅助类 用于生成新的源文件、class等
     */
    private Filer filer;
    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(TradeEnum.class);
        for (Element element : elements) {
            if(element instanceof TypeElement){
                TypeElement typeElement = (TypeElement) element;
                TradeEnum dataDic = typeElement.getAnnotation(TradeEnum.class);
                String objectType = typeElement.getSimpleName().toString();
                Elements elementUtils = processingEnv.getElementUtils();
                Types typeUtils = processingEnv.getTypeUtils();
                List<? extends Element> members = typeElement.getEnclosedElements();
                for (Element member : members) {
                    ElementKind kind = member.getKind();
                    if(kind.isField() && (kind.name().equals(ElementKind.ENUM_CONSTANT.name()))){
                    }
                }
            }
        }
        messager.printMessage(Diagnostic.Kind.WARNING, "信息");
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> types = new LinkedHashSet<>();
        types.add(TradeEnum.class.getCanonicalName());
        return types;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

}
