package com.njcb.ams.support.comm;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import javax.annotation.Resource;

import com.njcb.ams.BaseTest;
import org.springframework.stereotype.Service;

import com.njcb.ams.support.exception.ExceptionUtil;


/**
 * 序号生成器
 * 
 * @author liuyanlong
 */
@Service
public class SequenceServiceTest extends BaseTest {

	@Resource
	private SequenceService service;

	/**
	 * 启动threadCount线程获取序列，每线程取sum个序列，放入MAP中(map不可重复)。
	 * 
	 * 如果MAP的长度等于threadCount乘sum, 则是线程安全的，否则不安全
	 */
	public void testGenSeq() {
		long start = System.currentTimeMillis();
		Map<Object, Object> map = new ConcurrentHashMap<Object, Object>();
		int sum = 200000;

		int threadCount = 10;
		final CountDownLatch c = new CountDownLatch(threadCount);
		for (int i = 0; i < threadCount; i++) {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < sum; i++) {
						Object seq = service.genSeqStr("demo");
						// System.out.println(seq);
						if (map.containsKey(seq)) {
							System.out.println("出现重复：" + seq);
						}
						map.put(seq, seq);
					}
					c.countDown();
				}
			});
			t.start();
		}
		try {
			c.await();
		} catch (InterruptedException e) {
			ExceptionUtil.printStackTrace(e);
		}

		System.out.println("map.size():" + map.size());
		System.out.println("sum * threadCount" + sum * threadCount);
		if (map.size() == sum * threadCount) {
			System.out.println("线程安全");
		} else {
			System.out.println("NONO线程不安全");
		}
		System.out.println("耗时:" + (System.currentTimeMillis() - start));

	}
}
