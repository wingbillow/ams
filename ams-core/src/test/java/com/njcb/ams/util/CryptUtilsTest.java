package com.njcb.ams.util;

import javax.crypto.SecretKey;

import com.njcb.ams.support.exception.ExceptionUtil;

public class CryptUtilsTest {

	public void test() {
		AmsCryptUtils crypt = new AmsCryptUtils();
		// 执行MD5加密"Hello world!"
		// 生成一个DES算法的密匙
		SecretKey key = crypt.createSecretKey("DES");
		// 用密匙加密信息"Hello world!"
		String str1 = crypt.encryptToDES(key, "Hello");
		// 使用这个密匙解密
		String str2 = crypt.decryptByDES(key, str1);
		System.out.println(str2);
		// 创建公匙和私匙
		crypt.createPairKey();
		// 对Hello world!使用私匙进行签名
		crypt.signToInfo("Hello", "mysign.bat");
		// 利用公匙对签名进行验证。
		// if (crypt.validateSign("mysign.bat")) {
		// } else {
		// }
	}

	public void testGetFileMD5() {
		try {
//			String ret = CryptUtils.getFileMD5("C:/BusinessWastebook_20170619.dat");
//			System.out.println(ret);
		} catch (Exception e) {
			ExceptionUtil.printStackTrace(e);
		}
	}
}