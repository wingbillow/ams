package com.njcb.ams.util;

import org.junit.Assert;
import org.junit.Test;

public class AmsDateUtilsTest {

	@Test
	public void testGetCurrentDate8() throws Exception {
		Assert.assertNotNull(AmsDateUtils.getCurrentDate6());
	}
	
	
	@Test
	public void testGetNextDate() throws Exception {
		String currDate = "20180925";
		Assert.assertEquals(AmsDateUtils.getNextDate(currDate), "20180926");
	}
	
	
}
