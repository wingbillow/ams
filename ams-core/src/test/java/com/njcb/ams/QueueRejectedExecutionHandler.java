package com.njcb.ams;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author liuyanlong
 *
 */
public class QueueRejectedExecutionHandler implements RejectedExecutionHandler {

	public QueueRejectedExecutionHandler() {
	}

	public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
		// throw new RejectedExecutionException("Task " + r.toString() + "
		// rejected from " + e.toString());
		if (!e.isShutdown()) {
			r.run();
		}
	}
}
