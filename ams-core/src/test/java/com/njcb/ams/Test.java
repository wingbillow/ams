package com.njcb.ams;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.njcb.ams.support.exception.ExceptionUtil;

public class Test {
	public static void main(String[] args) {
		new Test().readRequest();
	}

	private void readRequest() {
		ThreadFactory threadFactory = Executors.defaultThreadFactory();
		QueueRejectedExecutionHandler rejectedExecutionHandler = new QueueRejectedExecutionHandler();
		ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 10, 10, TimeUnit.SECONDS,
				new ArrayBlockingQueue<Runnable>(2), threadFactory, rejectedExecutionHandler);
		System.out.println("执行开始111111");
		for (int i = 0; i < 5; i++) {
			pool.execute(new QueueWork());
		}
		System.out.println("执行完成");
	}

	class QueueWork implements Runnable {

		public QueueWork() {
		}

		@Override
		public void run() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				ExceptionUtil.printStackTrace(e);
			}
		}

	}
}
