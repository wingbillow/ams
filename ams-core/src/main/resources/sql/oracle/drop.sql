-- Drop table
drop table SYS_INFO cascade constraints;
-- Drop table
drop table SYS_TRADE_LOG cascade constraints;
-- Drop table
drop table SYS_TRADE_CONSOLE cascade constraints;
-- Drop table
drop table SYS_SEQ_CONTROL cascade constraints;
-- Drop table
drop table SYS_PARAM cascade constraints;
-- Drop table
drop table SYS_ALTER_RECORD cascade constraints;
-- Drop table
drop table SYS_EXCEPTION_INFO cascade constraints;