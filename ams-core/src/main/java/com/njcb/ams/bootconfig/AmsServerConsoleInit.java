package com.njcb.ams.bootconfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.njcb.ams.store.stable.TradeConsoleService;

/**
 * 初始化服务控制
 * @author LOONG
 */
@Component
@Lazy(false)
public class AmsServerConsoleInit implements ApplicationListener<ContextRefreshedEvent>{
	private static final Logger logger = LoggerFactory.getLogger(AmsServerConsoleInit.class);

	@Autowired
	private TradeConsoleService tradeConsoleService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("--------------------初始化服务控制---------------------");
		tradeConsoleService.loadServerConsole();
		logger.info("--------------------初始化服务完成---------------------");
	}

}
