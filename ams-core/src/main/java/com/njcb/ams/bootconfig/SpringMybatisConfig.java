package com.njcb.ams.bootconfig;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import com.njcb.ams.repository.dao.base.intercept.DatabaseChainInterceptor;
import com.njcb.ams.repository.dao.base.intercept.OpeInfoInterceptor;
import com.njcb.ams.store.page.interceptor.PageInterceptor;

import java.util.Properties;

/**
 * @author LOONG
 */
@Configuration
@EnableTransactionManagement
public class SpringMybatisConfig implements TransactionManagementConfigurer{
	
	@Autowired
	DataSource dataSource;

	/**
	 * mybatis利用DatabaseId支持oracle和mysql
	 * @return
	 */
	@Bean
	public DatabaseIdProvider getDatabaseIdProvider() {
		DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
		Properties properties = new Properties();
		properties.setProperty("Oracle","oracle");
		properties.setProperty("MySQL","mysql");
		databaseIdProvider.setProperties(properties);
		return databaseIdProvider;
	}

	@Bean(name = "sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource);
		factoryBean.setDatabaseIdProvider(getDatabaseIdProvider());
		factoryBean.setTypeAliasesPackage("com.njcb.**.entity.**");
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		factoryBean.setMapperLocations(resolver.getResources("classpath:com/njcb/**/mapper/**.xml"));
		PageInterceptor pageInterceptor = new PageInterceptor();
		OpeInfoInterceptor opeInfo = new OpeInfoInterceptor();
		DatabaseChainInterceptor databaseChain = new DatabaseChainInterceptor();
		Interceptor[] plugins = new Interceptor[]{pageInterceptor,opeInfo,databaseChain};
		factoryBean.setPlugins(plugins);
		return factoryBean.getObject();
	}
	
	@Override
	@Bean(name="transactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}
	
	@Bean(name="JdbcTemplate")
	public JdbcTemplate jdbcTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		return jdbcTemplate;
	}

	
}
