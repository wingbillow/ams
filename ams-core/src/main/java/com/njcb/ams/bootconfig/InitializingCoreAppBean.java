package com.njcb.ams.bootconfig;

import java.text.SimpleDateFormat;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 应用级bean初始化
 * @author LOONG
 *
 */
@Configuration
public class InitializingCoreAppBean {

	/**
	 * json映射对象
	 * @return
	 */
	@Bean(name = "objectMapper")
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		return objectMapper;
	}

	/**
	 * 对象转换映射工厂
	 * @return
	 */
	@Bean(name = "mapperFactory")
	public MapperFactory mapperFactory() {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		return mapperFactory;
	}
}
