package com.njcb.ams.bootconfig;

import com.njcb.ams.portal.SysBaseDefine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 在特定环境可见
 * @author LOONG
 */
@EnableWebMvc
@EnableSwagger2
@ComponentScan(basePackages = { "com.njcb" })
@Configuration
public class SwaggerApiConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket createRestApi() {
	Docket docket = new Docket(DocumentationType.SWAGGER_2);
	docket.apiInfo(apiInfo());
	docket.select()
		.apis(RequestHandlerSelectors
			.withClassAnnotation(io.swagger.annotations.Api.class))
		.paths(PathSelectors.any()).build();
	return docket;
    }

    private ApiInfo apiInfo() {

	return new ApiInfoBuilder()
		.title(SysBaseDefine.SYS_NAME + "的RESTful接口文档")
		.description("按照RESTful的思想构建" + SysBaseDefine.SYS_NAME
			+ "的服务,暂适用于MVC Controller 的交易，继续考虑webservice服务的处理。")
		.contact(new Contact(SysBaseDefine.PLATFORM_CONTACT_AUTHOR,
			SysBaseDefine.PLATFORM_WEBSITE,
			SysBaseDefine.PLATFORM_CONTACT_MAIL))
		.version(SysBaseDefine.PLATFORM_VERSION).build();
    }

}