package com.njcb.ams.bootconfig;

import com.njcb.ams.util.SysInfoUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author liuyanlong
 */
@Configuration
@EnableScheduling
public class SpringScheduledConfig {

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void updateCacheSysInfo() {
		SysInfoUtils.updateCacheSysInfo();
    }

}
