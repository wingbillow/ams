package com.njcb.ams.store.authcode;

import java.awt.Font;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;

import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * ttf字体文件
 * 
 * @author loong
 *
 */
public class ImgFontByte {
	static Font baseFont = null;
	
	public Font getFont(int fontHeight) {
		try {
//			Font baseFont = Font.createFont(Font.TRUETYPE_FONT, new ByteArrayInputStream(hex2byte(getFontByteStr())));
			if(null == baseFont){
				return new Font("Arial", Font.PLAIN, fontHeight);
			}
			return baseFont.deriveFont(Font.PLAIN, fontHeight);
		} catch (Exception e) {
			ExceptionUtil.printStackTrace(e);
			return new Font("Arial", Font.PLAIN, fontHeight);
		}
	}

	static {
		try {
			File file = new File(ImgFontByte.class.getClassLoader().getResource("/").getPath() + "resources/validate.ttf");
			if(file.exists()){
				FileInputStream aixing = new FileInputStream(file);
				baseFont = Font.createFont(Font.TRUETYPE_FONT, aixing).deriveFont(18f);
				aixing.close();
			}else{
				baseFont =  Font.createFont(Font.TRUETYPE_FONT, new ByteArrayInputStream(hex2byte("")));
			}
			
			
		} catch (Exception e) {
			ExceptionUtil.printStackTrace(e);
		}

	}
	
	private static byte[] hex2byte(String str) {
		if (str == null) {
			return null;
		}
		str = str.trim();
		int len = str.length();
		if (len == 0 || len % 2 > 0) {
			return null;
		}

		byte[] b = new byte[len / 2];
		try {
			for (int i = 0; i < str.length(); i += 2) {
				b[i / 2] = (byte) Integer.decode("0x" + str.substring(i, i + 2)).intValue();
			}
			return b;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * ttf字体文件的十六进制字符串
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getFontByteStr() {
		return "";
	}
}