package com.njcb.ams.store.text.transform;

import java.util.Hashtable;

/**
 * 文本解析接口
 * 
 * @author 刘彦龙
 *
 */
public interface DataBase {
    /**
     * 检查数据
     * @param map
     * @return
     */
    public int dataCheckAndConvert(Hashtable<String, String> map);

    /**
     * 数据导出
     * @param obj
     * @throws Exception
     */
    public void doDataProcess(Object... obj) throws Exception;
}
