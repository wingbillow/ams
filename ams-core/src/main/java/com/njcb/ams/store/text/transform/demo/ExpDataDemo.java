package com.njcb.ams.store.text.transform.demo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Hashtable;

import com.njcb.ams.store.text.transform.ExpDataBase;
import com.njcb.ams.store.text.transform.TableToText;
import com.njcb.ams.store.text.transform.UtilsCommon;

/**
 * 生成核心商票表外销账文件
 *
 * @author fxyy025
 *
 */

public class ExpDataDemo implements ExpDataBase {

	@Override
	public void doDataProcess(Object... obj) throws Exception {
		Connection conn = (Connection) obj[0];
		String fileName = (String) obj[1];
		String bhDate = (String) obj[2];

		TableToText dataTableToText = new TableToText();
		String fileFields = "ID,ONEDISCOUNT_BRANCH_NO,<0>,CORE_ACCNO,FACE_AMOUNT,<0.00>,<商票表外>";
		dataTableToText.setFileFields(fileFields);
		String whereStr = "WHERE ACC_DATE = '" + bhDate + "' AND ACC_DATE != WRITEOFF_DATE ";
		dataTableToText.setConn(conn);
		dataTableToText.setTableName("FILE_TRADE_BILL_OFFBALANCE");
		dataTableToText.setFileName(fileName);
		dataTableToText.setWhereStr(whereStr);
		dataTableToText.setSepertor(",");
		dataTableToText.setOKfile(true);
		dataTableToText.addDataCheck(this);
		dataTableToText.doExportProcess();

		TableToText dataTableToText2 = new TableToText();
		String fileFields2 = "ID,ONEDISCOUNT_BRANCH_NO,<1>,CORE_ACCNO,<0.00>,FACE_AMOUNT,<商票表外>";
		dataTableToText2.setFileFields(fileFields2);
		String whereStr2 = "WHERE WRITEOFF_DATE = '" + bhDate + "' AND ACC_DATE != WRITEOFF_DATE ";
		dataTableToText2.setConn(conn);
		dataTableToText2.setTableName("FILE_TRADE_BILL_OFFBALANCE");
		dataTableToText2.setFileName(fileName);
		dataTableToText2.setWhereStr(whereStr2);
		dataTableToText2.setSepertor(",");
		dataTableToText2.setOKfile(true);
		dataTableToText2.setAppend(true);
		dataTableToText2.addDataCheck(this);
		dataTableToText2.doExportProcess();

	}

	@Override
	public int dataCheckAndConvert(Hashtable<String, String> map) {
		if (map.containsKey("FACE_AMOUNT")) {
			DecimalFormat formatter = new DecimalFormat("############0.00");
			String value = formatter.format(new BigDecimal(map.get("FACE_AMOUNT")));
			map.put("FACE_AMOUNT", value);
		}
		return UtilsCommon.CHECK_RETURN_CODE_SUCCESS;
	}

//	public static void main(String[] args) {
//		try {
//			String url = "jdbc:oracle:thin:@159.156.1.53:1521:njbank";
//			String drive = "oracle.jdbc.driver.OracleDriver";
//			Class.forName(drive);
//			Connection conn = DriverManager.getConnection(url, "bms", "bms");
//			ExpDataDemo upinfo = new ExpDataDemo();
//			String fileName = "C:\\Users\\fxyy025\\Desktop\\daochu.txt";
//			upinfo.doDataProcess(conn, fileName, "20150613");
//		} catch (Exception e) {
//			ExceptionUtil.printStackTrace(e);
//		}
//	}
}
