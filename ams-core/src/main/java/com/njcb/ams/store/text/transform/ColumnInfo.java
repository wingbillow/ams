package com.njcb.ams.store.text.transform;

/**
 * 数据字段信息
 * 
 * @author 刘彦龙
 */
public class ColumnInfo {
	private String columnName;
	private String typeName;
	private int dataType;

	public ColumnInfo(String columnName, String typeName, int dataType) {
		this.columnName = columnName;
		this.typeName = typeName;
		this.dataType = dataType;
	}

	/** 获取字段名称 */
	public String getColumnName() {
		return columnName;
	}

	/** 设置字段名称 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/** 获取字段类型 */
	public int getDataType() {
		return dataType;
	}

	/** 设置字段类型 */
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	/** 获取字段类型名称 */
	public String getTypeName() {
		return typeName;
	}

	/** 设置字段类型名称 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		return "columnName=" + columnName + ",typeName=" + typeName;
	}
}
