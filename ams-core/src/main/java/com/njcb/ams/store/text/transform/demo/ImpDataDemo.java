package com.njcb.ams.store.text.transform.demo;

import java.sql.Connection;
import java.util.Hashtable;

import com.njcb.ams.store.text.transform.ImpDataBase;
import com.njcb.ams.store.text.transform.TextToTable;

/**
 * @author srxhx207
 */
public class ImpDataDemo implements ImpDataBase {

	@Override
    public int dataCheckAndConvert(Hashtable<String, String> map) {
		// 1有效,0无效数据跳过
		int result = 1;
		return result;
	}

	@Override
	public int[] doDataProcess(Object... obj) throws Exception {
		Connection conn = (Connection) obj[0];
		String fileName = (String) obj[1];
		String bhDate = (String) obj[2];
		String fileFields = "ZNGZLX,JIOYRQ,GNLISJ,ZHNGJG,HUOBDH,YEWUDH,YUEEXZ,SQJFYE,SQDFYE,KAIHSH,XIOHSH,JIEFSE,DAIFSE,JIEFBS,DAIFBS,JIEFYE,DAIFYE,JIEFJS,DAIFJS,SQJCHS,JIECHS,SHJNCH,JILUZT";
		TextToTable dataImportToTable = new TextToTable(conn, "AYWZZ", fileName);
		// dataImportToTable.setSequenceForID("SEQ_SWT_ACCPT_PTCPT");
		dataImportToTable.addFileFields(fileFields);
		dataImportToTable.setStaticData("SHUJRQ", bhDate);
		dataImportToTable.setStaticData("ZNGZLX", "");
		int[] count = new int[2];
		try {
			conn.setAutoCommit(false);
			dataImportToTable.addDataCheck(this);
			dataImportToTable.deleteData(" SHUJRQ = '" + bhDate + "'");
			count = dataImportToTable.doImportProcess();
			return count;
		} catch (Exception e) {
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
	}

//	public static void main(String[] args) {
//		try {
//			String url = "jdbc:oracle:thin:@159.1.64.25:1521:pjdb";
//			String user = "bms";
//			String pass = "bms";
//			String drive = "oracle.jdbc.driver.OracleDriver";
//			Class.forName(drive);
//			Connection conn = DriverManager.getConnection(url, user, pass);
//			ImpDataDemo upinfo = new ImpDataDemo();
//			String fileName = "C:\\Users\\fxyy025\\Desktop\\20150630_aywzz.txt";
//			upinfo.doDataProcess(conn, fileName, "20150630");
//		} catch (Exception e) {
//			ExceptionUtil.printStackTrace(e);
//		}
//
//	}

}
