/**
 * 交易服务层
 * curd操作及cqrs模式可直接调用dao服务，
 * 其他业务场景调用领域服务层domainService
 * 
 * @author liuyanlong
 */
package com.njcb.ams.application;