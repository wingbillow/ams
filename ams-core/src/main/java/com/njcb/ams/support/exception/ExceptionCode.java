package com.njcb.ams.support.exception;

/**
 * 业务错误输出定义
 * @author loong
 *
 */
public enum ExceptionCode implements ErrorCode {

	/**
	 * 系统默认错误码
	 */
	DEFAULT_EXCEPTION("9999"),
	/**
	 * 未定义的错误码
	 */
	UNDEFINED_EXCEPTION("9990"),
	/**
	 * 数据上下文错误
	 */
	NOTFOUND_DATA_BUS("0001"),
	/**
	 * 序列没找到
	 */
	SEQ_VALUE_NO_NOT_FOUND("0002"),

	/**
	 * 参数验证不通过
	 */
	PARAM_VALIDATE_FAIL("0003"),

	/**
	 * json解析异常
	 */
	JSON_PARSE_EXCEPTION("1005"),
	
	PROCESSING_RETRY("9001"),
	
	AUTH_INSUFFLCIENT("7001");
	
	private final String code;

	private ExceptionCode(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return code;
	}
	
	@Override
	public String toString() {
		return this.code;
	}

}
