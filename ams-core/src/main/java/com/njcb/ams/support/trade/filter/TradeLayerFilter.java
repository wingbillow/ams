package com.njcb.ams.support.trade.filter;

/**
 * 
 * @author liuyanlong
 *
 * 类功能描述：聚合服务交易过滤器
 *
 */
public interface TradeLayerFilter {
	Object doFilter(TradeFilterChain filterChain) throws Throwable;
}
