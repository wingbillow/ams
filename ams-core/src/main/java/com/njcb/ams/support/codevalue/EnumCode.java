package com.njcb.ams.support.codevalue;

import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * @author liuyanlong
 */
public interface EnumCode {
    /**
     * 获取枚举代码
     *
     * @return
     */
    String getCode();

    /**
     * 获取枚举代码说明
     *
     * @return
     */
    String getDesc();

    /**
     * 判断美剧代码是否指定代码
     *
     * @param code
     * @return
     */
    default Boolean is(String code) {
        if (code == null) {
            return Boolean.FALSE;
        }
        return this.getCode().equals(code);
    }

    /**
     * code转换码值枚举
     *
     * @param enumClass
     * @param code
     * @param <E>
     * @return
     */
    static <E extends Enum<E>> E valueOfCode(Class<E> enumClass, String code) {
        if (!EnumCode.class.isAssignableFrom(enumClass)) {
            ExceptionUtil.throwAppException("[" + enumClass.getSimpleName() + "]不是派生自EnumCode");
        }
        EnumCode[] enumArr = (EnumCode[]) enumClass.getEnumConstants();
        for (EnumCode e : enumArr) {
            if (code.equals(e.getCode())) {
                return (E) e;
            }
        }
        return null;
    }

    /**
     * 获取描述文本
     *
     * @return
     */
    default String getText() {
        return getCode() + "-" + getDesc();
    }
}
