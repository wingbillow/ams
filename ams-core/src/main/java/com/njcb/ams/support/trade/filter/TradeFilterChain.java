package com.njcb.ams.support.trade.filter;

import org.aspectj.lang.ProceedingJoinPoint;

import com.njcb.ams.support.annotation.Trader;

/**
 * @author LOONG
 */
public class TradeFilterChain implements TradeLayerFilter {
	//当前执行filter的offset
	private int pos = 0;
	//当前filter的数量
	private int count = 0;
	private TradeLayerFilter[] filters;
	private ProceedingJoinPoint joinPoint;
	private Trader traderAnnotation;
	
	public TradeFilterChain(ProceedingJoinPoint pjd, Trader anm, TradeLayerFilter[] filters) {
		this.joinPoint = pjd;
		this.traderAnnotation = anm;
		this.filters = filters;
		count = filters.length;
	}
	
	@Override
	public Object doFilter(TradeFilterChain filterChain) throws Throwable {
		if(pos < count){
			TradeLayerFilter tradeFilter = filters[pos++];
			return tradeFilter.doFilter(filterChain);
		}else{
			return joinPoint.proceed();
		}
	}
	
	
	public Trader getTraderAnnotation() {
		return traderAnnotation;
	}
	
	public ProceedingJoinPoint getJoinPoint() {
		return joinPoint;
	}
	
	
}
