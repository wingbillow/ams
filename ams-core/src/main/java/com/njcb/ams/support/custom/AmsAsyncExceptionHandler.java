package com.njcb.ams.support.custom;

import java.lang.reflect.Method;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import com.njcb.ams.support.exception.ExceptionUtil;

/**
 * @author liuyanlong
 */
public class AmsAsyncExceptionHandler implements AsyncUncaughtExceptionHandler{

	@Override
	public void handleUncaughtException(Throwable throwable, Method method, Object... args) {
		ExceptionUtil.printStackTrace(throwable);
	}

}
