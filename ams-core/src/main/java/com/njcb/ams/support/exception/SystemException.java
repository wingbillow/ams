package com.njcb.ams.support.exception;

/**
 * @author LOONG
 */
public interface SystemException {
	/**
	 * 错误码
	 * @return
	 */
	String getErrorCode();
}
