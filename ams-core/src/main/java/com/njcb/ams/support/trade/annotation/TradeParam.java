package com.njcb.ams.support.trade.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 作用于应用交易层Trade,当使用TradeService.execute()调用交易时,
 * execute传入参数与交易参数个数或名称不匹配,利用此注解在交易中标记参数可辅助参数映射。
 * 
 * @see com.njcb.ams.support.trade.TradeService#execute(String, Object, Class)
 * 
 * @author liuyanlong
 *
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TradeParam {
	String value() default "";
}
