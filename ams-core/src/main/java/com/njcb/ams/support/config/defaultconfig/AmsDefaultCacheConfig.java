package com.njcb.ams.support.config.defaultconfig;

import com.njcb.ams.support.config.AmsCacheConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 缓存配置
 */
@Component
@Configuration
public class AmsDefaultCacheConfig implements AmsCacheConfiguration {
	
}
