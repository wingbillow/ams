package com.njcb.ams.support.exception;

public interface ErrorCode {
	String getCode();
}
