package com.njcb.ams.support.log;

import com.njcb.ams.factory.comm.DataBus;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * 用于日志中获取交易代码
 * @author liuyanlong
 */
public class TradeCodeConvert extends ClassicConverter {

	@Override
	public String convert(ILoggingEvent event) {
		DataBus globalInfo = DataBus.getUncheckInstance();
		if (null != globalInfo && null != DataBus.getTrader()) {
			return DataBus.getTrader().tradeCode();
		} else {
			return "";
		}
	}
}
