package com.njcb.ams.support.plug;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * ams平台插件接口
 * @author LOONG
 */
public interface Plug {

    /**
     * 插件名称
     * @return
     */
    String plugName();

    /**
     * 系统初始化
     */
    default void start(){

    }

    /**
     * 系统停止
     */
    default void stop(){

    }

    /**
     * 添加扫描的注解类型
     * @return
     */
    default List<Class> annotationType(){
        return new ArrayList<>();
    }

    /**
     * 处理被注解的对象
     * @param bean
     * @param annotation
     */
    default void annotationBean(Object bean, Annotation annotation){

    }
}
