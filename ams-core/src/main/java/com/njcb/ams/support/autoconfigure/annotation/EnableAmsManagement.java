package com.njcb.ams.support.autoconfigure.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.njcb.ams.support.autoconfigure.AmsPackageScan;

/**
 * @author LOONG
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(AmsPackageScan.class)
@Documented
@EnableCaching
@EnableTransactionManagement
@MapperScan("com.njcb.*.mapper")
@ServletComponentScan("com.njcb.**.filter")
public @interface EnableAmsManagement {
    String[] scanBasePackages() default {};
}
