package com.njcb.ams.support.log;

import com.njcb.ams.portal.SysBaseDefine;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * 用于日志中获取系统编码
 * 
 * @author liuyanlong
 */
public class SysIdConvert extends ClassicConverter {

	@Override
	public String convert(ILoggingEvent event) {
		return SysBaseDefine.SYS_ID;
	}

}
