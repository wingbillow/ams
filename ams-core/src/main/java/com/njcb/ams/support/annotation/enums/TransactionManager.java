package com.njcb.ams.support.annotation.enums;

/**
 * 枚举类型
 * @author LIUYANLONG
 *
 */
public enum TransactionManager {
	/**
     * 主事物管理器
     */
	GLOBAL,
	/**
	 * 从事物管理器
	 */
	REGION,
	/**
	 * 无事物
	 */
	NONE;
}
