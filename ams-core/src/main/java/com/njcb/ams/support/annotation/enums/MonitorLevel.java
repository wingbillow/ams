package com.njcb.ams.support.annotation.enums;

/**
 * 枚举,服务监管级别
 * @author LIUYANLONG
 *
 */
public enum MonitorLevel {
	/**
     * 完全监管
     */
	FULL_MONITOR,
	/**
	 *日志监管
	 */
	LOG_MONITOR,
	/**
	 *服务监管
	 */
	SERVER_MONITOR,
	/**
	 *无监管
	 */
	NO_MONITOR;

}
