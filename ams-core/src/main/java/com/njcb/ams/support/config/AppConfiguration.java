package com.njcb.ams.support.config;

/**
 * 系统回调服务，用于设置应用版本，初始化。销毁服务等
 * @author liuyanlong
 *
 */
public interface AppConfiguration {
	/**
	 * 应用系统版本号
	 * @return
	 */
	public String getAppVersion();
	
	/**
	 * 系统初始化调用
	 * @throws Exception
	 */
	public void init() throws Exception;
	
	/**
	 * 系统销毁调用
	 */
	public void destroy();
}
