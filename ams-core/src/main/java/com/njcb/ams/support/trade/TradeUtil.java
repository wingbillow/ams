package com.njcb.ams.support.trade;

import com.njcb.ams.factory.comm.DataBus;

public class TradeUtil {
	private static String TRADE_UTIL = "TRADE_UTIL";
	
	private String tradeCode;
	private String tradeName;
	private String reqSeq;
    private String globalSeq;
	private String transSeq;
	private String busiSign;
	private String remark;
	
	public static void setReqSeq(String reqSeq) {
		TradeUtil head = getInstance();
		head.reqSeq = reqSeq;
	}
	
	public static String getReqSeq() {
		TradeUtil head = getInstance();
		return head.reqSeq;
	}
	
	public static void setGlobalSeq(String globalSeq) {
		TradeUtil head = getInstance();
		head.globalSeq = globalSeq;
	}
	
	public static String getGlobalSeq() {
		TradeUtil head = getInstance();
		return head.globalSeq;
	}
	
	public static void setTransSeq(String transSeq) {
		TradeUtil head = getInstance();
		head.transSeq = transSeq;
	}
	
	public static String getTransSeq() {
		TradeUtil head = getInstance();
		return head.transSeq;
	}
	
	public static void setTradeCode(String tradeCode) {
		TradeUtil head = getInstance();
		head.tradeCode = tradeCode;
	}
	
	public static String getTradeCode() {
		TradeUtil head = getInstance();
		return head.tradeCode;
	}
	
	public static void setTradeName(String tradeName) {
		TradeUtil head = getInstance();
		head.tradeName = tradeName;
	}
	
	public static String getTradeName() {
		TradeUtil head = getInstance();
		return head.tradeName;
	}

	public static void setBusiSign(String busiSign) {
		TradeUtil head = getInstance();
		head.busiSign = busiSign;
	}

	public static String getBusiSign() {
		TradeUtil head = getInstance();
		return head.busiSign;
	}

	public static void setRemark(String remark) {
		TradeUtil head = getInstance();
		head.remark = remark;
	}

	public static String getRemark() {
		TradeUtil head = getInstance();
		return head.remark;
	}

	/**
	 * 方法功能描述：初始化TradeUtil
	 * @return
	 */
	private static TradeUtil getInstance() {
		DataBus globalInfo = DataBus.getUncheckInstance();
		if (null != globalInfo) {
			TradeUtil head = (TradeUtil) DataBus.getAttribute(TRADE_UTIL);
			if (null == head) {
				head = new TradeUtil();
			}
			DataBus.setAttribute(TRADE_UTIL, head);
			return head;
		}else{
			TradeUtil head = new TradeUtil();
			DataBus.setAttribute(TRADE_UTIL, head);
			return head;
		}
	}
	
}
