package com.njcb.ams.support.annotation.enums;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * 枚举类型
 * @author LIUYANLONG
 *
 */
@DataDic(dataType = "TradeType", dataTypeName = "交易类型")
public enum TradeType implements EnumCode {
	/**
     * 查询类
     */
	TYPE_QUERY("10","查询类"),
	/**
	 *交易类
	 */
	TYPE_TRANS("20","交易类");

	private String code;
	private String desc;

	TradeType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDesc() {
		return this.desc;
	}
}
