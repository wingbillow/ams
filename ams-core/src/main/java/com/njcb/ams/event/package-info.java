/**
 * 事件驱动基础设施包
 * 计划提供事件驱动基础开发支持逻辑
 * 
 * @author liuyanlong
 */
package com.njcb.ams.event;