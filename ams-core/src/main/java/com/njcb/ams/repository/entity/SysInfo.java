package com.njcb.ams.repository.entity;

import java.io.Serializable;

/**
 * @author liuyanlong
 */
public class SysInfo implements Serializable{
	private static final long serialVersionUID = 509228664797491422L;
    /**
     * 运行状态 1日间,2日终，3切日失败
     */
	public static final String RUN_STATUS_1 = "1";
	public static final String RUN_STATUS_2 = "2";
	public static final String RUN_STATUS_3 = "3";

    /**
     * 系统日期取值方式
     * 1 物理日期
     * 2 逻辑日期
     */
	public static final String INSP_TYPE_1 = "1";
	public static final String INSP_TYPE_2 = "2";

	private Integer id;

    private String sysId;

    private String sysShort;

    private String sysName;

    private String channelId;

    private String busiDate;

    private String lbatDate;

    private String nbatDate;

    private String status;
    //检查类型
    private String inspType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getSysShort() {
        return sysShort;
    }

    public void setSysShort(String sysShort) {
        this.sysShort = sysShort;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getBusiDate() {
        return busiDate;
    }

    public void setBusiDate(String busiDate) {
        this.busiDate = busiDate;
    }

    public String getLbatDate() {
        return lbatDate;
    }

    public void setLbatDate(String lbatDate) {
        this.lbatDate = lbatDate;
    }

    public String getNbatDate() {
        return nbatDate;
    }

    public void setNbatDate(String nbatDate) {
        this.nbatDate = nbatDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInspType() {
		return inspType;
	}
    
    public void setInspType(String inspType) {
		this.inspType = inspType;
	}

	@Override
	public String toString() {
		return "SysInfo [id=" + id + ", sysId=" + sysId + ", sysShort=" + sysShort + ", sysName=" + sysName
				+ ", channelId=" + channelId + ", busiDate=" + busiDate + ", lbatDate=" + lbatDate + ", nbatDate="
				+ nbatDate + ", status=" + status + ", inspType=" + inspType + "]";
	}
    
}