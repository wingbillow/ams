package com.njcb.ams.repository.dao;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysInfoMapper;
import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.repository.entity.SysInfoExample;
import com.njcb.ams.util.AmsDateUtils;
import com.njcb.ams.util.SysInfoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author
 */
@Repository
public class SysInfoDAO extends BaseMyBatisDAO<SysInfo, SysInfoExample, Integer> {
	private static final Logger logger = LoggerFactory.getLogger(SysInfoDAO.class);

	public SysInfoDAO() {
		this.entityClass = SysInfo.class;
	}

	@Autowired
	private AmsProperties amsProperties;
	
	@Autowired
	private SysInfoMapper mapper;

	@Override
	@SuppressWarnings("unchecked")
	public SysInfoMapper getMapper() {
		return mapper;
	}

	@Cacheable(value = "UserTempParamCache", key = "'SysInfo'")
	public SysInfo selectSysInfo() {
		logger.debug("获取交易日期查缓存未命中，查询数据库");
		if(amsProperties.getDbEnable()){
			return mapper.selectByPrimaryKey(1);
		}else{
			SysInfo sysInfo = new SysInfo();
			sysInfo.setId(1);
			sysInfo.setSysId(amsProperties.getSysId());
			sysInfo.setSysShort(amsProperties.getSysShort());
			sysInfo.setSysName(amsProperties.getSysName());
			sysInfo.setChannelId(amsProperties.getChannelId());
			sysInfo.setBusiDate(AmsDateUtils.getCurrentDate8());
    		sysInfo.setLbatDate(AmsDateUtils.getBeforeDate(sysInfo.getBusiDate()));
    		sysInfo.setNbatDate(AmsDateUtils.getNextDate(sysInfo.getBusiDate()));
			return sysInfo;
		}
		
	}

	public SysInfo selectSysInfoForUpdate() {
		return mapper.selectByPrimaryKeyForUpdate(1);
	}

	public List<SysInfo> selectAll() {
		return mapper.selectByExample(new SysInfoExample());
	}
	
	@Override
	@CacheEvict(value = "UserTempParamCache", key = "'SysInfo'")
	public Integer updateByPrimaryKey(SysInfo sysInfo) {
		Integer count = mapper.updateByPrimaryKey(sysInfo);
		SysInfoUtils.updateCacheSysInfo();
		return count;
	}

	@Override
	@CacheEvict(value = "UserTempParamCache", key = "'SysInfo'")
	public Integer updateByPrimaryKeySelective(SysInfo sysInfo) {
		Integer count = mapper.updateByPrimaryKeySelective(sysInfo);
		SysInfoUtils.updateCacheSysInfo();
		return count;
	}

}
