package com.njcb.ams.repository.dao.mapper;

import com.njcb.ams.repository.entity.SysInfo;
import com.njcb.ams.repository.entity.SysInfoExample;
import org.springframework.stereotype.Component;

/**
 * @author LOONG
 */
@Component
public interface SysInfoMapper extends BaseMapper<SysInfo, SysInfoExample, Integer> {
}