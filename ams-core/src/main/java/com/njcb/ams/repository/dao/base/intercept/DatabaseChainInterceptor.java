package com.njcb.ams.repository.dao.base.intercept;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.factory.comm.DataBus;
import com.njcb.ams.store.stable.TradeConsoleService;

/**
 * @author liuyanlong
 */
@Intercepts(value = {
        @Signature(type = Executor.class,
                method = "update",
                args = {MappedStatement.class, Object.class}),
        @Signature(type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class,
                        CacheKey.class, BoundSql.class}),
        @Signature(type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class DatabaseChainInterceptor implements Interceptor {
	private static final Logger logger = LoggerFactory.getLogger(DatabaseChainInterceptor.class);

	@Override
    public Object intercept(Invocation invocation) throws Throwable {
		executeDatabaseChain(invocation);
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
    
	private void executeDatabaseChain(Invocation invocation) throws InvocationTargetException, IllegalAccessException {
		//仅debug模式下执行
		if(!logger.isDebugEnabled()){
			return;
		}
		
		if (null == DataBus.getUncheckInstance() || null == DataBus.getTrader()) {
			return;
		}
		Object target = invocation.getTarget();
		if (target instanceof Executor) {
			final Object[] args = invocation.getArgs();
			MappedStatement ms = (MappedStatement) args[0];
			Object parameterObject = args[1];
			if (parameterObject instanceof MappedStatement) {

			}
			BoundSql boundSql = ms.getBoundSql(parameterObject);
			String sql = boundSql.getSql();
			TradeConsoleService tradeConsoleService = TradeConsoleService.getInstance();
			if (null == tradeConsoleService) {
				return;
			}
			
			//更新交易控制信息中统计的SQL
			SysTradeConsole tcb = tradeConsoleService.getTradeConsoleInfo(DataBus.getTrader().tradeCode());
			if(StringUtils.isEmpty(tcb.getDatabaseChain()) || !tcb.getDatabaseChain().contains(sql)){
				if(StringUtils.isEmpty(tcb.getDatabaseChain())){
					tcb.setDatabaseChain(sql);
				}else{
					tcb.setDatabaseChain(tcb.getDatabaseChain() + "|" + sql);
				}
			}
			
		}
	}
	
}
