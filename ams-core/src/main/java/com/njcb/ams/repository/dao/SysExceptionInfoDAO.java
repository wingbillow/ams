package com.njcb.ams.repository.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SysExceptionInfoDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Map<String,String> getList() {
		StringBuilder sbsql = new StringBuilder(200);
		sbsql.append(" select code, text, type ");
		sbsql.append("   from SYS_EXCEPTION_INFO ");
		sbsql.append("  ");
		List<Map<String, Object>> retList = jdbcTemplate.queryForList(sbsql.toString());
		Map<String,String> rtMap = new HashMap<>();
		for (Map<String, Object> map : retList) {
			String code = (String) map.get("code");
			String text = (String) map.get("text");
			rtMap.put(code, text);
		}
		return rtMap;
	}

}
