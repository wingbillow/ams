package com.njcb.ams.repository.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SysTradeConsoleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysTradeConsoleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIsNull() {
            addCriterion("TRADE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIsNotNull() {
            addCriterion("TRADE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andTradeCodeEqualTo(String value) {
            addCriterion("TRADE_CODE =", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotEqualTo(String value) {
            addCriterion("TRADE_CODE <>", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeGreaterThan(String value) {
            addCriterion("TRADE_CODE >", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_CODE >=", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLessThan(String value) {
            addCriterion("TRADE_CODE <", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLessThanOrEqualTo(String value) {
            addCriterion("TRADE_CODE <=", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeLike(String value) {
            addCriterion("TRADE_CODE like", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotLike(String value) {
            addCriterion("TRADE_CODE not like", value, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeIn(List<String> values) {
            addCriterion("TRADE_CODE in", values, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotIn(List<String> values) {
            addCriterion("TRADE_CODE not in", values, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeBetween(String value1, String value2) {
            addCriterion("TRADE_CODE between", value1, value2, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeCodeNotBetween(String value1, String value2) {
            addCriterion("TRADE_CODE not between", value1, value2, "tradeCode");
            return (Criteria) this;
        }

        public Criteria andTradeNameIsNull() {
            addCriterion("TRADE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andTradeNameIsNotNull() {
            addCriterion("TRADE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andTradeNameEqualTo(String value) {
            addCriterion("TRADE_NAME =", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotEqualTo(String value) {
            addCriterion("TRADE_NAME <>", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameGreaterThan(String value) {
            addCriterion("TRADE_NAME >", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_NAME >=", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLessThan(String value) {
            addCriterion("TRADE_NAME <", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLessThanOrEqualTo(String value) {
            addCriterion("TRADE_NAME <=", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameLike(String value) {
            addCriterion("TRADE_NAME like", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotLike(String value) {
            addCriterion("TRADE_NAME not like", value, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameIn(List<String> values) {
            addCriterion("TRADE_NAME in", values, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotIn(List<String> values) {
            addCriterion("TRADE_NAME not in", values, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameBetween(String value1, String value2) {
            addCriterion("TRADE_NAME between", value1, value2, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeNameNotBetween(String value1, String value2) {
            addCriterion("TRADE_NAME not between", value1, value2, "tradeName");
            return (Criteria) this;
        }

        public Criteria andTradeGroupIsNull() {
            addCriterion("TRADE_GROUP is null");
            return (Criteria) this;
        }

        public Criteria andTradeGroupIsNotNull() {
            addCriterion("TRADE_GROUP is not null");
            return (Criteria) this;
        }

        public Criteria andTradeGroupEqualTo(String value) {
            addCriterion("TRADE_GROUP =", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupNotEqualTo(String value) {
            addCriterion("TRADE_GROUP <>", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupGreaterThan(String value) {
            addCriterion("TRADE_GROUP >", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_GROUP >=", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupLessThan(String value) {
            addCriterion("TRADE_GROUP <", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupLessThanOrEqualTo(String value) {
            addCriterion("TRADE_GROUP <=", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupLike(String value) {
            addCriterion("TRADE_GROUP like", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupNotLike(String value) {
            addCriterion("TRADE_GROUP not like", value, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupIn(List<String> values) {
            addCriterion("TRADE_GROUP in", values, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupNotIn(List<String> values) {
            addCriterion("TRADE_GROUP not in", values, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupBetween(String value1, String value2) {
            addCriterion("TRADE_GROUP between", value1, value2, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andTradeGroupNotBetween(String value1, String value2) {
            addCriterion("TRADE_GROUP not between", value1, value2, "tradeGroup");
            return (Criteria) this;
        }

        public Criteria andMaxExecIsNull() {
            addCriterion("MAX_EXEC is null");
            return (Criteria) this;
        }

        public Criteria andMaxExecIsNotNull() {
            addCriterion("MAX_EXEC is not null");
            return (Criteria) this;
        }

        public Criteria andMaxExecEqualTo(Integer value) {
            addCriterion("MAX_EXEC =", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecNotEqualTo(Integer value) {
            addCriterion("MAX_EXEC <>", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecGreaterThan(Integer value) {
            addCriterion("MAX_EXEC >", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecGreaterThanOrEqualTo(Integer value) {
            addCriterion("MAX_EXEC >=", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecLessThan(Integer value) {
            addCriterion("MAX_EXEC <", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecLessThanOrEqualTo(Integer value) {
            addCriterion("MAX_EXEC <=", value, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecIn(List<Integer> values) {
            addCriterion("MAX_EXEC in", values, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecNotIn(List<Integer> values) {
            addCriterion("MAX_EXEC not in", values, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecBetween(Integer value1, Integer value2) {
            addCriterion("MAX_EXEC between", value1, value2, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxExecNotBetween(Integer value1, Integer value2) {
            addCriterion("MAX_EXEC not between", value1, value2, "maxExec");
            return (Criteria) this;
        }

        public Criteria andMaxRateIsNull() {
            addCriterion("MAX_RATE is null");
            return (Criteria) this;
        }

        public Criteria andMaxRateIsNotNull() {
            addCriterion("MAX_RATE is not null");
            return (Criteria) this;
        }

        public Criteria andMaxRateEqualTo(Integer value) {
            addCriterion("MAX_RATE =", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateNotEqualTo(Integer value) {
            addCriterion("MAX_RATE <>", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateGreaterThan(Integer value) {
            addCriterion("MAX_RATE >", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateGreaterThanOrEqualTo(Integer value) {
            addCriterion("MAX_RATE >=", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateLessThan(Integer value) {
            addCriterion("MAX_RATE <", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateLessThanOrEqualTo(Integer value) {
            addCriterion("MAX_RATE <=", value, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateIn(List<Integer> values) {
            addCriterion("MAX_RATE in", values, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateNotIn(List<Integer> values) {
            addCriterion("MAX_RATE not in", values, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateBetween(Integer value1, Integer value2) {
            addCriterion("MAX_RATE between", value1, value2, "maxRate");
            return (Criteria) this;
        }

        public Criteria andMaxRateNotBetween(Integer value1, Integer value2) {
            addCriterion("MAX_RATE not between", value1, value2, "maxRate");
            return (Criteria) this;
        }

        public Criteria andCurrExecIsNull() {
            addCriterion("CURR_EXEC is null");
            return (Criteria) this;
        }

        public Criteria andCurrExecIsNotNull() {
            addCriterion("CURR_EXEC is not null");
            return (Criteria) this;
        }

        public Criteria andCurrExecEqualTo(Integer value) {
            addCriterion("CURR_EXEC =", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecNotEqualTo(Integer value) {
            addCriterion("CURR_EXEC <>", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecGreaterThan(Integer value) {
            addCriterion("CURR_EXEC >", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecGreaterThanOrEqualTo(Integer value) {
            addCriterion("CURR_EXEC >=", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecLessThan(Integer value) {
            addCriterion("CURR_EXEC <", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecLessThanOrEqualTo(Integer value) {
            addCriterion("CURR_EXEC <=", value, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecIn(List<Integer> values) {
            addCriterion("CURR_EXEC in", values, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecNotIn(List<Integer> values) {
            addCriterion("CURR_EXEC not in", values, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecBetween(Integer value1, Integer value2) {
            addCriterion("CURR_EXEC between", value1, value2, "currExec");
            return (Criteria) this;
        }

        public Criteria andCurrExecNotBetween(Integer value1, Integer value2) {
            addCriterion("CURR_EXEC not between", value1, value2, "currExec");
            return (Criteria) this;
        }

        public Criteria andTimeCycleIsNull() {
            addCriterion("TIME_CYCLE is null");
            return (Criteria) this;
        }

        public Criteria andTimeCycleIsNotNull() {
            addCriterion("TIME_CYCLE is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCycleEqualTo(Integer value) {
            addCriterion("TIME_CYCLE =", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleNotEqualTo(Integer value) {
            addCriterion("TIME_CYCLE <>", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleGreaterThan(Integer value) {
            addCriterion("TIME_CYCLE >", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleGreaterThanOrEqualTo(Integer value) {
            addCriterion("TIME_CYCLE >=", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleLessThan(Integer value) {
            addCriterion("TIME_CYCLE <", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleLessThanOrEqualTo(Integer value) {
            addCriterion("TIME_CYCLE <=", value, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleIn(List<Integer> values) {
            addCriterion("TIME_CYCLE in", values, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleNotIn(List<Integer> values) {
            addCriterion("TIME_CYCLE not in", values, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleBetween(Integer value1, Integer value2) {
            addCriterion("TIME_CYCLE between", value1, value2, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andTimeCycleNotBetween(Integer value1, Integer value2) {
            addCriterion("TIME_CYCLE not between", value1, value2, "timeCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleIsNull() {
            addCriterion("NUMBER_CYCLE is null");
            return (Criteria) this;
        }

        public Criteria andNumberCycleIsNotNull() {
            addCriterion("NUMBER_CYCLE is not null");
            return (Criteria) this;
        }

        public Criteria andNumberCycleEqualTo(Integer value) {
            addCriterion("NUMBER_CYCLE =", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleNotEqualTo(Integer value) {
            addCriterion("NUMBER_CYCLE <>", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleGreaterThan(Integer value) {
            addCriterion("NUMBER_CYCLE >", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleGreaterThanOrEqualTo(Integer value) {
            addCriterion("NUMBER_CYCLE >=", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleLessThan(Integer value) {
            addCriterion("NUMBER_CYCLE <", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleLessThanOrEqualTo(Integer value) {
            addCriterion("NUMBER_CYCLE <=", value, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleIn(List<Integer> values) {
            addCriterion("NUMBER_CYCLE in", values, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleNotIn(List<Integer> values) {
            addCriterion("NUMBER_CYCLE not in", values, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleBetween(Integer value1, Integer value2) {
            addCriterion("NUMBER_CYCLE between", value1, value2, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andNumberCycleNotBetween(Integer value1, Integer value2) {
            addCriterion("NUMBER_CYCLE not between", value1, value2, "numberCycle");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedIsNull() {
            addCriterion("TIME_SPEED is null");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedIsNotNull() {
            addCriterion("TIME_SPEED is not null");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedEqualTo(Integer value) {
            addCriterion("TIME_SPEED =", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedNotEqualTo(Integer value) {
            addCriterion("TIME_SPEED <>", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedGreaterThan(Integer value) {
            addCriterion("TIME_SPEED >", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedGreaterThanOrEqualTo(Integer value) {
            addCriterion("TIME_SPEED >=", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedLessThan(Integer value) {
            addCriterion("TIME_SPEED <", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedLessThanOrEqualTo(Integer value) {
            addCriterion("TIME_SPEED <=", value, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedIn(List<Integer> values) {
            addCriterion("TIME_SPEED in", values, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedNotIn(List<Integer> values) {
            addCriterion("TIME_SPEED not in", values, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedBetween(Integer value1, Integer value2) {
            addCriterion("TIME_SPEED between", value1, value2, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeSpeedNotBetween(Integer value1, Integer value2) {
            addCriterion("TIME_SPEED not between", value1, value2, "timeSpeed");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumIsNull() {
            addCriterion("TIME_TAKE_NUM is null");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumIsNotNull() {
            addCriterion("TIME_TAKE_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumEqualTo(Integer value) {
            addCriterion("TIME_TAKE_NUM =", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumNotEqualTo(Integer value) {
            addCriterion("TIME_TAKE_NUM <>", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumGreaterThan(Integer value) {
            addCriterion("TIME_TAKE_NUM >", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("TIME_TAKE_NUM >=", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumLessThan(Integer value) {
            addCriterion("TIME_TAKE_NUM <", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumLessThanOrEqualTo(Integer value) {
            addCriterion("TIME_TAKE_NUM <=", value, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumIn(List<Integer> values) {
            addCriterion("TIME_TAKE_NUM in", values, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumNotIn(List<Integer> values) {
            addCriterion("TIME_TAKE_NUM not in", values, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumBetween(Integer value1, Integer value2) {
            addCriterion("TIME_TAKE_NUM between", value1, value2, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeTakeNumNotBetween(Integer value1, Integer value2) {
            addCriterion("TIME_TAKE_NUM not between", value1, value2, "timeTakeNum");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateIsNull() {
            addCriterion("TIME_SUCC_RATE is null");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateIsNotNull() {
            addCriterion("TIME_SUCC_RATE is not null");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateEqualTo(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE =", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateNotEqualTo(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE <>", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateGreaterThan(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE >", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE >=", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateLessThan(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE <", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("TIME_SUCC_RATE <=", value, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateIn(List<BigDecimal> values) {
            addCriterion("TIME_SUCC_RATE in", values, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateNotIn(List<BigDecimal> values) {
            addCriterion("TIME_SUCC_RATE not in", values, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("TIME_SUCC_RATE between", value1, value2, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andTimeSuccRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("TIME_SUCC_RATE not between", value1, value2, "timeSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateIsNull() {
            addCriterion("NUMBER_SUCC_RATE is null");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateIsNotNull() {
            addCriterion("NUMBER_SUCC_RATE is not null");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateEqualTo(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE =", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateNotEqualTo(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE <>", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateGreaterThan(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE >", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE >=", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateLessThan(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE <", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("NUMBER_SUCC_RATE <=", value, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateIn(List<BigDecimal> values) {
            addCriterion("NUMBER_SUCC_RATE in", values, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateNotIn(List<BigDecimal> values) {
            addCriterion("NUMBER_SUCC_RATE not in", values, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("NUMBER_SUCC_RATE between", value1, value2, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andNumberSuccRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("NUMBER_SUCC_RATE not between", value1, value2, "numberSuccRate");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIsNull() {
            addCriterion("TRADE_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIsNotNull() {
            addCriterion("TRADE_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andTradeStatusEqualTo(String value) {
            addCriterion("TRADE_STATUS =", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotEqualTo(String value) {
            addCriterion("TRADE_STATUS <>", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusGreaterThan(String value) {
            addCriterion("TRADE_STATUS >", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_STATUS >=", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusLessThan(String value) {
            addCriterion("TRADE_STATUS <", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusLessThanOrEqualTo(String value) {
            addCriterion("TRADE_STATUS <=", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusLike(String value) {
            addCriterion("TRADE_STATUS like", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotLike(String value) {
            addCriterion("TRADE_STATUS not like", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIn(List<String> values) {
            addCriterion("TRADE_STATUS in", values, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotIn(List<String> values) {
            addCriterion("TRADE_STATUS not in", values, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusBetween(String value1, String value2) {
            addCriterion("TRADE_STATUS between", value1, value2, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotBetween(String value1, String value2) {
            addCriterion("TRADE_STATUS not between", value1, value2, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeMsgIsNull() {
            addCriterion("TRADE_MSG is null");
            return (Criteria) this;
        }

        public Criteria andTradeMsgIsNotNull() {
            addCriterion("TRADE_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andTradeMsgEqualTo(String value) {
            addCriterion("TRADE_MSG =", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgNotEqualTo(String value) {
            addCriterion("TRADE_MSG <>", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgGreaterThan(String value) {
            addCriterion("TRADE_MSG >", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgGreaterThanOrEqualTo(String value) {
            addCriterion("TRADE_MSG >=", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgLessThan(String value) {
            addCriterion("TRADE_MSG <", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgLessThanOrEqualTo(String value) {
            addCriterion("TRADE_MSG <=", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgLike(String value) {
            addCriterion("TRADE_MSG like", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgNotLike(String value) {
            addCriterion("TRADE_MSG not like", value, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgIn(List<String> values) {
            addCriterion("TRADE_MSG in", values, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgNotIn(List<String> values) {
            addCriterion("TRADE_MSG not in", values, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgBetween(String value1, String value2) {
            addCriterion("TRADE_MSG between", value1, value2, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andTradeMsgNotBetween(String value1, String value2) {
            addCriterion("TRADE_MSG not between", value1, value2, "tradeMsg");
            return (Criteria) this;
        }

        public Criteria andImplClassIsNull() {
            addCriterion("IMPL_CLASS is null");
            return (Criteria) this;
        }

        public Criteria andImplClassIsNotNull() {
            addCriterion("IMPL_CLASS is not null");
            return (Criteria) this;
        }

        public Criteria andImplClassEqualTo(String value) {
            addCriterion("IMPL_CLASS =", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassNotEqualTo(String value) {
            addCriterion("IMPL_CLASS <>", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassGreaterThan(String value) {
            addCriterion("IMPL_CLASS >", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassGreaterThanOrEqualTo(String value) {
            addCriterion("IMPL_CLASS >=", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassLessThan(String value) {
            addCriterion("IMPL_CLASS <", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassLessThanOrEqualTo(String value) {
            addCriterion("IMPL_CLASS <=", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassLike(String value) {
            addCriterion("IMPL_CLASS like", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassNotLike(String value) {
            addCriterion("IMPL_CLASS not like", value, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassIn(List<String> values) {
            addCriterion("IMPL_CLASS in", values, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassNotIn(List<String> values) {
            addCriterion("IMPL_CLASS not in", values, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassBetween(String value1, String value2) {
            addCriterion("IMPL_CLASS between", value1, value2, "implClass");
            return (Criteria) this;
        }

        public Criteria andImplClassNotBetween(String value1, String value2) {
            addCriterion("IMPL_CLASS not between", value1, value2, "implClass");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}