package com.njcb.ams.repository.entity;

import java.util.ArrayList;
import java.util.List;

public class SysInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSysIdIsNull() {
            addCriterion("SYS_ID is null");
            return (Criteria) this;
        }

        public Criteria andSysIdIsNotNull() {
            addCriterion("SYS_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSysIdEqualTo(String value) {
            addCriterion("SYS_ID =", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotEqualTo(String value) {
            addCriterion("SYS_ID <>", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdGreaterThan(String value) {
            addCriterion("SYS_ID >", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdGreaterThanOrEqualTo(String value) {
            addCriterion("SYS_ID >=", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLessThan(String value) {
            addCriterion("SYS_ID <", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLessThanOrEqualTo(String value) {
            addCriterion("SYS_ID <=", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLike(String value) {
            addCriterion("SYS_ID like", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotLike(String value) {
            addCriterion("SYS_ID not like", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdIn(List<String> values) {
            addCriterion("SYS_ID in", values, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotIn(List<String> values) {
            addCriterion("SYS_ID not in", values, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdBetween(String value1, String value2) {
            addCriterion("SYS_ID between", value1, value2, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotBetween(String value1, String value2) {
            addCriterion("SYS_ID not between", value1, value2, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysShortIsNull() {
            addCriterion("SYS_SHORT is null");
            return (Criteria) this;
        }

        public Criteria andSysShortIsNotNull() {
            addCriterion("SYS_SHORT is not null");
            return (Criteria) this;
        }

        public Criteria andSysShortEqualTo(String value) {
            addCriterion("SYS_SHORT =", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortNotEqualTo(String value) {
            addCriterion("SYS_SHORT <>", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortGreaterThan(String value) {
            addCriterion("SYS_SHORT >", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortGreaterThanOrEqualTo(String value) {
            addCriterion("SYS_SHORT >=", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortLessThan(String value) {
            addCriterion("SYS_SHORT <", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortLessThanOrEqualTo(String value) {
            addCriterion("SYS_SHORT <=", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortLike(String value) {
            addCriterion("SYS_SHORT like", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortNotLike(String value) {
            addCriterion("SYS_SHORT not like", value, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortIn(List<String> values) {
            addCriterion("SYS_SHORT in", values, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortNotIn(List<String> values) {
            addCriterion("SYS_SHORT not in", values, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortBetween(String value1, String value2) {
            addCriterion("SYS_SHORT between", value1, value2, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysShortNotBetween(String value1, String value2) {
            addCriterion("SYS_SHORT not between", value1, value2, "sysShort");
            return (Criteria) this;
        }

        public Criteria andSysNameIsNull() {
            addCriterion("SYS_NAME is null");
            return (Criteria) this;
        }

        public Criteria andSysNameIsNotNull() {
            addCriterion("SYS_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andSysNameEqualTo(String value) {
            addCriterion("SYS_NAME =", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameNotEqualTo(String value) {
            addCriterion("SYS_NAME <>", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameGreaterThan(String value) {
            addCriterion("SYS_NAME >", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameGreaterThanOrEqualTo(String value) {
            addCriterion("SYS_NAME >=", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameLessThan(String value) {
            addCriterion("SYS_NAME <", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameLessThanOrEqualTo(String value) {
            addCriterion("SYS_NAME <=", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameLike(String value) {
            addCriterion("SYS_NAME like", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameNotLike(String value) {
            addCriterion("SYS_NAME not like", value, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameIn(List<String> values) {
            addCriterion("SYS_NAME in", values, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameNotIn(List<String> values) {
            addCriterion("SYS_NAME not in", values, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameBetween(String value1, String value2) {
            addCriterion("SYS_NAME between", value1, value2, "sysName");
            return (Criteria) this;
        }

        public Criteria andSysNameNotBetween(String value1, String value2) {
            addCriterion("SYS_NAME not between", value1, value2, "sysName");
            return (Criteria) this;
        }

        public Criteria andChannelIdIsNull() {
            addCriterion("CHANNEL_ID is null");
            return (Criteria) this;
        }

        public Criteria andChannelIdIsNotNull() {
            addCriterion("CHANNEL_ID is not null");
            return (Criteria) this;
        }

        public Criteria andChannelIdEqualTo(String value) {
            addCriterion("CHANNEL_ID =", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdNotEqualTo(String value) {
            addCriterion("CHANNEL_ID <>", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdGreaterThan(String value) {
            addCriterion("CHANNEL_ID >", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL_ID >=", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdLessThan(String value) {
            addCriterion("CHANNEL_ID <", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL_ID <=", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdLike(String value) {
            addCriterion("CHANNEL_ID like", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdNotLike(String value) {
            addCriterion("CHANNEL_ID not like", value, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdIn(List<String> values) {
            addCriterion("CHANNEL_ID in", values, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdNotIn(List<String> values) {
            addCriterion("CHANNEL_ID not in", values, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdBetween(String value1, String value2) {
            addCriterion("CHANNEL_ID between", value1, value2, "channelId");
            return (Criteria) this;
        }

        public Criteria andChannelIdNotBetween(String value1, String value2) {
            addCriterion("CHANNEL_ID not between", value1, value2, "channelId");
            return (Criteria) this;
        }

        public Criteria andBusiDateIsNull() {
            addCriterion("BUSI_DATE is null");
            return (Criteria) this;
        }

        public Criteria andBusiDateIsNotNull() {
            addCriterion("BUSI_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andBusiDateEqualTo(String value) {
            addCriterion("BUSI_DATE =", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateNotEqualTo(String value) {
            addCriterion("BUSI_DATE <>", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateGreaterThan(String value) {
            addCriterion("BUSI_DATE >", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateGreaterThanOrEqualTo(String value) {
            addCriterion("BUSI_DATE >=", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateLessThan(String value) {
            addCriterion("BUSI_DATE <", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateLessThanOrEqualTo(String value) {
            addCriterion("BUSI_DATE <=", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateLike(String value) {
            addCriterion("BUSI_DATE like", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateNotLike(String value) {
            addCriterion("BUSI_DATE not like", value, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateIn(List<String> values) {
            addCriterion("BUSI_DATE in", values, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateNotIn(List<String> values) {
            addCriterion("BUSI_DATE not in", values, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateBetween(String value1, String value2) {
            addCriterion("BUSI_DATE between", value1, value2, "busiDate");
            return (Criteria) this;
        }

        public Criteria andBusiDateNotBetween(String value1, String value2) {
            addCriterion("BUSI_DATE not between", value1, value2, "busiDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateIsNull() {
            addCriterion("LBAT_DATE is null");
            return (Criteria) this;
        }

        public Criteria andLbatDateIsNotNull() {
            addCriterion("LBAT_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andLbatDateEqualTo(String value) {
            addCriterion("LBAT_DATE =", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateNotEqualTo(String value) {
            addCriterion("LBAT_DATE <>", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateGreaterThan(String value) {
            addCriterion("LBAT_DATE >", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateGreaterThanOrEqualTo(String value) {
            addCriterion("LBAT_DATE >=", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateLessThan(String value) {
            addCriterion("LBAT_DATE <", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateLessThanOrEqualTo(String value) {
            addCriterion("LBAT_DATE <=", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateLike(String value) {
            addCriterion("LBAT_DATE like", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateNotLike(String value) {
            addCriterion("LBAT_DATE not like", value, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateIn(List<String> values) {
            addCriterion("LBAT_DATE in", values, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateNotIn(List<String> values) {
            addCriterion("LBAT_DATE not in", values, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateBetween(String value1, String value2) {
            addCriterion("LBAT_DATE between", value1, value2, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andLbatDateNotBetween(String value1, String value2) {
            addCriterion("LBAT_DATE not between", value1, value2, "lbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateIsNull() {
            addCriterion("NBAT_DATE is null");
            return (Criteria) this;
        }

        public Criteria andNbatDateIsNotNull() {
            addCriterion("NBAT_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andNbatDateEqualTo(String value) {
            addCriterion("NBAT_DATE =", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateNotEqualTo(String value) {
            addCriterion("NBAT_DATE <>", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateGreaterThan(String value) {
            addCriterion("NBAT_DATE >", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateGreaterThanOrEqualTo(String value) {
            addCriterion("NBAT_DATE >=", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateLessThan(String value) {
            addCriterion("NBAT_DATE <", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateLessThanOrEqualTo(String value) {
            addCriterion("NBAT_DATE <=", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateLike(String value) {
            addCriterion("NBAT_DATE like", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateNotLike(String value) {
            addCriterion("NBAT_DATE not like", value, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateIn(List<String> values) {
            addCriterion("NBAT_DATE in", values, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateNotIn(List<String> values) {
            addCriterion("NBAT_DATE not in", values, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateBetween(String value1, String value2) {
            addCriterion("NBAT_DATE between", value1, value2, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andNbatDateNotBetween(String value1, String value2) {
            addCriterion("NBAT_DATE not between", value1, value2, "nbatDate");
            return (Criteria) this;
        }

        public Criteria andInspTypeIsNull() {
            addCriterion("INSP_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andInspTypeIsNotNull() {
            addCriterion("INSP_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andInspTypeEqualTo(String value) {
            addCriterion("INSP_TYPE =", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeNotEqualTo(String value) {
            addCriterion("INSP_TYPE <>", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeGreaterThan(String value) {
            addCriterion("INSP_TYPE >", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeGreaterThanOrEqualTo(String value) {
            addCriterion("INSP_TYPE >=", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeLessThan(String value) {
            addCriterion("INSP_TYPE <", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeLessThanOrEqualTo(String value) {
            addCriterion("INSP_TYPE <=", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeLike(String value) {
            addCriterion("INSP_TYPE like", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeNotLike(String value) {
            addCriterion("INSP_TYPE not like", value, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeIn(List<String> values) {
            addCriterion("INSP_TYPE in", values, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeNotIn(List<String> values) {
            addCriterion("INSP_TYPE not in", values, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeBetween(String value1, String value2) {
            addCriterion("INSP_TYPE between", value1, value2, "inspType");
            return (Criteria) this;
        }

        public Criteria andInspTypeNotBetween(String value1, String value2) {
            addCriterion("INSP_TYPE not between", value1, value2, "inspType");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}