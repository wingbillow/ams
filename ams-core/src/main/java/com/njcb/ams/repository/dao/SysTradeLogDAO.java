package com.njcb.ams.repository.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.njcb.ams.bootconfig.AmsProperties;
import com.njcb.ams.repository.dao.base.BaseMyBatisDAO;
import com.njcb.ams.repository.dao.mapper.SysTradeLogMapper;
import com.njcb.ams.repository.entity.SysTradeConsole;
import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.repository.entity.SysTradeLogExample;

@Repository
public class SysTradeLogDAO extends BaseMyBatisDAO<SysTradeLog, SysTradeLogExample, Integer>{

	public SysTradeLogDAO() {
		this.entityClass = SysTradeLog.class;
	}

	@Autowired
	private SysTradeLogMapper mapper;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private AmsProperties amsProperties;

	@Override
	@SuppressWarnings("unchecked")
	public SysTradeLogMapper getMapper() {
		return mapper;
	}

	@Override
    public Integer insert(SysTradeLog tradeLog) {
		if(!amsProperties.getDbEnable()){
			return 1;
		}
		return jdbcTemplate.update(
				"insert into SYS_TRADE_LOG (TRADE_CODE, TRADE_NAME, TRADE_TYPE, TRADE_NODE, USE_TIME, REQ_SEQ, GLOBAL_SEQ, TRANS_SEQ, REQ_DATE, REQ_TIME, SERVICE_ID, SVC_SCN, RESP_SEQ, RESP_DATE, "
						+ "RESP_TIME, RESP_RCV_TIME, TRANS_STATUS, RET_CODE, RET_MSG, BUSI_SIGN, TIME_STAMP, APP_VERSION, REMARK) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps) throws SQLException {
						ps.setString(1, tradeLog.getTradeCode());
						ps.setString(2, tradeLog.getTradeName());
						ps.setString(3, tradeLog.getTradeType());
						ps.setString(4, tradeLog.getTradeNode());
						ps.setLong(5, tradeLog.getUseTime());
						ps.setString(6, tradeLog.getReqSeq());
						ps.setString(7, tradeLog.getGlobalSeq());
						ps.setString(8, tradeLog.getTransSeq());
						ps.setString(9, tradeLog.getReqDate());
						ps.setString(10, tradeLog.getReqTime());
						ps.setString(11, tradeLog.getServiceId());
						ps.setString(12, tradeLog.getSvcScn());
						ps.setString(13, tradeLog.getRespSeq());
						ps.setString(14, tradeLog.getRespDate());
						ps.setString(15, tradeLog.getRespTime());
						ps.setString(16, tradeLog.getRespRcvTime());
						ps.setString(17, tradeLog.getTransStatus());
						ps.setString(18, tradeLog.getRetCode());
						ps.setString(19, null != tradeLog.getRetMsg() && tradeLog.getRetMsg().length() > 1024 ? tradeLog.getRetMsg().substring(0, 1024) : tradeLog.getRetMsg());
						ps.setString(20, tradeLog.getBusiSign());
						ps.setLong(21, tradeLog.getTimeStamp());
						ps.setString(22, tradeLog.getAppVersion());
						ps.setString(23, tradeLog.getRemark());
					}
				});
	}
	
	
	@Override
	public Integer insertBatch(List<SysTradeLog> tradeLogs) {
		if(!amsProperties.getDbEnable()){
			return 1;
		}
		
		int[] updateArray = jdbcTemplate.batchUpdate(
				"insert into SYS_TRADE_LOG (TRADE_CODE, TRADE_NAME, TRADE_TYPE, TRADE_NODE, USE_TIME, REQ_SEQ, GLOBAL_SEQ, TRANS_SEQ, REQ_DATE, REQ_TIME, SERVICE_ID, SVC_SCN, RESP_SEQ, RESP_DATE, "
						+ "RESP_TIME, RESP_RCV_TIME, TRANS_STATUS, RET_CODE, RET_MSG, BUSI_SIGN, TIME_STAMP, APP_VERSION, REMARK) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						SysTradeLog tradeLog = tradeLogs.get(i);
						ps.setString(1, tradeLog.getTradeCode());
						ps.setString(2, tradeLog.getTradeName());
						ps.setString(3, tradeLog.getTradeType());
						ps.setString(4, tradeLog.getTradeNode());
						ps.setLong(5, tradeLog.getUseTime());
						ps.setString(6, tradeLog.getReqSeq());
						ps.setString(7, tradeLog.getGlobalSeq());
						ps.setString(8, tradeLog.getTransSeq());
						ps.setString(9, tradeLog.getReqDate());
						ps.setString(10, tradeLog.getReqTime());
						ps.setString(11, tradeLog.getServiceId());
						ps.setString(12, tradeLog.getSvcScn());
						ps.setString(13, tradeLog.getRespSeq());
						ps.setString(14, tradeLog.getRespDate());
						ps.setString(15, tradeLog.getRespTime());
						ps.setString(16, tradeLog.getRespRcvTime());
						ps.setString(17, tradeLog.getTransStatus());
						ps.setString(18, tradeLog.getRetCode());
						ps.setString(19, null != tradeLog.getRetMsg() && tradeLog.getRetMsg().length() > 1024 ? tradeLog.getRetMsg().substring(0, 1024) : tradeLog.getRetMsg());
						ps.setString(20, tradeLog.getBusiSign());
						ps.setLong(21, tradeLog.getTimeStamp());
						ps.setString(22, tradeLog.getAppVersion());
						ps.setString(23, tradeLog.getRemark());

					}
					@Override
					public int getBatchSize() {
						return tradeLogs.size();
					}
				});
		
		int updateCount = 0;
		for (int val : updateArray) {
			updateCount += val;
		}
		return updateCount;
	}

	public List<?> getTradeUseTime(SysTradeConsole inBean) {
		StringBuilder sbsql = new StringBuilder(200);
		sbsql.append(" select t.trade_code, avg(t.use_time) as use_time, count(1) as trade_sum, ");
		sbsql.append(" sum(case when t.trans_status = 'S' then 1 else 0 end) as trade_succ ");
		sbsql.append("   from SYS_TRADE_LOG t ");
		sbsql.append("  where 1 = 1 ");
		if (null != inBean && null != inBean.getTimeCycle()) {
			sbsql.append("    and t.time_stamp > " + (System.currentTimeMillis() - (inBean.getTimeCycle() * 1000)));
		} else {
			sbsql.append("    and t.time_stamp > " + (System.currentTimeMillis() - 86400000));
		}
		if (null != inBean && StringUtils.isNotEmpty(inBean.getTradeCode())) {
			sbsql.append("    and t.trade_code like '%" + inBean.getTradeCode() + "%' ");
		}
		if (null != inBean && StringUtils.isNotEmpty(inBean.getTradeName())) {
			sbsql.append("    and t.trade_name like '%" + inBean.getTradeName() + "%' ");
		}
		sbsql.append("  group by t.trade_code ");
		sbsql.append("  ");
		List<Map<String, Object>> retList = jdbcTemplate.queryForList(sbsql.toString());
		return retList;
	}
	
	public SysTradeLog selectByReqDateAndSeq(String reqDate, String reqSeq) {
		SysTradeLogExample example = new SysTradeLogExample();
		example.createCriteria().andReqDateEqualTo(reqDate).andReqSeqEqualTo(reqSeq);
		return selectOneByExample(example);
	}

}
