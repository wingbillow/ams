package com.njcb.ams.web.filter;

import com.njcb.ams.portal.SysBaseDefine;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author liuyanlong
 * 设置全局编码为UTF-8
 */
@WebFilter(urlPatterns = "/*", filterName = "CharacterEncodingFilter")
public class CharacterEncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        request.setCharacterEncoding(SysBaseDefine.CharSet.UTF8.value);
        response.setCharacterEncoding(SysBaseDefine.CharSet.UTF8.value);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
