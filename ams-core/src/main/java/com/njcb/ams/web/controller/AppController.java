package com.njcb.ams.web.controller;

import com.njcb.ams.application.BusinessOPSManager;
import com.njcb.ams.repository.entity.SysInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.njcb.ams.pojo.dto.ComponentInfoDTO;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.dto.standard.PageResponse;

/**
 * @author srxhx207
 */
@RestController
@Lazy
public class AppController {
    private static final Logger logger = LoggerFactory.getLogger(AppController.class);

    @Autowired
    private BusinessOPSManager businessManager;

    @RequestMapping(value = "/open/appinfo")
    @ResponseBody
    public EntityResponse<SysInfo> appInfo() {
        return EntityResponse.build(businessManager.appInfo());
    }

    @RequestMapping(value = "/open/components")
    @ResponseBody
    public PageResponse<ComponentInfoDTO> componentList() {
        return businessManager.componentList();
    }

}
