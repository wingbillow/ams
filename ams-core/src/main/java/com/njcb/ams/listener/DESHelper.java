package com.njcb.ams.listener;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 * 
 * @author alan
 * @version 1.0
 * @since 1.0
 */
public class DESHelper {

	public static void main(String[] args) {
		BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();
		//设置密钥
		basicTextEncryptor.setPassword("helloAlanGoodMorning");
		// 加密
//		System.out.println(basicTextEncryptor.encrypt("ccs"));
		//解密
		System.out.println(basicTextEncryptor.decrypt("vTWHVSrg+Nxl+cLP1EyaPg=="));
	}

}
