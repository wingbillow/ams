package com.njcb.ams.pojo.dto.standard;

/**
 * 排序描述 
 *
 * @author liuyanlong
 */
public class OrderDesc {

	/**
	 * 排序字段
	 */
    private String field;
    private boolean asc = true;

    public String getField() {
		return field;
	}
    
    public void setField(String field) {
		this.field = field;
	}

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }
}
