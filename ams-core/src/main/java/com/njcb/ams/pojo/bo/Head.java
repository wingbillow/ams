package com.njcb.ams.pojo.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Http请求头
 * @author liuyanlong
 *
 */
public class Head {
	/**
	 * 请求流水
	 */
	private String reqSeq;

	/**
	 * 当前页数
	 */
	private int page;

	/**
	 * 每页条数
	 */
	private int rows;

	/**
	 * 处理模式  SysBaseDefine.QUERY_MODEL_EXPALL excel导出
	 */
	private String model;

	/**
	 * excel 标题
	 */
	private ArrayList<String> columnTitle;

	/**
	 * excel 列名
	 */
	private ArrayList<String> columnValue;

	/**
	 * 数据字典类型列表
	 */
	private Map<String, List<String>> dataDic;


	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public Map<String, List<String>> getDataDic() {
		return dataDic;
	}

	public void setDataDic(Map<String, List<String>> dataDic) {
		this.dataDic = dataDic;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getModel() {
		return model;
	}

	public void setColumnTitle(ArrayList<String> columnTitle) {
		this.columnTitle = columnTitle;
	}

	public ArrayList<String> getColumnTitle() {
		return columnTitle;
	}

	public void setColumnValue(ArrayList<String> columnValue) {
		this.columnValue = columnValue;
	}

	public ArrayList<String> getColumnValue() {
		return columnValue;
	}
}
