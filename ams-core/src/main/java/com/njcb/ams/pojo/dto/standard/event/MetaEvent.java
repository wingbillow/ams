package com.njcb.ams.pojo.dto.standard.event;

/**
 *
 * @author liuyanlong
 */
public abstract class MetaEvent extends Event {
	private static final long serialVersionUID = 1L;

	//事件主题
    public abstract String getTopic();

}
