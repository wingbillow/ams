package com.njcb.ams.pojo.dto.standard;

/**
 * 客户端命令模式标准
 *
 * @author liuyanlong
 */
public abstract class Request extends DTO{

    private static final long serialVersionUID = 1L;

    /**
     * command的操作人
     */
	private String conductUser;

	/**
     * command的操作时间
     */
    private String conductTime;

	/**
	 * @return the conductUser
	 */
	public String getConductUser() {
		return conductUser;
	}

	/**
	 * @param conductUser the conductUser to set
	 */
	public void setConductUser(String conductUser) {
		this.conductUser = conductUser;
	}

	/**
	 * @return the conductTime
	 */
	public String getConductTime() {
		return conductTime;
	}

	/**
	 * @param conductTime the conductTime to set
	 */
	public void setConductTime(String conductTime) {
		this.conductTime = conductTime;
	}


}
