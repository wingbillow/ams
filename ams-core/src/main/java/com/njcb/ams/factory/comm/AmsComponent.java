package com.njcb.ams.factory.comm;

/**
 * @author LOONG
 */
public interface AmsComponent {
    /**
     * 获取组件名称
     * @return
     */
    String getComponentName();

    /**
     * 组件说明信息
     * @return
     */
    String getDescription();

    /**
     * 获取组件上下文
     * @return
     */
    String getComponentContext();
}
