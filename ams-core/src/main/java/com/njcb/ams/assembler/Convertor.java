package com.njcb.ams.assembler;

import com.njcb.ams.util.AmsBeanUtils;

/**
 * 转换器， 用于客户端对象、域对象、数据对象之间的转换
 *
 * @author liuyanlong
 */
public interface Convertor<DTO, ENTITY, VO, BO> {

	default public DTO entityToDto(ENTITY entityObject) {
		DTO clientObject = null;
		AmsBeanUtils.copyProperties(clientObject, entityObject);
		return clientObject;
	}

	default public DTO voToDto(VO viewObject) {
		DTO clientObject = null;
		AmsBeanUtils.copyProperties(clientObject, viewObject);
		return clientObject;
	}

	default public ENTITY dtoToEntity(DTO clientObject) {
		ENTITY entityObject = null;
		AmsBeanUtils.copyProperties(entityObject, clientObject);
		return entityObject;
	}

	default public ENTITY voToEntity(VO viewObject) {
		ENTITY entityObject = null;
		AmsBeanUtils.copyProperties(entityObject, viewObject);
		return entityObject;
	}

	default public VO entityToVo(ENTITY entityObject) {
		VO dataObject = null;
		AmsBeanUtils.copyProperties(dataObject, entityObject);
		return dataObject;
	}
	
	default public BO entityToBo(ENTITY entityObject) {
		BO dataObject = null;
		AmsBeanUtils.copyProperties(dataObject, entityObject);
		return dataObject;
	}

}

