package com.njcb.ams.assembler;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

import com.njcb.ams.repository.entity.SysTradeLog;
import com.njcb.ams.pojo.dto.SysTradeLogDTO;

/**
 * @author LOONG
 */
@Component
public class SysTradeLogConvertor implements Convertor<SysTradeLogDTO, SysTradeLog, SysTradeLog, Object> {

	private static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

	static{
		mapperFactory.classMap(SysTradeLog.class,SysTradeLogDTO.class)
				.byDefault().register();
	}

	@Override
    public SysTradeLog dtoToEntity(SysTradeLogDTO clientObject) {
		SysTradeLog entityObject = mapperFactory.getMapperFacade().map(clientObject,SysTradeLog.class);
		return entityObject;
	}
}
