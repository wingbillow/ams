package com.njcb.ams.util;

import java.nio.ByteBuffer;
/**
 * @author liuyanlong
 */
public class AmsByteUtils {
	private static ByteBuffer buffer = ByteBuffer.allocate(8);

	public static int bytesToInt(byte[] bytes) {
		int num = bytes[3] & 0xFF;
		num |= (bytes[2] << 8) & 0xFF00;
		num |= (bytes[1] << 16) & 0xFF0000;
		num |= (bytes[0] << 24) & 0xFF000000;
		return num;
	}

	public static byte[] intToBytes(int i) {
		byte[] result = new byte[4];
		result[0] = (byte)((i >> 24) & 0xFF);
		result[1] = (byte)((i >> 16) & 0xFF);
		result[2] = (byte)((i >> 8) & 0xFF);
		result[3] = (byte)(i & 0xFF);
		return result;
	}

	public static byte[] longToBytes(long x) {
		buffer.putLong(x);
		return buffer.array();
	}
	
	public static long bytesTolong(byte[] bytes) {
		buffer.put(bytes, 0 , bytes.length);
		return buffer.getLong();
	}

}

