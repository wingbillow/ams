package com.njcb.ams.util;


import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author liuyanlong
 *
 */
public class AmsNumberUtils{

	public static boolean isNumeric(String inputStr) {
		if(StringUtils.isEmpty(inputStr) || !StringUtils.isNumeric(inputStr)){
			return false;
		}
		return true;
	}
	
}
