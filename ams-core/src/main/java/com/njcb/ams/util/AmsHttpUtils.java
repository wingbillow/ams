package com.njcb.ams.util;

import com.njcb.ams.portal.SysBaseDefine;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

/**
 * @author LOONG
 */
public class AmsHttpUtils {

    /**
     * ajax头标识
     */
    private static final String AJAX_HEAD = "X-Requested-With";
    private static final String AJAX_HEAD_VALUE = "XMLHttpRequest";

	/**
	 * 判断是否Ajax请求
	 * @param request
	 * @return
	 */
	public static Boolean isAjaxRequest(HttpServletRequest request) {
	    if(null == request || null == request.getHeader(AJAX_HEAD)){
	        return false;
        }
		if(AJAX_HEAD_VALUE.toUpperCase().equals(request.getHeader(AJAX_HEAD).toUpperCase())){
			return true;
		}
		return false;
	}

    /**
     * URL 解码
     */
    public static String getURLDecoderString(String str, String enc) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLDecoder.decode(str, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * URL 转码
     */
    public static String getURLEncoderString(String str, String enc) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        String str = "%E8%AF%B7%E5%81%87%E6%B5%81%E7%A8%8B";
        System.out.println(getURLEncoderString(str, SysBaseDefine.CharSet.UTF8.value));
        System.out.println(getURLDecoderString(str, SysBaseDefine.CharSet.UTF8.value));

    }

}
