package com.njcb.ams.util;

import com.njcb.ams.support.codevalue.EnumCode;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;

/**
 * IS工具类，用来判断基本的是否
 *
 * @author liuyanlong
 */
public class AmsUtils {

    /**
     * @param object 检测对象
     */
    public static boolean isNull(Object object) {
        return !isNotNull(object);
    }

    /**
     * @param object 检测对象
     */
    public static boolean isNotNull(Object object) {
        if (object == null) {
            return false;
        } else if (object instanceof String && "".equals(object)) {
            return false;
        } else if (object instanceof Integer && (0 == (Integer) object)) {
            return false;
        } else if (object instanceof Collection && CollectionUtils.isEmpty((Collection<?>) object)) {
            return false;
        } else if (object instanceof Object[] && ObjectUtils.isEmpty((Object[]) object)) {
            return false;
        } else if (object instanceof Map && ObjectUtils.isEmpty((Map<?, ?>) object)) {
            return false;
        }
        return true;
    }

    /**
     * @param objects 检测对象
     */
    public static boolean isNotNull(Object... objects) {
        for (Object object : objects) {
            if (isNull(object)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断集合中只有一个实体
     *
     * @param collection
     * @return
     */
    public static boolean isOneEntity(Collection<?> collection) {
        return null != collection && collection.size() == 1;
    }

    /**
     * 方法功能描述：判断是否正负浮点数字
     *
     * @param inputStr
     * @return
     */
    public static boolean isNumeric(String inputStr) {
        try {
            new BigDecimal(inputStr);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 方法功能描述：判断是否整数
     *
     * @param inputStr
     * @return
     */
    public static boolean isInteger(String inputStr) {
        if (StringUtils.isEmpty(inputStr) || !StringUtils.isNumeric(inputStr)) {
            return false;
        }
        return true;
    }

    /**
     * @param expected 预期值
     * @param actual   真实值
     */
    public static boolean isEqual(String expected, String actual) {
        if (!actual.equals(expected)) {
            return false;
        }
        return true;
    }

    /**
     * 验证日期格式yyyyMMdd
     *
     * @param str
     * @return
     */
    public static boolean isDate(String str) {
        boolean result = true;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            format.setLenient(false);
            format.parse(str);
        } catch (Exception e) {
            result = false;
        }
        return result;
    }


    /**
     * 验证指定格式日期
     *
     * @param str
     * @param fmt
     * @return
     */
    public static boolean isFormatDate(String str, String fmt) {
        boolean result = true;
        SimpleDateFormat format = new SimpleDateFormat(fmt);
        try {
            format.setLenient(false);
            format.parse(str);
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static <E extends Enum<E>> E valueOfCode(Class<E> enumClass, String code) {
        return EnumCode.valueOfCode(enumClass, code);
    }

}
