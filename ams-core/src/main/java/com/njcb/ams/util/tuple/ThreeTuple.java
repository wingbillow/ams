package com.njcb.ams.util.tuple;

/**
 * 三元组，持有三对象
 * @author LOONG
 *
 * @param <A>
 * @param <B>
 * @param <C>
 */
public class ThreeTuple<A, B, C> extends TwoTuple<A, B> {
	public final C third;

	public ThreeTuple(A a, B b, C c) {
		super(a, b);
		this.third = c;
	}

	@Override
	public String toString() {
		return "(" + first + "," + second + "," + third + ")";
	}
}
