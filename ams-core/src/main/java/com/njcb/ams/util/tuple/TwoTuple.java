package com.njcb.ams.util.tuple;

/**
 * 二元组，持有两对象
 * @author LOONG
 *
 * @param <A>
 * @param <B>
 */
public class TwoTuple<A, B> {
	public final A first;
	public final B second;

	public TwoTuple(A a, B b) {
		this.first = a;
		this.second = b;
	}

	@Override
	public String toString() {
		return "(" + first + "," + second + ")";
	}
}
