package com.njcb.ams.util;

import com.njcb.ams.factory.domain.AppContext;
import com.njcb.ams.repository.dao.SysInfoDAO;
import com.njcb.ams.repository.entity.SysInfo;

/**
 * @author LOONG
 */
public class SysInfoUtils {

    /**
     * 缓存的系统信息
     */
    private static SysInfo cacheSysInfo;

    public static SysInfo getSysInfo() {
        if (null == cacheSysInfo) {
            updateCacheSysInfo();
        }
        return cacheSysInfo;
    }

    public static String getSysId() {
        return getSysInfo().getSysId();
    }

    public static String getChannelId() {
        return getSysInfo().getChannelId();
    }

    public static String getSysName() {
        return getSysInfo().getSysName();
    }

    public static String getBusiDate() {
        return getSysInfo().getBusiDate();
    }

    public static void updateCacheSysInfo() {
        SysInfo sysInfo = AppContext.getBean(SysInfoDAO.class).selectSysInfo();
        /**
         * 如果sysinfo未配置、或配置类型为检查物理日期，则直接取物理日期
         */
        if (SysInfo.INSP_TYPE_1.equals(sysInfo.getInspType())) {
            sysInfo.setBusiDate(AmsDateUtils.getCurrentDate8());
            sysInfo.setLbatDate(AmsDateUtils.getBeforeDate(sysInfo.getBusiDate()));
            sysInfo.setNbatDate(AmsDateUtils.getNextDate(sysInfo.getBusiDate()));
        }
        cacheSysInfo = sysInfo;
    }
}
