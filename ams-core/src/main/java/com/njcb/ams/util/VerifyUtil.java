package com.njcb.ams.util;

import java.util.UUID;

public final class VerifyUtil {
	public static String getVerifyCode() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	public static String getUserID(String emailCode) {
		String userid = emailCode.substring(emailCode.indexOf("-") + 1);
		return userid;
	}

	public static String getUUID(String emailCode) {
		String uuid = emailCode.substring(0, emailCode.indexOf("-"));
		return uuid;
	}

}
